<?php

// src/ScenarisationProcessBundle/DataFixtures/ORM/LoadQuestionData.php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use ScenarisationProcessBundle\Entity\GeneralQuestion;
use ScenarisationProcessBundle\Entity\SequenceQuestion;
use ScenarisationProcessBundle\Entity\SessionQuestion;

use ScenarisationProcessBundle\Entity\QuestionOrder;

class LoadQuestionData implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        /*$em                     = $this->container->get('doctrine')->getManager();
        $repoScenarisationStage = $em->getRepository('ScenarisationProcessBundle:ScenarisationStage');

        $this->ajouterGeneralQuestion($manager,$repoScenarisationStage, "Etape 1", "A", 1);
        $this->ajouterGeneralQuestion($manager,$repoScenarisationStage, "Etape 1", "B", 2);
        $this->ajouterGeneralQuestion($manager,$repoScenarisationStage, "Etape 1", "C", 3);
        $this->ajouterGeneralQuestion($manager,$repoScenarisationStage, "Etape 1", "D", 4);
        */
    }

    public function ajouterGeneralQuestion(ObjectManager $manager, $repoScenarisationStage, $intituleScenarisationStage, $libelleFr, $position)
    {
        $question = new GeneralQuestion();
        $Position = new QuestionOrder();

        $Position->setPosition($position);

        $question->setFrWording($libelleFr);
        $etapeScenarisationMere = $repoScenarisationStage->findOneBy(array("title" => $intituleScenarisationStage));

        $etapeScenarisationMere->addQuestionOrder($Position);
        $question->addQuestionOrder($Position);

        $manager->persist($question);
        $manager->flush();
    }

    public function ajouterSequenceQuestion($manager,$repoScenarisationStage, $intituleEtape, $libelleFr, $position)
    {
      $questionSequence = new SequenceQuestion();
      $Position         = new QuestionOrder();

      $Position->setPosition($position);

      $questionSequence->setFrWording($libelleFr);
      $etapeScenarionMere = $repoScenarisationStage->findOneBy(array("title" => $intituleEtape));

      $etapeScenarionMere->addQuestionOrder($Position);
      $questionSequence->addQuestionOrder($Position);

      $manager->persist($questionSequence);
      $manager->flush();

    }

    public function ajouterSessionQuestion($manager,$repoScenarisationStage, $intituleEtape, $libelleFr, $position)
    {
      $questionSeance = new SessionQuestion();
      $Position         = new QuestionOrder();

      $Position->setPosition($position);

      $questionSeance->setFrWording($libelleFr);
      $etapeScenarionMere = $repoScenarisationStage->findOneBy(array("title" => $intituleEtape));

      $etapeScenarionMere->addQuestionOrder($Position);
      $questionSeance->addQuestionOrder($Position);

      $manager->persist($questionSeance);
      $manager->flush();

    }

    public function getOrder()
    {
        return 9;
    }
}
