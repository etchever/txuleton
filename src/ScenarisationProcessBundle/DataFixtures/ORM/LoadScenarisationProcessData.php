<?php

// src/ScenarisationProcessBundle/DataFixtures/ORM/LoadScenarisationProcessData.php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use ScenarisationProcessBundle\Entity\ScenarisationProcess;

class LoadScenarisationProcessData implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
      //  $this->ajouterScenarisationProcess($manager,"Démarche de Marta","Démarche qui a été présentée au mois d'avril et qui se divise en 3 étapes.");
    }

    public function ajouterScenarisationProcess(ObjectManager $manager, $intitule, $description)
    {
      $demarcheScenarisation = new ScenarisationProcess();

      $demarcheScenarisation->setTitle($intitule);
      $demarcheScenarisation->setDescription($description);

      $manager->persist($demarcheScenarisation);
      $manager->flush();
    }

    public function getOrder()
    {
        return 3;
    }
}
