<?php

namespace ScenarioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TrainingCourse
 *
 * @ORM\Table(name="training_course")
 * @ORM\Entity(repositoryClass="ScenarioBundle\Repository\TrainingCourseRepository")
 */
class TrainingCourse
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="shortname", type="string", length=255)
     */
    private $shortname;

    /**
     * @var string
     *
     * @ORM\Column(name="fullname", type="text")
     */
    private $fullname;

    /**
     * @ORM\OneToMany(targetEntity="ScenarioBundle\Entity\PedagogicalScenarioTrainingCourse", mappedBy="trainingCourse")
     * @ORM\JoinColumn(nullable=true)
     */
    private $pedagogicalScenarioTrainingCourse;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set shortname
     *
     * @param string $shortname
     *
     * @return TrainingCourse
     */
    public function setShortname($shortname)
    {
        $this->shortname = $shortname;

        return $this;
    }

    /**
     * Get shortname
     *
     * @return string
     */
    public function getShortname()
    {
        return $this->shortname;
    }

    /**
     * Set fullname
     *
     * @param string $fullname
     *
     * @return TrainingCourse
     */
    public function setFullname($fullname)
    {
        $this->fullname = $fullname;

        return $this;
    }

    /**
     * Get fullname
     *
     * @return string
     */
    public function getFullname()
    {
        return $this->fullname;
    }

    /**
     * Add pedagogicalScenarioTrainingCourse
     *
     * @param \ScenarioBundle\Entity\PedagogicalScenarioTrainingCourse $pedagogicalScenarioTrainingCourse
     *
     * @return TrainingCourse
     */
    public function addPedagogicalScenarioTrainingCourse(\ScenarioBundle\Entity\PedagogicalScenarioTrainingCourse $pedagogicalScenarioTrainingCourse)
    {
        $this->pedagogicalScenarioTrainingCourse[] = $pedagogicalScenarioTrainingCourse;
        $pedagogicalScenarioTrainingCourse->setTrainingCourse($this);

        return $this;
    }

    /**
     * Remove pedagogicalScenarioTrainingCourse
     *
     * @param \ScenarioBundle\Entity\PedagogicalScenarioTrainingCourse PedagogicalScenarioTrainingCourse
     */
    public function removePedagogicalScenarioTrainingCourse(\ScenarioBundle\Entity\PedagogicalScenarioTrainingCourse $pedagogicalScenarioTrainingCourse)
    {
        $this->pedagogicalScenarioTrainingCourse->removeElement($pedagogicalScenarioTrainingCourse);
    }

    /**
     * Get pedagogicalScenarioTrainingCourse
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPedagogicalScenarioTrainingCourse()
    {
        return $this->pedagogicalScenarioTrainingCourse;
    }
}
