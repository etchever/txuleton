<?php

// src/ScenarioBundle/DataFixtures/ORM/LoadAnswerData.php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use ScenarioBundle\Entity\Answer;
use ScenarioBundle\Entity\GeneralAnswer;
use ScenarioBundle\Entity\SequenceAnswer;
use ScenarioBundle\Entity\SessionAnswer;

class LoadAnswerData implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        /*$em = $this->container->get('doctrine')->getManager();

        // A n'as pas de réponse

        // B a une réponse dans M1102
        $this->ajouterReponseGenerale($manager,$em,"B","rep B","M1102");

        // C a une réponse dans M1102 et une dans M1103
        $this->ajouterReponseGenerale($manager,$em,"C","rep C M1102", "M1102");
        $this->ajouterReponseGenerale($manager,$em,"C","rep C M1103", "M1103");

        // D a une réponse dans M1103 et M1104 (aucune dans M1102)
        $this->ajouterReponseGenerale($manager,$em,"D","red D M1103", "M1103");
        $this->ajouterReponseGenerale($manager,$em,"D","red D M1104", "M1104");
        /*$this->ajouterReponseSequence($manager,$em,);
        $this->ajouterReponseSeance($manager,$em,);*/

    }

    public function ajouterReponseGenerale(ObjectManager $manager, $em, $intituleQuestion, $contenuReponse,$scenarioPedagogique)
    {

      $reponseGenerale = new GeneralAnswer();
      $reponseGenerale->setTextAnswer($contenuReponse);
      $reponseGenerale->setIsValid(false);

      $repoQuestionGenerale = $em->getRepository('ScenarisationProcessBundle:GeneralQuestion');
      $questionAssociee = $repoQuestionGenerale->findOneBy(array("frWording" => $intituleQuestion));
      $reponseGenerale->setGeneralQuestion($questionAssociee);

      $repoScenarioPeda = $em->getRepository('ScenarioBundle:PedagogicalScenario');
      $scenarioAssocie  = $repoScenarioPeda->findOneBy(array("shortname" => $scenarioPedagogique));
      $scenarioAssocie->addAnswer($reponseGenerale);

      $questionAssociee->addAnswer($reponseGenerale);



      $manager->persist($reponseGenerale);
      $manager->flush();

    }

    public function getOrder()
    {
        return 10;
    }
}
