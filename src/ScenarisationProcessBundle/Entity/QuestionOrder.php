<?php

namespace ScenarisationProcessBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * QuestionOrder
 *
 * @ORM\Table(name="question_order")
 * @ORM\Entity(repositoryClass="ScenarisationProcessBundle\Repository\QuestionOrderRepository")
 */
class QuestionOrder
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="position", type="smallint")
     * @Gedmo\Sortable(groups={"scenarisationStage"})
     */
    private $position;

    /**
     * @ORM\ManyToOne(targetEntity="ScenarisationProcessBundle\Entity\Question", inversedBy="questionOrder", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    private $question;

    /**
     * @ORM\ManyToOne(targetEntity="ScenarisationProcessBundle\Entity\ScenarisationStage", inversedBy="questionOrder", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    private $scenarisationStage;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return ordreQuestion
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set scenarisationStage
     *
     * @param \ScenarisationProcessBundle\Entity\ScenarisationStage $scenarisationStage
     *
     * @return QuestionOrder
     */
    public function setScenarisationStage(\ScenarisationProcessBundle\Entity\ScenarisationStage $scenarisationStage = null)
    {
        $this->scenarisationStage = $scenarisationStage;

        return $this;
    }

    /**
     * Get scenarisationStage
     *
     * @return \ScenarisationProcessBundle\Entity\ScenarisationStage
     */
    public function getScenarisationStage()
    {
        return $this->scenarisationStage;
    }

    /**
     * Set question
     *
     * @param \ScenarisationProcessBundle\Entity\Question $question
     *
     * @return QuestionOrder
     */
    public function setQuestion(\ScenarisationProcessBundle\Entity\Question $question = null)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return \ScenarisationProcessBundle\Entity\Question
     */
    public function getQuestion()
    {
        return $this->question;

    }

}
