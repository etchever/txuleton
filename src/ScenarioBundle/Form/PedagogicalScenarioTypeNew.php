<?php

namespace ScenarioBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class PedagogicalScenarioTypeNew extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('project', EntityType::class, array(
                    'label' => 'project',
                    'class' => 'ScenarioBundle:Project',
                    'choice_label' => 'shortname',
                    'multiple'  => false,
                    'expanded' => false,
                    'required' => false,
                    'empty_value' => null,
                    'placeholder' => '(none)',
                    ));

        $builder->add('shortname','text', array( 'label' => 'shortname'));
        $builder->add('fullname','text', array( 'label' => 'fullname'));

        $builder->add('referentTeacher', EntityType::class, array(
                    'label' => 'referentTeacher',
                    'class' => 'UserBundle:User',
                    'choice_label' => 'lastnameFirstname',
                    'query_builder' => function (EntityRepository $er) {
                      return $er->createQueryBuilder('u')
                        ->where('u.roles LIKE :role')
                        ->orderBy('u.lastname', 'ASC')
                        ->setParameter('role','%ROLE_TEACHER%');
                      }
                    //'multiple'  => true
                    ));

        $builder->add('scenarisationProcess', EntityType::class, array(
                    'label' => 'scenarisationProcess',
                    'class' => 'ScenarisationProcessBundle:ScenarisationProcess',
                    'choice_label' => 'title'
                    //'multiple'  => true
                    ));

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ScenarioBundle\Entity\PedagogicalScenario'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'scenariobundle_pedagogicalscenario';
    }


}
