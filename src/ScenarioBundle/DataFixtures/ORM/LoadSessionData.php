<?php

// src/ScenarioBundle/DataFixtures/ORM/LoadSessionData.php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use ScenarioBundle\Entity\Session;

class LoadSessionData implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        /*$em = $this->container->get('doctrine')->getManager();

        $this->ajouterS1($manager,$em);*/

    }

    public function ajouterS1(ObjectManager $manager,$em)
    {
      $s1 = new Session();

      $s1->setDescription("Une longue description");
      $s1->setGoal("Un bel objectif");
      $s1->setModality("Vidéo");
      $s1->setOrder(1);

      $repository = $em->getRepository('ScenarioBundle:Sequence');
      $seq1 = $repository->findOneBy(array('title' => 'Les conditions'));
      $seq1->addSession($s1);

      $manager->persist($s1);
      $manager->flush();
    }

    public function getOrder()
    {
        return 5;
    }
}
