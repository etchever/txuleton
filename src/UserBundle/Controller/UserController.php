<?php

namespace UserBundle\Controller;

use UserBundle\Entity\User;
use UserBundle\Entity\Lecturer;
use UserBundle\Entity\PedagogicalEngineer;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Translation\Translator;

use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * User controller.
 *
 */
class UserController extends Controller
{
    /**
     * Lists all utilisateur entities.
     *
     */
    public function indexAction(Request $request)
    {


        // We generate the new breadcrumb
        $this->createBreadcrumb("index");
        $this->updateMenu();

        $em = $this->getDoctrine()->getManager();

        //$utilisateurs = $em->getRepository('UserBundle:User')->findAll();

        $queryBuilder = $em->getRepository('UserBundle:User')->createQueryBuilder('users');
        $query = $queryBuilder->getQuery();

        $paginator  = $this->get('knp_paginator');

        $utilisateurs = $paginator->paginate(
          $query,
          $request->query->getInt('page', 1)/*page number*/,
          5/*limit per page*/
        );

        return $this->render('UserBundle:user:index.html.twig', array(
            'users' => $utilisateurs
        ));
    }

    /**
     * Creates a new utilisateur entity.
     *
     */
    public function newAction(Request $request)
    {
      // We generate the new breadcrumb
      $this->createBreadcrumb("new");
      $this->updateMenu();

        $utilisateur = new User();

        // Obligé de mettre la ligne suivante sinon le formulaire ne s'affiche pas correctement
        $utilisateur->setRoles('ROLE_TEACHER');
        $form = $this->createForm('UserBundle\Form\UserTypeNew', $utilisateur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($utilisateur);
            $em->flush();

            return $this->redirectToRoute('user_show', array('id' => $user->getId()));
        }

        return $this->render('UserBundle:user:new.html.twig', array(
            'user' => $utilisateur,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a utilisateur entity.
     *
     */
    public function showAction(User $user)
    {
      // We generate the new breadcrumb
      $this->createBreadcrumb("show",$user);
      $this->updateMenu();

        $deleteForm = $this->createDeleteForm($user);

        return $this->render('UserBundle:user:show.html.twig', array(
            'user'        => $user,
            'delete_form' => $deleteForm->createView()
        ));
    }

    /**
     * Displays a form to edit an existing utilisateur entity.
     *
     */
    public function editAction(Request $request, User $user)
    {
      // We generate the new breadcrumb
      $this->createBreadcrumb("edit",$user);
      $this->updateMenu();

        $deleteForm = $this->createDeleteForm($user);
        $editForm   = $this->createForm('UserBundle\Form\UserTypeEdit', $user);

        // Je dois ajouter le role ici (et pas dans le form) car la valeur change en fonction de la langue courante ("engineer"/"ingénieur"/...)
        // Le problème c'est que mon getRoles() retourne au formulaire un truc générique ROLE_TEACHER.
        // On ne peut pas créer une autre méthode dans User car la classe ne connait pas la locale courante
        // Donc on est obligé de traduire le contenu ici...
        $editForm->add('roles','text', array(
                           'label' => 'role',
                           'read_only' => 'true',
                           'data' => $this->get('translator')->trans($editForm->getData()->getRoles()[0])
                           )
                      );

        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid())
        {
          $this->getDoctrine()->getManager()->flush();

          return $this->redirectToRoute('user_edit', array('id' => $user->getId()));
        }

        return $this->render('UserBundle:user:edit.html.twig', array(
            'user' => $user,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a utilisateur entity.
     *
     */
    public function deleteAction(Request $request, User $user)
    {
        $form = $this->createDeleteForm($user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

          // If the user that we want to delete is the user who is executing the function
          // We remove his cookie
          if ($this->get('security.context')->getToken()->getUser()->getId() == $user->getId())
          {
            return $this->redirectToRoute('logout');
            /*$this->get('security.context')->setToken(null);
            $this->get('request')->getSession()->invalidate();*/
          }

          $em = $this->getDoctrine()->getManager();
          $em->remove($user);
          $em->flush();
        }

        /* Petit message de reussite, à traduire */
        $request->getSession()->getFlashBag()->add('success', "L'utilisateur '" . $user->getFirstname() . " " . $user->getLastname() . "' a bien été supprimé.");
        return $this->redirectToRoute('user_index');
    }

    /**
     * Send an email to user containing a link to redefine his password.
     *
     */
    public function askToRedefinePasswordAction(Request $request, User $user)
    {
      $user->setToken();

      $em = $this->getDoctrine()->getManager();
      $em->persist($user);
      $em->flush();

      $message = (new \Swift_Message("Création d'un nouveau mot de passe"))
        ->setFrom('cda-iut@laposte.net')
        ->setTo($user->getEmail())
        ->setBody(
            $this->renderView(
                // app/Resources/views/Emails/registration.html.twig
                'Emails/redefinePassword.html.twig',
                array(
                  'user'  => $user,
                  'token' => $user->getToken())
            ),
            'text/html'
        );

         $this->get('mailer')->send($message);

      /* Petit message de reussite, à traduire */
      $request->getSession()->getFlashBag()->add('success', // traduction dans le bundle user
      $this->get('translator')->trans('mail.success', array("%firstname%" => $user->getFirstname(), "%lastname%" => $user->getLastname(), "%email%" => $user->getEmail())));
      return $this->redirectToRoute('user_index');

    }

    /**
     * Creates a form to delete a utilisateur entity.
     *
     * @param User $utilisateur The utilisateur entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(User $user)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('user_delete', array('id' => $user->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    private function createBreadcrumb($currentAction, $user = null)
    {
      $breadcrumbs = $this->get("white_october_breadcrumbs");
      $breadcrumbs->addItem("admin");
      switch($currentAction)
      {
        case "index":
          $breadcrumbs->addItem("user");
        break;

        case "new":
          $breadcrumbs->addRouteItem("user", "user_index", [
              'userRole' => ($this->get('security.authorization_checker')->isGranted('ROLE_PEDAGOGICAL_ENGINEER')?"pedagogical-engineer":"teacher")
          ]);
          $breadcrumbs->addItem("user.new");
        break;

        case "show":
          $breadcrumbs->addRouteItem("user", "user_index", [
              'userRole' => ($this->get('security.authorization_checker')->isGranted('ROLE_PEDAGOGICAL_ENGINEER')?"pedagogical-engineer":"teacher")
          ]);
          $breadcrumbs->addItem($user->getFirstname() . " " . $user->getLastname());
          $breadcrumbs->addItem("show");
        break;

        case "edit":
          $breadcrumbs->addRouteItem("user", "user_index", [
              'userRole' => ($this->get('security.authorization_checker')->isGranted('ROLE_PEDAGOGICAL_ENGINEER')?"pedagogical-engineer":"teacher")
          ]);
          $breadcrumbs->addItem($user->getFirstname() . " " . $user->getLastname());
          $breadcrumbs->addItem("edit");
        break;
      }
    }

    private function updateMenu()
    {
      // Mise en évidence du menu, changement du menu sélectionné
      $menu = array('pedagogical_scenario' => "",
                    'projects'             => "",
                    'trainingCourse'       => "",
                    'user'                 => "list-group-item-info",
                    'parameters'           => "",
                    'scenarisation_process'=> "",
                    'scenarisation_stage'  => "",
                    'question'             => "");

      // Mise en session du menu
      $this->get('session')->set('menu',$menu);
    }
}
