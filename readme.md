# Présentation

Txuleton est une plateforme Web de scénarisation pédagogique. La plateforme s'adresse à des ingénieurs pédagogiques et à des enseignants désirant élaborer collaborativement des modules d'enseignement adaptés pour de l'enseignement mixte ou à distance. 

![](https://git.univ-pau.fr/etchever/txuleton/-/raw/master/presentation/txuleton1.jpg)

Txuleton permet aux ingénieurs pédagogiques d'élaborer de nouvelles démarches de scénarisation pour tenir compte des spécificités des modules pédagogiques à concevoir mais aussi du niveau de connaissance des enseignants concernant les outils TICE. 

![](https://git.univ-pau.fr/etchever/txuleton/-/raw/master/presentation/txuleton2.jpg)

Pour élaborer ces démarches de scénarisation, Txuleton propose aux ingénieurs pédagogiques de définir des étapes de conception ou de réutiliser des étapes de conception utilisées dans d'autres démarches de scénarisation. Dans étape de conception, les ingénieurs pédagogiques peuvent définir un certain nombre de questions qui seront posées aux enseignants afin de les aider à formaliser les modalités d'enseignement et la progression pédagogique qu'ils souhaitent intégrer dans leur module. Ici aussi, les questions peuvent être définies par les ingénieurs pédagogiques ou puisées dans une banque de questions utilisées dans d'autres démarches de scénarisation.

Une fois la démarche de scénarisation définie, les ingénieurs pédagogiques peuvent la dérouler pas à pas en soumettant les différentes questions aux enseignants, en recueillant les réponses et en passant à l'étape suivante de conception si les éléments d'information fournis par les enseignants sont jugés suffisamment clairs.

Les spécifications finales peuvent être exportées par Txuleton sous forme d'un module SCORM définissant les séances et les séquences du scénario élaboré. Ce module SCORM peut ensuite être importé sur la plateforme Moodle permettant ensuite aux ingénieurs et aux enseignants de définir les différentes activités et ressources à intégrer dans chaque séance / séquence. 

# Crédits
Le prototype Txuleton a été élaboré dans le cadre des activités de recherche de l’équipe [T2I](https://liuppa.univ-pau.fr/fr/activites-scientifiques/axes-de-recherche/t2i.html "T2I") du Laboratoire d’Informatique de l’Université de Pau et des Pays de l’Adour ([LIUPPA](https://liuppa.univ-pau.fr/ "LIUPPA")). Sa conception et son développement ont été réalisés par des enseignants-chercheurs de l’[IUT de Bayonne](https://www.iutbayonne.univ-pau.fr/ "IUT de Bayonne") (Pantxika Dagorret, Patrick Etcheverry, Philippe Lopistéguy, Christophe Marquesuzaà, Thierry Nodenot,), une ingénieure pédagogique de l’IUT de Bayonne (Marta Toribio Fontenla) et un étudiant d’école d’ingénieur de l’[ISIMA](https://www.isima.fr/ "ISIMA") (Gaizka Alçuyet).


# Aperçu du prototype
![Txuleton](https://git.univ-pau.fr/etchever/txuleton/-/raw/master/presentation/demoTxuleton.mp4)




