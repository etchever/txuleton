<?php

namespace ScenarioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Project
 *
 * @ORM\Table(name="project")
 * @ORM\Entity(repositoryClass="ScenarioBundle\Repository\ProjectRepository")
 */
class Project
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="shortname", type="string", length=255)
     */
    private $shortname;

    /**
     * @var string
     *
     * @ORM\Column(name="fullname", type="text")
     */
    private $fullname;

    /**
     * @ORM\OneToMany(targetEntity="ScenarioBundle\Entity\PedagogicalScenario", mappedBy="project", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $pedagogicalScenarios;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set shortname
     *
     * @param string $shortname
     *
     * @return Project
     */
    public function setShortname($shortname)
    {
        $this->shortname = $shortname;

        return $this;
    }

    /**
     * Get shortname
     *
     * @return string
     */
    public function getShortname()
    {
        return $this->shortname;
    }

    /**
     * Set fullname
     *
     * @param string $fullname
     *
     * @return Project
     */
    public function setFullname($fullname)
    {
        $this->fullname = $fullname;

        return $this;
    }

    /**
     * Get fullname
     *
     * @return string
     */
    public function getFullname()
    {
        return $this->fullname;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->pedagogicalScenario = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add pedagogicalScenario
     *
     * @param \ScenarioBundle\Entity\PedagogicalScenario $pedagogicalScenario
     *
     * @return Project
     */
    public function addPedagogicalScenario(\ScenarioBundle\Entity\PedagogicalScenario $pedagogicalScenario)
    {
        $this->pedagogicalScenarios[] = $pedagogicalScenario;

        return $this;
    }

    /**
     * Remove pedagogicalScenario
     *
     * @param \ScenarioBundle\Entity\PedagogicalScenario $pedagogicalScenario
     */
    public function removePedagogicalScenario(\ScenarioBundle\Entity\PedagogicalScenario $pedagogicalScenario)
    {
        $this->pedagogicalScenarios->removeElement($pedagogicalScenario);

    }

    /**
     * Get pedagogicalScenario
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPedagogicalScenario()
    {
        return $this->pedagogicalScenario;
    }
}
