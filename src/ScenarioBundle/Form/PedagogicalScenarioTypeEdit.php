<?php

namespace ScenarioBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class PedagogicalScenarioTypeEdit extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('project', EntityType::class, array(
                    'label' => 'project',
                    'class' => 'ScenarioBundle:Project',
                    'choice_label' => 'shortname',
                    'multiple'  => false,
                    'expanded' => false,
                    'required' => false,
                    'placeholder' => '(none)',
                    'empty_value' => null
                    ));

        $builder->add('shortname','text', array( 'label' => 'shortname'));
        $builder->add('fullname','text', array( 'label' => 'fullname'));

        $builder->add('referentTeacher', EntityType::class, array(
                    'label' => 'referentTeacher',
                    'class' => 'UserBundle:User',
                    'choice_label' => 'lastnameFirstname',
                    'query_builder' => function (EntityRepository $er) {
                      return $er->createQueryBuilder('u')
                        ->where('u.roles LIKE :role')
                        ->orderBy('u.lastname', 'ASC')
                        ->setParameter('role','%ROLE_TEACHER%');
                      }
                    //'multiple'  => true
                    ));

        // Une fois que la démarche de scénarisation d'un scénario pédagogique a été définie, on ne peut plus la modifier.
        $builder->add('scenarisationProcess', 'text', array(
                    /* On veut simplement afficher le nom de la démarche sous forme textuelle non pas
                       ss forme d'entité modifiable. L'option 'mapped' signale à Doctrine de ne pas
                       enregistrer ce champ qui est ici un texte et non pas une entité démarche enregistrable */
                    'mapped' => false,
                    'label' => 'scenarisationProcess',
                    'read_only' => true,
                    /* $option['data'] contient le scénario pédagogique courant sous forme object
                       il est envoyé par le controller par la méthode "createForm" */
                    'data' => $options['data']->getScenarisationProcess()->__toString()
                    ));



    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ScenarioBundle\Entity\PedagogicalScenario'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'scenariobundle_pedagogicalscenario';
    }


}
