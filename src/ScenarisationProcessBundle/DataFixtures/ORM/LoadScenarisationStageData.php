<?php

// src/ScenarisationProcessBundle/DataFixtures/ORM/LoadEtapeScenarisationData.php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use ScenarisationProcessBundle\Entity\ScenarisationStage;
use ScenarisationProcessBundle\Entity\ScenarisationStageOrder;

class LoadEtapeScenarisationData implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
      /*  $em = $this->container->get('doctrine')->getManager();

        // On récupère la démarche de Marta
        $repoScenarisationProcess = $em->getRepository('ScenarisationProcessBundle:ScenarisationProcess');
        $demarcheMarta = $repoScenarisationProcess->findOneBy(array('title' => 'Démarche de Marta'));

        // On ajoute nos trois étapes
        $this->ajouterEtape($manager,$demarcheMarta,"Etape 1",1);
        $this->ajouterEtape($manager,$demarcheMarta,"Etape 2",2);
        $this->ajouterEtape($manager,$demarcheMarta,"Etape 3",3);
        */
    }

    public function ajouterEtape(ObjectManager $manager, $demarcheScenarisationMere, $intitule, $ordre)
    {
        $etape                   = new ScenarisationStage();
        $ordreEtapeScenarisation = new ScenarisationStageOrder();

        $etape->setTitle($intitule);

        if ($demarcheScenarisationMere != NULL)
        {
          $ordreEtapeScenarisation->setPosition($ordre);
          $demarcheScenarisationMere->addScenarisationStageOrder($ordreEtapeScenarisation);
          $etape->addScenarisationStageOrder($ordreEtapeScenarisation);
        }
        $manager->persist($etape);
        $manager->flush();
    }

    public function getOrder()
    {
        return 7;
    }
}
