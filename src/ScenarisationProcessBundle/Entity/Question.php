<?php

namespace ScenarisationProcessBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Question
 *
 * @ORM\Table(name="question")
 * @ORM\Entity(repositoryClass="ScenarisationProcessBundle\Repository\QuestionRepository")
 *
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({"question" = "Question", "questionGenerale" = "GeneralQuestion", "questionSequence" = "SequenceQuestion", "questionSeance" = "SessionQuestion"})
 */
class Question
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="enWording", type="text", nullable=true)
     * By default, we say that the question's wording contains a text that say that there is no translation in the current language.
     */
    private $enWording = "No translation";

    /**
     * @var string
     *
     * @ORM\Column(name="frWording", type="text", nullable=true)
     * By default, we say that the question's wording contains a text that say that there is no translation in the current language.
     */
    private $frWording = "Pas de traduction";

    /**
     * @var string
     *
     * @ORM\Column(name="esWording", type="text", nullable=true)
     * By default, we say that the question's wording contains a text that say that there is no translation in the current language.
     */
    private $esWording = "No traduction";

    /**
     * @ORM\OneToMany(targetEntity="ScenarisationProcessBundle\Entity\QuestionOrder", mappedBy="question", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $questionOrder;

    /**
     * @ORM\OneToMany(targetEntity="ScenarioBundle\Entity\Answer", mappedBy="question", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $answers;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set enWording
     *
     * @param string $enWording
     *
     * @return Question
     */
    public function setEnWording($enWording)
    {
        $this->enWording = $enWording;

        return $this;
    }

    /**
     * Get enWording
     *
     * @return string
     */
    public function getEnWording()
    {
        return $this->enWording;
    }

    /**
     * Set frWording
     *
     * @param string $frWording
     *
     * @return Question
     */
    public function setFrWording($frWording)
    {
        $this->frWording = $frWording;

        return $this;
    }

    /**
     * Get frWording
     *
     * @return string
     */
    public function getFrWording()
    {
        return $this->frWording;
    }

    /**
     * Set esWording
     *
     * @param string $esWording
     *
     * @return Question
     */
    public function setEsWording($esWording)
    {
        $this->esWording = $esWording;

        return $this;
    }

    /**
     * Get esWording
     *
     * @return string
     */
    public function getEsWording()
    {
        return $this->esWording;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->questionOrder = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add questionOrder
     *
     * @param \ScenarisationProcess\Entity\QuestionOrder $questionOrder
     *
     * @return Question
     */
    public function addQuestionOrder(\ScenarisationProcessBundle\Entity\QuestionOrder $questionOrder)
    {
        $this->questionOrder[] = $questionOrder;
        $questionOrder->setQuestion($this);

        return $this;
    }

    /**
     * Remove questionOrder
     *
     * @param \ScenarisationProcess\Entity\QuestionOrder $questionOrder
     */
    public function removeQuestionOrder(\ScenarisationProcessBundle\Entity\QuestionOrder $questionOrder)
    {
        $this->questionOrder->removeElement($questionOrder);
    }

    /**
     * Get questionOrder
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuestionOrder()
    {
        return $this->questionOrder;
    }

    /**
     * Add answer
     *
     * @param \ScenarioBundle\Entity\Answer $answer
     *
     * @return Question
     */
    public function addAnswer(\ScenarioBundle\Entity\Answer $answer)
    {
        $this->answers[] = $answer;
        $answer->setQuestion($this);

        return $this;
    }

    /**
     * Remove answer
     *
     * @param \ScenarioBundle\Entity\Answer $answer
     */
    public function removeAnswer(\ScenarioBundle\Entity\Answer $answer)
    {
        $this->answers->removeElement($answer);
        $answer->setQuestion(null);
    }

    /**
     * Get answer
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnswers()
    {
        return $this->answers;
    }
}
