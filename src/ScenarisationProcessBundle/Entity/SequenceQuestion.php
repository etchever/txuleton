<?php

namespace ScenarisationProcessBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SequenceQuestion
 *
 * @ORM\Table(name="sequence_question")
 * @ORM\Entity(repositoryClass="ScenarisationProcessBundle\Repository\SequenceQuestionRepository")
 */
class SequenceQuestion extends Question
{
  /**
   * @ORM\OneToMany(targetEntity="ScenarioBundle\Entity\SequenceAnswer", mappedBy="sequenceQuestion", cascade={"persist"})
   * @ORM\JoinColumn(nullable=false)
   */
  private $sequenceAnswers;

  /**
   * Add sequenceAnswer
   *
   * @param \ScenarioBundle\Entity\SequenceAnswer sequenceAnswer
   *
   * @return SequenceQuestion
   */
  public function addSequenceAnswer(\ScenarioBundle\Entity\SequenceAnswer $sequenceAnswer)
  {
      $this->sequenceAnswers[] = $sequenceAnswer;

      return $this;
  }

  /**
   * Remove sequenceAnswer
   *
   * @param \ScenarioBundle\Entity\GeneralAnswer $sequenceAnswer
   */
  public function removeSequenceAnswer(\ScenarioBundle\Entity\GeneralAnswer $sequenceAnswer)
  {
      $this->sequenceAnswers->removeElement($sequenceQuestion);
  }

  /**
   * Get sequenceAnswer
   *
   * @return \Doctrine\Common\Collections\Collection
   */
  public function getSequenceAnswers()
  {
      return $this->sequenceAnswers;
  }

  public function getType()
  {
    return 'sequenceQuestion';
  }
}
