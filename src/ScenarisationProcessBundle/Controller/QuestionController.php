<?php

namespace ScenarisationProcessBundle\Controller;

use ScenarisationProcessBundle\Entity\Question;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

// Pour les formulaires
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

use ScenarisationProcessBundle\Entity\SessionQuestion;
use ScenarisationProcessBundle\Entity\SequenceQuestion;
use ScenarisationProcessBundle\Entity\GeneralQuestion;

/**
 * Question controller.
 *
 */
class QuestionController extends Controller
{
    /**
     * Lists all question entities.
     *
     */
    public function indexAction(Request $request)
    {
        // We generate the new breadcrumb
        $this->createBreadcrumb("index");
        // Mise en évidence du menu, changement du menu sélectionné
        $this->updateMenu();


        $em = $this->getDoctrine()->getManager();

        //$questions = $em->getRepository('ScenarisationProcessBundle:Question')->findAll();
        $queryBuilder = $em->getRepository('ScenarisationProcessBundle:Question')->createQueryBuilder('questions');
        $query = $queryBuilder->getQuery();

        $paginator  = $this->get('knp_paginator');

        $questions = $paginator->paginate(
          $query,
          $request->query->getInt('page', 1)/*page number*/,
          10/*limit per page*/
        );
        return $this->render('ScenarisationProcessBundle:question:index.html.twig', array(
            'questions' => $questions
        ));
    }

    /**
     * Creates a new question entity.
     *
     */
    public function newAction(Request $request)
    {
      // We generate the new breadcrumb
      $this->createBreadcrumb("new");
      // Mise en évidence du menu, changement du menu sélectionné
      $this->updateMenu();

         /* On construit un tableau dans lequel les données du formulaire
            seront recueillies */
          $donneesFormulaireQuestion = array();

          $editForm = $this->createQuestionForm($donneesFormulaireQuestion);

          $editForm->handleRequest($request);

        if ($editForm->isSubmitted())
        {
            $donneesFormulaireQuestionRecuperees = $editForm->getData();

            if ($this->questionFormIsValid($donneesFormulaireQuestionRecuperees))
            {
              $question = $this->createQuestionEntityFromFormData($donneesFormulaireQuestionRecuperees);

              $em = $this->getDoctrine()->getManager();
              $em->persist($question);
              $em->flush();

              return $this->redirectToRoute('question_show', array('id' => $question->getId()));
            }
        }

        return $this->render('ScenarisationProcessBundle:question:new.html.twig', array(
            'form' => $editForm->createView()//$form->createView(),
        ));
    }

    /**
     * Finds and displays a question entity.
     *
     */
    public function showAction(Question $question)
    {
      // We generate the new breadcrumb
      $this->createBreadcrumb("show", $question);

      // Mise en évidence du menu, changement du menu sélectionné
      $this->updateMenu();

        $deleteForm = $this->createDeleteForm($question);

        return $this->render('ScenarisationProcessBundle:question:show.html.twig', array(
            'question'    => $question,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing question entity.
     *
     */
    public function editAction(Request $request, Question $question)
    {
      // Avant de permettre l'édition il va falloir s'assurer que la question
      // n'a pas déjà une réponse initialisée
      if ($this->haveAlreadyAnAnswer($question) == false)
      {
        $em = $this->getDoctrine()->getManager();

        // We generate the new breadcrumb
        $this->createBreadcrumb("edit", $question);

        // Mise en évidence du menu, changement du menu sélectionné
        $deleteForm = $this->createDeleteForm($question);

          /* On construit un tableau dans lequel les données du formulaire
             seront recueillies */
             $donneesFormulaireQuestion = array();

             /* On récupère les données déjà présentes */
             $donneesFormulaireQuestion['enWording'] = $question->getEnWording();
             $donneesFormulaireQuestion['frWording'] = $question->getFrWording();
             $donneesFormulaireQuestion['esWording'] = $question->getEsWording();
             $donneesFormulaireQuestion['type']      = $question->getType();

             $editForm = $this->createQuestionForm($donneesFormulaireQuestion);

             $editForm->handleRequest($request);

           if ($editForm->isSubmitted())
           {
               $donneesFormulaireQuestionRecuperees = $editForm->getData();

               if ($this->questionFormIsValid($donneesFormulaireQuestionRecuperees))
               {
                 // Si le type de question n'a pas changé, on met juste à jour les 3 libellés
                 if ($question->getType() == $donneesFormulaireQuestionRecuperees['type'])
                 {
                    $this->copyTabDataToAttributes($donneesFormulaireQuestionRecuperees,$question);
                 }
                 else // Il faut créer une nouvelle question est effacer l'ancienne
                 {
                   /* On supprime l'ancienne */
                   $em->remove($question);
                   $em->flush();
                   /* On crée la nouvelle question */
                   $question = $this->createQuestionEntityFromFormData($donneesFormulaireQuestionRecuperees);
                 }

                 $em->persist($question);
                 $em->flush();

                 /* Pour qu'après la redirection il y ait un joli petite boîte de validation*/
                 $request->getSession()->getFlashBag()->add('success', 'La question a bien été modifiée.');

                 return $this->redirectToRoute('question_edit', array('id' => $question->getId()));
               }
           }

          return $this->render('ScenarisationProcessBundle:question:edit.html.twig', array(
              'question'    => $question,
              'edit_form'   => $editForm->createView(),
              'delete_form' => $deleteForm->createView(),
          ));
      }
      else
      {
        /* Message d'erreur, à traduire */
        $request->getSession()->getFlashBag()->add('alert', 'Impossible de modifier la question car elle possède déjà une réponse dans une démarche.');

        return $this->redirectToRoute('question_index');
      }
    }

    /**
     * Deletes a question entity.
     *
     */
    public function deleteAction(Request $request, Question $question)
    {
        $form = $this->createDeleteForm($question);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($question);
            $em->flush();
        }

        /* Petit message de reussite, à traduire */
        $request->getSession()->getFlashBag()->add('success', "La question a bien été supprimée.");
        return $this->redirectToRoute('question_index');
    }

    /**
     * Clone a question
     *
     */
    public function cloneAction(Request $request, Question $questionToClone)
    {
      $em = $this->container->get('doctrine')->getEntityManager('default');

      // On crée la nouvelle étape de scénarisation
      $newQuestion = clone $questionToClone;

      // On met à jour les attributs
      $newQuestion->setFrWording("Copie - " . $questionToClone->getFrWording());
      $newQuestion->setEnWording("Copy - "  . $questionToClone->getEnWording());
      $newQuestion->setEsWording("Copia - " . $questionToClone->getEsWording());

      $em->persist($newQuestion);
      $em->flush();

      /* Message de reussite, à traduire */
      $request->getSession()->getFlashBag()->add('success', 'La question a bien été clonée !');
      return $this->redirectToRoute('question_index');
    }

    /**
     * Creates a form to delete a question entity.
     *
     * @param Question $question The question entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Question $question)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('question_delete', array('id' => $question->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    private function questionFormIsValid($questionForm)
    {
      if (  isset($questionForm['enWording']) &&
            isset($questionForm['frWording']) &&
            isset($questionForm['esWording']) &&
            isset($questionForm['type']) )
      {

        if (  !empty($questionForm['enWording']) &&
              !empty($questionForm['frWording']) &&
              !empty($questionForm['esWording']) &&
              !empty($questionForm['type']))
        {

          if ($questionForm['type'] == "sessionQuestion" ||
              $questionForm['type'] == "sequenceQuestion" ||
              $questionForm['type'] == "generalQuestion")
          {
            return true;
          }

        }

      }
      else
      {
        return false;
      }
    }

    private function createQuestionForm($data)
    {
      $form   = $this->createFormBuilder($data);
      $form
            ->add('enWording',TextareaType::class, array("label" => "enWording", "data" => (isset($data['enWording'])?$data['enWording']:'')))
            ->add('frWording',TextareaType::class, array("label" => "frWording", "data" => (isset($data['frWording'])?$data['frWording']:'')))
            ->add('esWording',TextareaType::class, array("label" => "esWording", "data" => (isset($data['esWording'])?$data['esWording']:'')))
            ->add('type', ChoiceType::class, array(
                            'choices' => array(
                              'generalQuestion' => 'generalQuestion',
                              'sequenceQuestion'   => 'sequenceQuestion',
                              'sessionQuestion' => 'sessionQuestion'
                            ),
                            'expanded' => true,
                            'multiple' => false,
                            'data' => (isset($data['type'])?$data['type']:'')

                          ));

        return $form->getForm();
    }

    private function createQuestionEntityFromFormData($data)
    {
      switch ($data['type'])
      {
        case 'generalQuestion':
          $question = new GeneralQuestion();
          break;
        case 'sequenceQuestion':
          $question = new SequenceQuestion();
          break;
        case 'sessionQuestion' :
          $question = new SessionQuestion();
          break;
      }

      $this->copyTabDataToAttributes($data,$question);

      return $question;
    }

    /* Cette fonction est crée pour éviter la duplication de code (utilisée 2 fois)
       Elle permet simplement de copier les données des questions dans le tableau, au sein
       des attributs d'une question. */
    private function copyTabDataToAttributes($tabContainingDatas, $questionToUpdate)
    {
      $questionToUpdate->setEnWording($tabContainingDatas['enWording']);
      $questionToUpdate->setFrWording($tabContainingDatas['frWording']);
      $questionToUpdate->setEsWording($tabContainingDatas['esWording']);
    }

    private function updateMenu()
    {
      // Mise en évidence du menu, changement du menu sélectionné
      $menu = array('pedagogical_scenario' => "",
                    'projects'             => "",
                    'trainingCourse'       => "",
                    'user'                 => "",
                    'parameters'           => "",
                    'scenarisation_process'=> "",
                    'scenarisation_stage'  => "",
                    'question'             => "list-group-item-info");

      // Mise en session du menu
      $this->get('session')->set('menu',$menu);
    }

    private function createBreadcrumb($currentAction, $question = null)
    {
      $breadcrumbs = $this->get("white_october_breadcrumbs");
      switch($currentAction)
      {
        case "index":
          $breadcrumbs->addItem("question");
        break;

        case "show":
          $breadcrumbs->addRouteItem("question", "question_index");
          $breadcrumbs->addItem("show");
        break;

        case "new":
          $breadcrumbs->addRouteItem("question", "question_index");
          $breadcrumbs->addItem("question.new");
        break;

        case "edit":
          $breadcrumbs->addRouteItem("question", "question_index");
          $breadcrumbs->addItem("edit");

        break;
      }
    }

    private function haveAlreadyAnAnswer(Question $question)
    {
      /* On part du principe qu'il n'y a pas de réponse initialisée à la démarche */
      $answerInitializedFound = false;

      // Toutes les réponses
      foreach ($question->getAnswers() as $key => $currentAnswer)
      {
        // Si on tombe sur une vraie réponse correctement initialisée
        if($currentAnswer != null)
        {
          // On change l'état du booléen
          $answerInitializedFound = true;
          // On sort de la boucle
          break;
        }
      }

      // On retourne le booléen : Est ce qu'on a trouvé une réponse ou non
      return $answerInitializedFound;
    }
}
