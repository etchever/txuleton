<?php

namespace ScenarioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SequenceAnswer
 *
 * @ORM\Table(name="sequence_answer")
 * @ORM\Entity(repositoryClass="ScenarioBundle\Repository\SequenceAnswerRepository")
 */
class SequenceAnswer extends Answer
{

  /**
   * @var int
   *
   * @ORM\Column(name="id", type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $id;

  /**
   * @ORM\ManyToOne(targetEntity="ScenarisationProcessBundle\Entity\SequenceQuestion", inversedBy="sequenceAnswers", cascade={"persist"})
   * @ORM\JoinColumn(nullable=false)
   */
  private $sequenceQuestion;

  /**
   * @var array
   *
   * @ORM\Column(name="sequencearray", type="array", nullable=true)
   */
  private $sequencesArray;

  /**
   * @ORM\OneToMany(targetEntity="ScenarioBundle\Entity\Sequence", mappedBy="sequenceAnswer", cascade={"persist"})
   * @ORM\JoinColumn(nullable=true)
   */
  private $sequences;

  /**
   * Get id
   *
   * @return int
   */
  public function getId()
  {
      return $this->id;
  }

  /**
   * Set sequenceQuestion
   *
   * @param \ScenarisationProcessBundle\Entity\SequenceQuestion $sequenceQuestion
   *
   * @return SequenceAnswer
   */
  public function setSequenceQuestion(\ScenarisationProcessBundle\Entity\SequenceQuestion $sequenceQuestion)
  {
    $this->sequenceQuestion = $sequenceQuestion;

    $sequenceQuestion->addSequenceAnswer($this);

    return $this;
  }

  /**
   * Get sequenceQuestion
   *
   * @return \ScenarisationProcessBundle\Entity\SequenceQuestion
   */
  public function getSequenceQuestion()
  {
      return $this->sequenceQuestion;
  }

  /**
   * Add sequence
   *
   * @param \ScenarioBundle\Entity\Sequence $sequence
   *
   * @return SequenceAnswer
   */
  public function addSequence(\ScenarioBundle\Entity\Sequence $sequence)
  {
      $this->sequences[] = $sequence;
      $sequence->setSequenceAnswer($this);

      return $this;
  }

  /**
   * Remove sequence
   *
   * @param \ScenarioBundle\Entity\Sequence $sequence
   */
  public function removeSequence(\ScenarioBundle\Entity\Sequence $sequence)
  {
      $this->sequences->removeElement($sequence);
  }

  /**
   * Get sequences
   *
   * @return \Doctrine\Common\Collections\Collection
   */
  public function getSequences()
  {
      return $this->sequences;
  }

  /**
   * Get sequencesArray
   *
   * @return array
   */
  public function getSequencesArray()
  {
    return $this->sequencesArray;
  }

  /**
   * Set sequencesArray
   *
   * @param Array $sequencesArray
   *
   * @return SequencesArray
   */
  public function setSequencesArray($sequencesArray)
  {
    $this->sequencesArray = $sequencesArray;

    return $this;
  }

}
