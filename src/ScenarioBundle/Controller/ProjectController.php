<?php

namespace ScenarioBundle\Controller;

use ScenarioBundle\Entity\Project;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

// To use Security annotations on the top of fuctions
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Project controller.
 *
 */
class ProjectController extends Controller
{
    /**
     * Lists all project entities.
     *
     */
    public function indexAction(Request $request)
    {
      // We generate the new breadcrumb
      $this->createBreadcrumb("index");
      // Mise en évidence du menu, changement du menu sélectionné
      $this->updateMenu();


        $em = $this->getDoctrine()->getManager();

        //$projects = $em->getRepository('ScenarioBundle:Project')->findAll();

        $queryBuilder = $em->getRepository('ScenarioBundle:Project')->createQueryBuilder('projects');
        $query = $queryBuilder->getQuery();

        $paginator  = $this->get('knp_paginator');

        $projects = $paginator->paginate(
          $query,
          $request->query->getInt('page', 1)/*page number*/,
          5/*limit per page*/
        );

        return $this->render('ScenarioBundle:project:index.html.twig', array(
            'projects' => $projects
        ));
    }

    /**
     * Creates a new project entity.
     * @Security("has_role('ROLE_PEDAGOGICAL_ENGINEER')")
     */
    public function newAction(Request $request)
    {
      // We generate the new breadcrumb
      $this->createBreadcrumb("new");
      // Mise en évidence du menu, changement du menu sélectionné
      $this->updateMenu();

        $project = new Project();
        $form = $this->createForm('ScenarioBundle\Form\ProjectType', $project);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($project);
            $em->flush();

            return $this->redirectToRoute('project_show', array('id' => $project->getId(), 'userRole' => "pedagogical-engineer"));
        }

        return $this->render('ScenarioBundle:project:new.html.twig', array(
            'project' => $project,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a project entity.
     *
     */
    public function showAction(Project $project)
    {
        // We generate the new breadcrumb
        $this->createBreadcrumb("show",$project);
        // Mise en évidence du menu, changement du menu sélectionné
        $this->updateMenu();

        $deleteForm = $this->createDeleteForm($project);

        return $this->render('ScenarioBundle:project:show.html.twig', array(
            'project' => $project,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing project entity.
     * @Security("has_role('ROLE_PEDAGOGICAL_ENGINEER')")
     */
    public function editAction(Request $request, Project $project)
    {
      // We generate the new breadcrumb
      $this->createBreadcrumb("edit",$project);
      // Mise en évidence du menu, changement du menu sélectionné
      $this->updateMenu();

        $deleteForm = $this->createDeleteForm($project);
        $editForm = $this->createForm('ScenarioBundle\Form\ProjectType', $project);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('project_edit', array('id' => $project->getId(), 'userRole' => "pedagogical-engineer"));
        }

        return $this->render('ScenarioBundle:project:edit.html.twig', array(
            'project' => $project,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a project entity.
     * @Security("has_role('ROLE_PEDAGOGICAL_ENGINEER')")
     */
    public function deleteAction(Request $request, Project $project)
    {
        $form = $this->createDeleteForm($project);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($project);
            $em->flush();
        }

        /* Petit message de reussite, à traduire */
        $request->getSession()->getFlashBag()->add('success', "Le projet '" . $project->getShortname() . "' a bien été supprimé.");
        return $this->redirectToRoute('project_index', array('userRole' => 'pedagogical-engineer'));
    }

    /**
     * Creates a form to delete a project entity.
     *
     * @param Project $project The project entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Project $project)
    {
      if ($this->get('security.context')->isGranted('ROLE_PEDAGOGICAL_ENGINEER'))
      {
        $currentRole = 'pedagogical-engineer';
      }
      else if ($this->get('security.context')->isGranted('ROLE_TEACHER'))
      {
        $currentRole = 'teacher';
      }

      return $this->createFormBuilder()
          ->setAction($this->generateUrl('project_delete', array('id' => $project->getId(), 'userRole'=> $currentRole )))
          ->setMethod('DELETE')
          ->getForm()
      ;
    }

    public function pedagogicalScenarioManagementAction(Project $project)
    {
      // We generate the new breadcrumb
      $this->createBreadcrumb("pedagogical_scenario_management",$project);
      // Mise en évidence du menu, changement du menu sélectionné
      $this->updateMenu();

        $em = $this->getDoctrine()->getManager();

        /*$queryBuilder = $em->getRepository('ScenarioBundle:PedagogicalScenario')->createQueryBuilder('pedagogicalScenarios');
        $query = $queryBuilder->getQuery();

        $paginator  = $this->get('knp_paginator');

        $pedagogicalScenarios = $paginator->paginate(
          $query,
          $request->query->getInt('page', 1), // page number
          5 // limit per page
        );*/

        // On récupère la liste des scénarios pédagogiques qui n'ont pas pour projet le projet courant
        $dql_pedagogicalScenariosAvailable = $em->createQuery ('
        select ps
        FROM ScenarioBundle:PedagogicalScenario ps
        WHERE ps.project != :currentProject or ps.project is null');
        $dql_pedagogicalScenariosAvailable->setParameter('currentProject', $project);

        // On récupère la liste des scénarios pédagogiques qui ont pour projet le projet courant
        $dql_pedagogicalScenariosAlreadyLinked = $em->createQuery ('
        select ps
        FROM ScenarioBundle:PedagogicalScenario ps
        WHERE ps.project = :currentProject');
        $dql_pedagogicalScenariosAlreadyLinked->setParameter('currentProject', $project);

        $pedagogicalScenariosAvailable = $dql_pedagogicalScenariosAvailable->getResult();
        $pedagogicalScenariosAlreadyLinked = $dql_pedagogicalScenariosAlreadyLinked->getResult();

          return $this->render('ScenarioBundle:project:pedagogicalscenariomanagement.html.twig', array(
              'pedagogicalScenariosAvailable' => $pedagogicalScenariosAvailable,
              'pedagogicalScenariosAlreadyLinked' => $pedagogicalScenariosAlreadyLinked,
              'project' => $project
          ));
    }

    private function updateMenu()
    {
      // Mise en évidence du menu, changement du menu sélectionné
      $menu = array('pedagogical_scenario' => "",
                    'projects'             => "list-group-item-info",
                    'trainingCourse'               => "",
                    'user'                 => "",
                    'parameters'           => "",
                    'scenarisation_process'=> "",
                    'scenarisation_stage'  => "",
                    'question'             => "");

      // Mise en session du menu
      $this->get('session')->set('menu',$menu);
    }

    private function createBreadcrumb($currentAction, $project = null)
    {
      $breadcrumbs = $this->get("white_october_breadcrumbs");
      $breadcrumbs->addItem("admin");
      switch($currentAction)
      {
        case "index":
          $breadcrumbs->addItem("projects");
        break;

        case "show":
          $breadcrumbs->addRouteItem("projects", "project_index", [
              'userRole' => ($this->get('security.authorization_checker')->isGranted('ROLE_PEDAGOGICAL_ENGINEER')?"pedagogical-engineer":"teacher")
          ]);
          $breadcrumbs->addItem($project->getShortname());
          $breadcrumbs->addItem("show");

        break;

        case "new":
          $breadcrumbs->addRouteItem("projects", "project_index", [
              'userRole' => ($this->get('security.authorization_checker')->isGranted('ROLE_PEDAGOGICAL_ENGINEER')?"pedagogical-engineer":"teacher")
          ]);
          $breadcrumbs->addItem("project.new");
        break;

        case "edit":
          $breadcrumbs->addRouteItem("projects", "project_index", [
              'userRole' => ($this->get('security.authorization_checker')->isGranted('ROLE_PEDAGOGICAL_ENGINEER')?"pedagogical-engineer":"teacher")
          ]);
          $breadcrumbs->addItem($project->getShortname());
          $breadcrumbs->addItem("edit");

        break;

        case "pedagogical_scenario_management":
          $breadcrumbs->addRouteItem("projects", "project_index", [
              'userRole' => ($this->get('security.authorization_checker')->isGranted('ROLE_PEDAGOGICAL_ENGINEER')?"pedagogical-engineer":"teacher")
          ]);
          $breadcrumbs->addRouteItem($project->getShortname(), "project_show", [
              'userRole' => ($this->get('security.authorization_checker')->isGranted('ROLE_PEDAGOGICAL_ENGINEER')?"pedagogical-engineer":"teacher"),
              'id'       => $project->getId()
          ]);
          $breadcrumbs->addItem("associatedScenarios");
        break;
      }
    }
}
