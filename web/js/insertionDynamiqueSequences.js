$(document).ready(function() {
    // On récupère la balise <div>qui contient l'attribut « data-prototype » qui nous intéresse.
    var $codeHtmlFormulaireSequence = $('div#sequenceQuestion').children().children();

    // On ajoute un lien pour ajouter une nouvelle sequence
    //var $lienAjoutSequence = $('<a href="#" id="add_sequence">Ajouter une séquence</a>');
    //$codeHtmlFormulaireSequence.append($lienAjoutSequence);

    // On ajoute un nouveau champ auteur à chaque clic sur le lien d'ajout.
    $lienAjoutSequence.click(function(e) {
        addSequence($codeHtmlFormulaireSequence);
        e.preventDefault(); // évite qu'un # apparaisse dans l'URL
        return false;
    });

    // On définit un compteur unique pour nommer les champs Auteur qu'on va ajouter dynamiquement
    var compteurSequences = $codeHtmlFormulaireSequence.find(':input').length;

    // On ajoute le code html d'un premier formulaire Sequence s'il n'en existe pas déjà un
    if (compteurSequences == 0) {
        addSequence($codeHtmlFormulaireSequence);
    } else {
        // Pour chaque formulaire sequence affiché, on ajoute un lien de suppression
        $codeHtmlFormulaireSequence.children('div').each(function() {
            ajouterLienSuppression($(this));
        });
    }

    // La fonction qui ajoute un formulaire Auteur
    /*function addSequence($codeHtmlFormulaireSequence) {
        // Dans le contenu de l'attribut « data-prototype », on remplace :
        // - le texte "__name__label__" qu'il contient par notre label (ici un label vide pour ne pas surcharger le formulaire)
        // - le texte "__name__" qu'il contient par le numéro du champ
        var $codeHtmlNouvelleSequence = $($codeHtmlFormulaireSequence.attr('data-prototype').replace(/__name__label__/g, ' ')
                                      .replace(/__name__/g, compteurSequences));

        // On ajoute au code html un lien pour pouvoir supprimer l'auteur
        ajouterLienSuppression($codeHtmlNouvelleSequence);

        // On ajoute le code html qu'on vient de modifier à la fin de la balise <div>
        $codeHtmlFormulaireSequence.append($codeHtmlNouvelleSequence);

        // On incrémente le compteur pour que le prochain ajout se fasse avec un autre numéro
        compteurSequences++;
    }*/

    // La fonction qui ajoute un lien de suppression d'un auteur
    /*function ajouterLienSuppression($codeHtmlNouvelleSequence) {
        // Création du lien
        $lienSuppression = $('<a href="#">Supprimer cette séquence</a>');

        // Ajout du lien
        $codeHtmlNouvelleSequence.append($lienSuppression);

        // Ajout du listener sur le clic du lien
        $lienSuppression.click(function(e) {
            $codeHtmlNouvelleSequence.remove();
            e.preventDefault(); // évite qu'un # apparaisse dans l'URL
            return false;
        });
    }*/
});
