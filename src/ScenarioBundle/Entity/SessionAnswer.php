<?php

namespace ScenarioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SessionAnswer
 *
 * @ORM\Table(name="session_answer")
 * @ORM\Entity(repositoryClass="ScenarioBundle\Repository\SessionAnswerRepository")
 */
class SessionAnswer extends Answer
{
  /**
   * @var int
   *
   * @ORM\Column(name="id", type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $id;

  /**
   * @ORM\ManyToOne(targetEntity="ScenarisationProcessBundle\Entity\SessionQuestion", inversedBy="sessionAnswers", cascade={"persist"})
   * @ORM\JoinColumn(nullable=false)
   */
  private $sessionQuestion;

  /**
   * @var array
   *
   * @ORM\Column(name="sessionsarray", type="array", nullable=true)
   */
  private $sessionsArray;

  /**
   * @ORM\OneToMany(targetEntity="ScenarioBundle\Entity\Session", mappedBy="sessionAnswer", cascade={"persist"})
   * @ORM\JoinColumn(nullable=true)
   */

  private $sessions;

  /**
   * Get id
   *
   * @return int
   */
  public function getId()
  {
      return $this->id;
  }

  /**
   * Set sessionQuestion
   *
   * @param \ScenarisationProcessBundle\Entity\SessionQuestion $sessionQuestion
   *
   * @return SessionAnswer
   */
  public function setSessionQuestion(\ScenarisationProcessBundle\Entity\SessionQuestion $sessionQuestion)
  {
    $this->sessionQuestion = $sessionQuestion;

    $sessionQuestion->addSessionAnswer($this);

    return $this;
  }

  /**
   * Get sessionQuestion
   *
   * @return \ScenarisationProcessBundle\Entity\SessionQuestion
   */
  public function getSessionQuestion()
  {
      return $this->sessionQuestion;
  }

  /**
   * Add seance
   *
   * @param \ScenarioBundle\Entity\Session $session
   *
   * @return SessionAnswer
   */
  public function addSession(\ScenarioBundle\Entity\Session $session)
  {
      $this->sessions[] = $session;
      $session->setSessionAnswer($this);

      return $this;
  }

  /**
   * Remove seance
   *
   * @param \ScenarioBundle\Entity\Session $session
   */
  public function removeSession(\ScenarioBundle\Entity\Session $session)
  {
      $this->sessions->removeElement($session);
  }

  /**
   * Get sessions
   *
   * @return \Doctrine\Common\Collections\Collection
   */
  public function getSessions()
  {
      return $this->sessions;
  }

  /**
   * Get sessionsArray
   *
   * @return array
   */
  public function getSessionsArray()
  {
    return $this->sessionsArray;
  }

  /**
   * Set sessionsArray
   *
   * @param Array $sessionsArray
   *
   * @return SessionsArray
   */
  public function setSessionsArray($sessionsArray)
  {
    $this->sessionsArray = $sessionsArray;

    return $this;
  }

}
