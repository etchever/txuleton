<?php
// src/UserBundle/Controller/SecurityController.php;

namespace UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;

use UserBundle\Entity\User;

class SecurityController extends Controller
{
  public function loginAction(Request $request)
  {

    // Si le visiteur est déjà identifié, on le redirige vers l'accueil
    if ($this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED'))
    {
        // Par défaut, la langue est l'espagnol
        $language = 'es';
        /* On tente de récupérer la langue de l'utilisateur courant */
        $usr = $this->get('security.context')->getToken()->getUser();
        if ($usr->getPlateformLanguage() == 'es' || $usr->getPlateformLanguage() == 'en' || $usr->getPlateformLanguage() == 'fr')
        {
         $language = $usr->getPlateformLanguage();
        }

        if ($this->get('security.context')->isGranted('ROLE_PEDAGOGICAL_ENGINEER'))
        {
         return $this->redirect($this->generateUrl('pedagogicalscenario_index', array('_locale' => $language,'userRole'=> 'pedagogical-engineer')));
        }
        else
        {
         return $this->redirect($this->generateUrl('pedagogicalscenario_index', array('_locale' => $language,'userRole' => 'teacher')));
        }
    }

    // Le service authentication_utils permet de récupérer le nom d'utilisateur
    // et l'erreur dans le cas où le formulaire a déjà été soumis mais était invalide
    // (mauvais mot de passe par exemple)
    $authenticationUtils = $this->get('security.authentication_utils');

    return $this->render('UserBundle:Security:login.html.twig', array(
      'last_username' => $authenticationUtils->getLastUsername(),
      'error'         => $authenticationUtils->getLastAuthenticationError(),
    ));
  }

  public function redirectToLoginAction()
  {
    /* Si l'utilisateur est déjà authentifié, on le redirige vers la vue du login standart avec sa langue
    Cette dernière se chargera de le rediriger automatiquement vers la bonne pas */
    if ($this->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED'))
    {
      $usr = $this->get('security.context')->getToken()->getUser();
      return $this->redirect($this->generateUrl('login', array('_locale' => $usr->getPlateformLanguage())));
    }
    else
    {
      return $this->redirect($this->generateUrl('login', array('_locale' => 'es')));
    }
  }

  /*
   * This method allow a user to redefine his password.
   */
  public function redefinePasswordAction(Request $request, $token)
  {
    $em = $this->getDoctrine()->getManager();

    // Vérifier que le token est bon
    $utilisateur = $em->getRepository('UserBundle:User')->getUserFromToken($token);

    /* Si c'est bon, on va afficher le formulaire */
    if ($utilisateur != null)
    {
      /* On construit un tableau dans lequel les données du formulaire
      seront recueillies */
      $tabMotsDePasse = array();

      /* On créé un constructeur de formulaires qui pourra fabriquer des
      formulaires capables de remplir $tabMotsDePasse */
      $formulaireRedefinitionMotDePasse = $this->createFormBuilder($tabMotsDePasse)
      ->add('newPassword', 'password')
      ->add('repeatNewPassword', 'password')
      ->getForm();

      // Enregistrement des données dans $tabMotsDePasse après soumission

      $formulaireRedefinitionMotDePasse->handleRequest($request);

      if ($formulaireRedefinitionMotDePasse->isSubmitted())
      {
          /* on récupère les données du formulaire dans un tableau de 2 cases
             indicées par 'newPassword' et 'repeatNewPassword' */
          $tabMotsDePasse = $formulaireRedefinitionMotDePasse->getData();

          /* Si un des deux champs est vide */
          if ($tabMotsDePasse['newPassword'] == NULL || $tabMotsDePasse['repeatNewPassword'] == NULL)
          {
            /* Petit message de reussite, à traduire */
            $request->getSession()->getFlashBag()->add('alert', "Les deux champs doivent être complétés.");
          }
          else
          {
            /* si les mots de passe sont différents*/
            if ($tabMotsDePasse['newPassword'] != $tab['repeatNewPassword'])
            {
              /* Petit message de reussite, à traduire */
              $request->getSession()->getFlashBag()->add('alert', "Les mots de passe saisis ne sont pas identiques. Veuillez recommencer.");
            }
            else
            {
              /* On met à jour le mot de passe */
              $encoder        = $this->container->get('security.encoder_factory')->getEncoder($utilisateur);
              $passwordEncode = $encoder->encodePassword($tabMotsDePasse['newPassword'], $utilisateur->getSalt());
              $utilisateur->setPassword($passwordEncode);

              /* On supprime le jeton puisqu'il vient d'être utilisé */
              $utilisateur->removeToken();

              /* On enrgistre toutes les modifications en base de données */
              $em = $this->getDoctrine()->getManager();
              $em->persist($utilisateur);
              $em->flush();

              /* Petit message de reussite, à traduire */
              $request->getSession()->getFlashBag()->add('success', "Le mot de passe a bien été modifié, vous pouvez vous connecter.");
              return $this->redirectToRoute("login");
            }
          }
      }

      return $this->render('UserBundle:user:redefine_password.html.twig', array('formulaireRedefinitionMotDePasse' => $formulaireRedefinitionMotDePasse->createView()));
    }
    /* Si le token est mauvais, on redirige vers la sortie qui réaffichera la page de connexion [ARBITRAIRE] */
    else
    {
      return $this->redirectToRoute('logout');
    }

  }
}
