<?php

namespace ScenarioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PedagogicalScenarioTrainingCourse
 *
 * @ORM\Table(name="pedagogical_scenario_training_course", uniqueConstraints={@ORM\UniqueConstraint(name="pedagogical_scenario_already_include", columns={"training_course_id", "pedagogical_scenario_id"})})
 * @ORM\Entity(repositoryClass="ScenarioBundle\Repository\PedagogicalScenarioTrainingCourseRepository")
 */
class PedagogicalScenarioTrainingCourse
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="ScenarioBundle\Entity\PedagogicalScenario", inversedBy="pedagogicalScenarioTrainingCourse")
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    private $pedagogicalScenario;

    /**
     * @ORM\ManyToOne(targetEntity="ScenarioBundle\Entity\TrainingCourse", inversedBy="pedagogicalScenarioTrainingCourse")
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    private $trainingCourse;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pedagogicalScenario
     *
     * @param \ScenarioBundle\Entity\PedagogicalScenario $pedagogicalScenario
     *
     * @return PedagogicalScenarioTrainingCourse
     */
    public function setPedagogicalScenario(\ScenarioBundle\Entity\PedagogicalScenario $pedagogicalScenario)
    {
        $this->pedagogicalScenario = $pedagogicalScenario;

        return $this;
    }

    /**
     * Get pedagogicalScenario
     *
     * @return \ScenarioBundle\Entity\PedagogicalScenario
     */
    public function getPedagogicalScenario()
    {
        return $this->pedagogicalScenario;
    }

    /**
     * Set trainingCourse
     *
     * @param \ScenarioBundle\Entity\TrainingCourse $trainingCourse
     *
     * @return PedagogicalScenarioTrainingCourse
     */
    public function setTrainingCourse(\ScenarioBundle\Entity\TrainingCourse $trainingCourse)
    {
        $this->trainingCourse = $trainingCourse;

        return $this;
    }

    /**
     * Get trainingCourse
     *
     * @return \ScenarioBundle\Entity\TrainingCourse
     */
    public function getTrainingCourse()
    {
        return $this->trainingCourse;
    }


}
