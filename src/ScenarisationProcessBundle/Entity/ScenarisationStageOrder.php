<?php

namespace ScenarisationProcessBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * ScenarisationStageOrder
 *
 * @ORM\Table(name="scenarisation_stage_order", uniqueConstraints={@ORM\UniqueConstraint(name="unique_stage_in_process", columns={"scenarisation_process_id", "scenarisation_stage_id"})}))
 * @ORM\Entity(repositoryClass="ScenarisationProcessBundle\Repository\ScenarisationStageOrderRepository")
 */
class ScenarisationStageOrder
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     * @Gedmo\Sortable(groups={"scenarisationProcess"})
     * @ORM\Column(name="position", type="smallint")
     */
    private $position;

    /**
     * @ORM\ManyToOne(targetEntity="ScenarisationProcessBundle\Entity\ScenarisationProcess", inversedBy="scenarisationStageOrder", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    private $scenarisationProcess;

    /**
     * @ORM\ManyToOne(targetEntity="ScenarisationProcessBundle\Entity\ScenarisationStage", inversedBy="scenarisationStageOrder", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    private $scenarisationStage;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return ordreQuestion
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set scenarisationProcess
     *
     * @param \ScenarisationProcessBundle\Entity\ScenarisationProcess $scenarisationProcess
     *
     * @return ScenarisationStageOrder
     */
    public function setScenarisationProcess(\ScenarisationProcessBundle\Entity\ScenarisationProcess $scenarisationProcess = null)
    {
        $this->scenarisationProcess = $scenarisationProcess;

        return $this;
    }

    /**
     * Get scenarisationProcess
     *
     * @return \ScenarisationProcessBundle\Entity\ScenarisationProcess
     */
    public function getScenarisationProcess()
    {
        return $this->scenarisationProcess;
    }

    /**
     * Set scenarisationStage
     *
     * @param \ScenarisationProcessBundle\Entity\ScenarisationStage $scenarisationStage
     *
     * @return ScenarisationStageOrder
     */
    public function setScenarisationStage(\ScenarisationProcessBundle\Entity\ScenarisationStage $scenarisationStage = null)
    {
        $this->scenarisationStage = $scenarisationStage;

        return $this;
    }

    /**
     * Get scenarisationStage
     *
     * @return \ScenarisationProcessBundle\Entity\ScenarisationStage
     */
    public function getScenarisationStage()
    {
        return $this->scenarisationStage;
    }
}
