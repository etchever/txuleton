<?php

namespace ScenarisationProcessBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GeneralQuestion
 *
 * @ORM\Table(name="general_question")
 * @ORM\Entity(repositoryClass="ScenarisationProcessBundle\Repository\GeneralQuestionRepository")
 */
class GeneralQuestion extends Question
{
  /**
   * @ORM\OneToMany(targetEntity="ScenarioBundle\Entity\GeneralAnswer", mappedBy="generalQuestion", cascade={"persist"})
   * @ORM\JoinColumn(nullable=false)
   */
  private $generalAnswers;

  /**
   * Add generalAnswer
   *
   * @param \ScenarioBundle\Entity\GeneralAnswer generalAnswer
   *
   * @return GeneralQuestion
   */
  public function addGeneralAnswer(\ScenarioBundle\Entity\GeneralAnswer $generalAnswer)
  {
      $this->generalAnswers[] = $generalAnswer;

      return $this;
  }

  /**
   * Remove generalAnswer
   *
   * @param \ScenarioBundle\Entity\GeneralAnswer $generalAnswer
   */
  public function removeGeneralAnswer(\ScenarioBundle\Entity\GeneralAnswer $generalAnswer)
  {
      $this->generalAnswers->removeElement($generalQuestion);
  }

  /**
   * Get generalAnswers
   *
   * @return \Doctrine\Common\Collections\Collection
   */
  public function getGeneralAnswers()
  {
      return $this->generalAnswers;
  }

  public function getType()
  {
    return 'generalQuestion';
  }
}
