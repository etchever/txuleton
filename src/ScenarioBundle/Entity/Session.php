<?php

namespace ScenarioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Session
 *
 * @ORM\Table(name="session")
 * @ORM\Entity(repositoryClass="ScenarioBundle\Repository\SessionRepository")
 */
class Session
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="goal", type="text")
     */
    private $goal;

    /**
     * @var string
     *
     * @ORM\Column(name="modality", type="string", length=255)
     */
    private $modality;

    /**
     * @ORM\ManyToOne(targetEntity="ScenarioBundle\Entity\Sequence", inversedBy="sessions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $sequence;

    /**
     * @var int
     *
     * @ORM\Column(name="`order`", type="smallint")
     */
    private $order;

    /**
     * @ORM\ManyToOne(targetEntity="ScenarioBundle\Entity\SessionAnswer", inversedBy="sessions", cascade={"persist","remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $sessionAnswer;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Session
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set goal
     *
     * @param string $goal
     *
     * @return Session
     */
    public function setGoal($goal)
    {
        $this->goal = $goal;

        return $this;
    }

    /**
     * Get goal
     *
     * @return string
     */
    public function getGoal()
    {
        return $this->goal;
    }

    /**
     * Set modality
     *
     * @param string $modality
     *
     * @return Session
     */
    public function setModality($modality)
    {
        $this->modality = $modality;

        return $this;
    }

    /**
     * Get modality
     *
     * @return string
     */
    public function getModality()
    {
        return $this->modality;
    }

    /**
     * Set sequence
     *
     * @param \ScenarioBundle\Entity\Sequence $sequence
     *
     * @return Session
     */
    public function setSequence(\ScenarioBundle\Entity\Sequence $sequence)
    {
        $this->sequence = $sequence;

        return $this;
    }

    /**
     * Get sequence
     *
     * @return \ScenarioBundle\Entity\Sequence
     */
    public function getSequence()
    {
        return $this->sequence;
    }

    /**
     * Set order
     *
     * @param int $order
     *
     * @return Question
     */
    public function setOrder($order)
    {
      $this->order = $order;

      return $this;
    }

    /**
     * Get order
     *
     * @return int
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set sessionAnswer
     *
     * @param \ScenarioBundle\Entity\SessionAnswer $sessionAnswer
     *
     * @return SessionAnswer
     */
    public function setSessionAnswer(\ScenarioBundle\Entity\SessionAnswer $sessionAnswer)
    {
        $this->sessionAnswer = $sessionAnswer;

        return $this;
    }

    /**
     * Get sessionAnswer
     *
     * @return \ScenarioBundle\Entity\SessionAnswer $sessionAnswer
     */
    public function getSessionAnswer()
    {
        return $this->sessionAnswer;
    }
}
