<?php

namespace ScenarioBundle\Controller;

use ScenarioBundle\Entity\TrainingCourse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use ScenarioBundle\Entity\PedagogicalScenario;
use ScenarioBundle\Entity\PedagogicalScenarioTrainingCourse;

/**
 * TrainingCourse controller.
 *
 */
class TrainingCourseController extends Controller
{
    /**
     * Lists all  entities.
     *
     */
    public function indexAction(Request $request)
    {
      // We generate the new breadcrumb
      $this->createBreadcrumb("index");
      // Mise en évidence du menu, changement du menu sélectionné
      $this->updateMenu();

      $em = $this->getDoctrine()->getManager();

      $queryBuilder = $em->getRepository('ScenarioBundle:TrainingCourse')->createQueryBuilder('trainingCourses');
      $query = $queryBuilder->getQuery();

      $paginator  = $this->get('knp_paginator');

      $trainingCourses = $paginator->paginate(
        $query,
        $request->query->getInt('page', 1)/*page number*/,
        5/*limit per page*/
      );

      return $this->render('ScenarioBundle:trainingCourse:index.html.twig', array(
          'trainingCourses' => $trainingCourses
      ));
    }

    /**
     * Creates a new trainingCourse entity.
     *
     */
    public function newAction(Request $request)
    {
      // We generate the new breadcrumb
      $this->createBreadcrumb("new");
      // Mise en évidence du menu, changement du menu sélectionné
      $this->updateMenu();

        $trainingCourse = new TrainingCourse();
        $form = $this->createForm('ScenarioBundle\Form\TrainingCourseType', $trainingCourse);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($trainingCourse);
            $em->flush();

            return $this->redirectToRoute('training_course_show', array('id' => $trainingCourse->getId(), 'userRole' => "pedagogical-engineer"));
        }

        return $this->render('ScenarioBundle:trainingCourse:new.html.twig', array(
            'trainingCourse' => $trainingCourse,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a trainingCourse entity.
     *
     */
    public function showAction(TrainingCourse $trainingCourse)
    {
      // We generate the new breadcrumb
      $this->createBreadcrumb("show", $trainingCourse);
      // Mise en évidence du menu, changement du menu sélectionné
      $this->updateMenu();

        $deleteForm = $this->createDeleteForm($trainingCourse);

        return $this->render('ScenarioBundle:trainingCourse:show.html.twig', array(
            'trainingCourse' => $trainingCourse,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing trainingCourse entity.
     *
     */
    public function editAction(Request $request, TrainingCourse $trainingCourse)
    {
      // We generate the new breadcrumb
      $this->createBreadcrumb("edit", $trainingCourse);
      // Mise en évidence du menu, changement du menu sélectionné
      $this->updateMenu();

        $deleteForm = $this->createDeleteForm($trainingCourse);
        $editForm = $this->createForm('ScenarioBundle\Form\TrainingCourseType', $trainingCourse);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('training_course_edit', array('id' => $trainingCourse->getId(), 'userRole' => "pedagogical-engineer"));
        }

        return $this->render('ScenarioBundle:trainingCourse:edit.html.twig', array(
            'trainingCourse' => $trainingCourse,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'userRole' => "pedagogical-engineer"
        ));
    }

    /**
     * Deletes a trainingCourse entity.
     *
     */
    public function deleteAction(Request $request, TrainingCourse $trainingCourse)
    {
        $form = $this->createDeleteForm($trainingCourse);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($trainingCourse);
            $em->flush();
        }

        /* Petit message de reussite, à traduire */
        $request->getSession()->getFlashBag()->add('success', "La formation '" . $trainingCourse->getShortname() . "' a bien été supprimée.");
        return $this->redirectToRoute('training_course_index', array('userRole' => 'pedagogical-engineer'));
    }

    /**
     * Creates a form to delete a trainingCourse entity.
     *
     * @param TrainingCourse $trainingCourse The trainingCourse entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TrainingCourse $trainingCourse)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('training_course_delete', array('id' => $trainingCourse->getId(), 'userRole' => "pedagogical-engineer")))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     * Permit to manage the pedagogical scenarios of a TrainingCourse
     *
     * @param TrainingCourse $trainingCourse The trainingCourse entity
     *
     */
    public function pedagogicalScenarioManagementAction(TrainingCourse $trainingCourse)
    {  // We generate the new breadcrumb
      $this->createBreadcrumb("pedagogical_scenario_management",$trainingCourse);
      // Mise en évidence du menu, changement du menu sélectionné
      $this->updateMenu();

        $em = $this->getDoctrine()->getManager();

        /*$queryBuilder = $em->getRepository('ScenarioBundle:PedagogicalScenario')->createQueryBuilder('pedagogicalScenarios');
        $query = $queryBuilder->getQuery();

        $paginator  = $this->get('knp_paginator');

        $pedagogicalScenarios = $paginator->paginate(
          $query,
          $request->query->getInt('page', 1), // page number
          5 // limit per page
        );*/

        // On récupère la liste des scénarios pédagogiques de la trainingCourse courante
        $scenariosPedagogiquesAlreadyLinked = $em->createQuery ('
        select ps, psf, f
        FROM ScenarioBundle:PedagogicalScenario ps
        JOIN ps.pedagogicalScenarioTrainingCourse psf
        JOIN psf.trainingCourse f
        WHERE psf.trainingCourse = :trainingCourse');
        $scenariosPedagogiquesAlreadyLinked->setParameter('trainingCourse', $trainingCourse);

        // On récupère la liste des scénario pédagogiques qui ne sont pas dans la trainingCourse courante
        $scenariosPedagogiquesAvailable = $em->createQuery ('
        select pss
        FROM ScenarioBundle:PedagogicalScenario pss
        WHERE pss NOT IN (select ps
        FROM ScenarioBundle:PedagogicalScenario ps
        JOIN ps.pedagogicalScenarioTrainingCourse psf
        JOIN psf.trainingCourse f
        WHERE psf.trainingCourse = :trainingCourse)');
        $scenariosPedagogiquesAvailable->setParameter('trainingCourse', $trainingCourse);

        $pedagogicalScenariosAvailable     = $scenariosPedagogiquesAvailable->getResult();
        $pedagogicalScenariosAlreadyLinked = $scenariosPedagogiquesAlreadyLinked->getResult();

        return $this->render('ScenarioBundle:trainingCourse:pedagogicalscenariomanagement.html.twig', array(
            'pedagogicalScenariosAvailable' => $pedagogicalScenariosAvailable,
            'pedagogicalScenariosAlreadyLinked' => $pedagogicalScenariosAlreadyLinked,
            'trainingCourse' => $trainingCourse
        ));
    }

    /**
     * Add a PedagogicalScenario in a TrainingCourse
     *
     * @param TrainingCourse $trainingCourse The trainingCourse entity
     * @param TrainingCourse $trainingCourse The pedagogical scenario entity
     */
    public function addPedagogicalScenarioAction(TrainingCourse $trainingCourse, PedagogicalScenario $pedagogicalScenario)
    {
        $em = $this->getDoctrine()->getManager();

        $pedagogicalScenarioTrainingCourse = new PedagogicalScenarioTrainingCourse();

        $trainingCourse->addPedagogicalScenarioTrainingCourse($pedagogicalScenarioTrainingCourse);
        $pedagogicalScenario->addPedagogicalScenarioTrainingCourse($pedagogicalScenarioTrainingCourse);

        $em->persist($pedagogicalScenarioTrainingCourse);
        $em->flush();

        // Cette méthode est également utilisée par le Scénario Pédagogique
        // Il faut dont renvoyer vers la bonne vue
        $request = $this->container->get('request');
        $routeName = $request->get('_route');

        if ($routeName == 'pedagogicalscenario_training_course_add')
          return $this->redirectToRoute('pedagogicalscenario_training_course_management', array('userRole' => 'pedagogical-engineer', 'id' => $pedagogicalScenario->getId()));
        else
          return $this->redirectToRoute('training_course_pedagogical_scenario_management', array('userRole' => 'pedagogical-engineer', 'id' => $trainingCourse->getId()));
    }

    /**
     * Delete a PedagogicalScenario from a TrainingCourse
     *
     * @param TrainingCourse $trainingCourse The trainingCourse entity
     * @param TrainingCourse $trainingCourse The pedagogical scenario entity
     */
    public function deletePedagogicalScenarioAction(TrainingCourse $trainingCourse, PedagogicalScenario $pedagogicalScenario)
    {
        $em = $this->getDoctrine()->getManager();

        $dql_pedagogicalScenarioTrainingCourse = $em->createQuery('SELECT psf
                                                          FROM ScenarioBundle:PedagogicalScenarioTrainingCourse psf
                                                          WHERE psf.trainingCourse = :trainingCourseId AND psf.pedagogicalScenario = :pedagogicalScenarioId');

        $dql_pedagogicalScenarioTrainingCourse->setParameter('trainingCourseId',$trainingCourse->getId());
        $dql_pedagogicalScenarioTrainingCourse->setParameter('pedagogicalScenarioId',$pedagogicalScenario->getId());

        $pedagogicalScenarioTrainingCourse = $dql_pedagogicalScenarioTrainingCourse->getOneOrNullResult();

        $trainingCourse->removePedagogicalScenarioTrainingCourse($pedagogicalScenarioTrainingCourse);
        $pedagogicalScenario->removePedagogicalScenarioTrainingCourse($pedagogicalScenarioTrainingCourse);

        $em->remove($pedagogicalScenarioTrainingCourse);
        $em->flush();

        // Cette méthode est également utilisée par le Scénario Pédagogique
        // Il faut dont renvoyer vers la bonne vue
        $request = $this->container->get('request');
        $routeName = $request->get('_route');

        if ($routeName == 'pedagogicalscenario_training_course_delete')
          return $this->redirectToRoute('pedagogicalscenario_training_course_management', array('userRole' => 'pedagogical-engineer', 'id' => $pedagogicalScenario->getId()));
        else
          return $this->redirectToRoute('training_course_pedagogical_scenario_management', array('userRole' => 'pedagogical-engineer', 'id' => $trainingCourse->getId()));
    }

    private function updateMenu()
    {
      // Mise en évidence du menu, changement du menu sélectionné
      $menu = array('pedagogical_scenario' => "",
                    'projects'             => "",
                    'trainingCourse'            => "list-group-item-info",
                    'user'                 => "",
                    'parameters'           => "",
                    'scenarisation_process'=> "",
                    'scenarisation_stage'  => "",
                    'question'             => "");

      // Mise en session du menu
      $this->get('session')->set('menu',$menu);
    }

    private function createBreadcrumb($currentAction, $trainingCourse = null)
    {
      $breadcrumbs = $this->get("white_october_breadcrumbs");
      $breadcrumbs->addItem("admin");
      switch($currentAction)
      {
        case "index":
          $breadcrumbs->addItem("trainingCourses");
        break;

        case "show":
          $breadcrumbs->addRouteItem("trainingCourses", "training_course_index", [
              'userRole' => ($this->get('security.authorization_checker')->isGranted('ROLE_PEDAGOGICAL_ENGINEER')?"pedagogical-engineer":"teacher")
          ]);
          $breadcrumbs->addItem($trainingCourse->getShortname());
          $breadcrumbs->addItem("show");

        break;

        case "new":
          $breadcrumbs->addRouteItem("trainingCourses", "training_course_index", [
              'userRole' => ($this->get('security.authorization_checker')->isGranted('ROLE_PEDAGOGICAL_ENGINEER')?"pedagogical-engineer":"teacher")
          ]);
          $breadcrumbs->addItem("trainingCourse.new");
        break;

        case "edit":
          $breadcrumbs->addRouteItem("trainingCourses", "training_course_index", [
              'userRole' => ($this->get('security.authorization_checker')->isGranted('ROLE_PEDAGOGICAL_ENGINEER')?"pedagogical-engineer":"teacher")
          ]);
          $breadcrumbs->addItem($trainingCourse->getShortname());
          $breadcrumbs->addItem("edit");

        break;

        case "pedagogical_scenario_management":
          $breadcrumbs->addRouteItem("trainingCourses", "training_course_index", [
              'userRole' => ($this->get('security.authorization_checker')->isGranted('ROLE_PEDAGOGICAL_ENGINEER')?"pedagogical-engineer":"teacher")
          ]);
          $breadcrumbs->addRouteItem($trainingCourse->getShortname(), "training_course_show", [
              'userRole' => ($this->get('security.authorization_checker')->isGranted('ROLE_PEDAGOGICAL_ENGINEER')?"pedagogical-engineer":"teacher"),
              'id'       => $trainingCourse->getId()
          ]);
          $breadcrumbs->addItem("associatedScenarios");
        break;
      }
    }
}
