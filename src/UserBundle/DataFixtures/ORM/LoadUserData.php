<?php

// src/UserBundle/DataFixtures/ORM/LoadUserData.php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use UserBundle\Entity\User;
use UserBundle\Entity\PedagogicalEngineer;
use UserBundle\Entity\Lecturer;

class LoadUserData implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $this->creerUtilisateur("ingenieur","ingenieur","fr","ingenieur","patrick.etcheverry@iutbayonne.univ-pau.fr","ROLE_PEDAGOGICAL_ENGINEER",$manager);
    }

    public function creerUtilisateur($lastname, $firstname, $platformLanguage, $plainPassword, $email, $role, ObjectManager $manager)
    {
        $user = new User();

        $user->setLastname($lastname);
        $user->setFirstname($firstname);
        $user->setPlateformLanguage($platformLanguage);
        $user->setEmail($email);
        $user->setRoles($role);
        $user->generateSalt();

        $encoder  = $this->container->get('security.encoder_factory')->getEncoder($user);
        $passwordEncode = $encoder->encodePassword($plainPassword, $user->getSalt());

        $user->setPassword($passwordEncode);

        $manager->persist($user);
        $manager->flush();
    }

    public function creerUtilisateurFictif($lastname, $firstname, ObjectManager $manager)
    {
        if (rand(0,1) == 0)
          $role = "ROLE_PEDAGOGICAL_ENGINEER";
        else
          $role = "ROLE_TEACHER";

        if (rand(0,2) == 0)
          $platformLanguage = "en";
        else if (rand(0,1) == 0)
          $platformLanguage = "fr";
        else
          $platformLanguage = "es";

        $this->creerUtilisateur($lastname, $firstname, $platformLanguage, "azerty",$firstname . $lastname . "@yahoo.fr", $role,$manager);
    }

    public function getOrder()
    {
        return 1;
    }
}
