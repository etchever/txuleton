<?php

namespace ScenarisationProcessBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ScenarisationStageType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title','text',array('label' => 'title'));
        $builder->add('goals',TextareaType::class,array('label' => 'goals'));
        $builder->add('description',TextareaType::class,array('label' => 'description'));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ScenarisationProcessBundle\Entity\ScenarisationStage'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'scenarisationprocessbundle_scenarisationstage';
    }


}
