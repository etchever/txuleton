<?php

namespace ScenarioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Sequence
 *
 * @ORM\Table(name="sequence")
 * @ORM\Entity(repositoryClass="ScenarioBundle\Repository\SequenceRepository")
 */
class Sequence
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="text")
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="duration", type="string", length=255)
     */
    private $duration;

    /**
     * @ORM\ManyToOne(targetEntity="ScenarioBundle\Entity\PedagogicalScenario", inversedBy="sequences")
     * @ORM\JoinColumn(nullable=false)
     */
    private $pedagogicalScenario;

    /**
     * @ORM\OneToMany(targetEntity="ScenarioBundle\Entity\Session", mappedBy="sequence", cascade={"persist","remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $sessions;

    /**
     * @var int
     *
     * @ORM\Column(name="`order`", type="smallint")
     */
    private $order;

    /**
     * @ORM\ManyToOne(targetEntity="ScenarioBundle\Entity\SequenceAnswer", inversedBy="sequences", cascade={"persist","remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $sequenceAnswer;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Sequence
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set duration
     *
     * @param string $duration
     *
     * @return Sequence
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration
     *
     * @return string
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Set pedagogicalScenario
     *
     * @param \ScenarioBundle\Entity\PedagogicalScenario $pedagogicalScenario
     *
     * @return Sequence
     */
    public function setPedagogicalScenario(\ScenarioBundle\Entity\PedagogicalScenario $pedagogicalScenario)
    {
        $this->pedagogicalScenario = $pedagogicalScenario;

        return $this;
    }

    /**
     * Get pedagogicalScenario
     *
     * @return \ScenarioBundle\Entity\PedagogicalScenario
     */
    public function getPedagogicalScenario()
    {
        return $this->pedagogicalScenario;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->sessions = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add session
     *
     * @param \ScenarioBundle\Entity\Session $session
     *
     * @return Sequence
     */
    public function addSession(\ScenarioBundle\Entity\Session $session)
    {
        $this->sessions[] = $session;
        $session->setSequence($this);

        return $this;
    }

    /**
     * Remove session
     *
     * @param \ScenarioBundle\Entity\Session $session
     */
    public function removeSession(\ScenarioBundle\Entity\Session $session)
    {
        $this->sessions->removeElement($session);
    }

    /**
     * Get sessions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSessions()
    {
        return $this->sessions;
    }

    /**
     * Set order
     *
     * @param int $order
     *
     * @return Question
     */
    public function setOrder($order)
    {
      $this->order = $order;

      return $this;
    }

    /**
     * Get order
     *
     * @return int
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set sequenceAnswer
     *
     * @param \ScenarioBundle\Entity\SequenceAnswer $sequenceAnswer
     *
     * @return SequenceAnswer
     */
    public function setSequenceAnswer(\ScenarioBundle\Entity\SequenceAnswer $sequenceAnswer)
    {
        $this->sequenceAnswer = $sequenceAnswer;

        return $this;
    }

    /**
     * Get sequenceAnswer
     *
     * @return \ScenarioBundle\Entity\SequenceAnswer $sequenceAnswer
     */
    public function getSessionAnswer()
    {
        return $this->sequenceAnswer;
    }
}
