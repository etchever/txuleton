<?php

namespace ScenarioBundle\Controller;

use ScenarioBundle\Entity\PedagogicalScenario;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

// To use Security annotations on the top of fuctions
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

// Pour redéfinir EntityType dans le controlleur
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use ScenarioBundle\Entity\Project;
use ScenarioBundle\Entity\GeneralAnswer;
use ScenarioBundle\Entity\SequenceAnswer;
use ScenarioBundle\Entity\SessionAnswer;

use ScenarisationProcessBundle\Entity\ScenarisationStage;
use ScenarisationProcessBundle\Entity\QuestionOrder;
use ScenarisationProcessBundle\Entity\Question;

use UserBundle\Entity\User;

/**
 * Pedagogical Scenario controller.
 *
 */
class PedagogicalScenarioController extends Controller
{
    /**
     * Lists all pedagogicalScenario entities.
     *
     */
    public function indexAction(Request $request)
    {
      /*$message = (new \Swift_Message('Hello Email'))
        ->setFrom('cda-iut@laposte.net')
        ->setTo('gaizkaalcuyet@yahoo.fr')
        ->setBody(
            $this->renderView(
                // app/Resources/views/Emails/registration.html.twig
                'Emails/registration.html.twig'
            ),
            'text/html'
        );

         $this->get('mailer')->send($message);

        var_dump("envoyé");*/

      // We generate the new breadcrumb
      $this->createBreadcrumb("index");
      $this->updateMenu();

        $em = $this->getDoctrine()->getManager();

        // Si l'utilisateur est un ingénieur pédagogique, on affiche tous les scénarios pédagogiques
        if ($this->get('security.authorization_checker')->isGranted('ROLE_PEDAGOGICAL_ENGINEER'))
        {
          $queryBuilder = $em->getRepository('ScenarioBundle:PedagogicalScenario')->createQueryBuilder('pedagogicalScenarios');
          $query = $queryBuilder->getQuery();
        }
        //Sinon, on affiche seulement ceux dans lesquels l'enseignant intervient
        else
        {
          // On veut récupérer les scénarios pédagogiques de l'utilisateur courant
          $query = $this->get('security.context')->getToken()->getUser()->getPedagogicalScenarioDesigned();
        }

        $paginator  = $this->get('knp_paginator');

        $pedagogicalScenarios = $paginator->paginate(
          $query,
          $request->query->getInt('page', 1)/*page number*/,
          5/*limit per page*/
        );
        return $this->render('ScenarioBundle:pedagogicalscenario:index.html.twig', array(
            'pedagogicalScenarios' => $pedagogicalScenarios
        ));
    }

    /**
     * Creates a new pedagogicalScenario entity.
     * @Security("has_role('ROLE_PEDAGOGICAL_ENGINEER')")
     */
    public function newAction(Request $request)
    {
      $em = $this->getDoctrine()->getManager();

      // On regarde s'il existe des démarches de scénarisation
      $scenarisationProcessRepo = $em->getRepository('ScenarisationProcessBundle:ScenarisationProcess');
      $nbScenProcess = $scenarisationProcessRepo->getNb();

      if ($nbScenProcess <= 0)
      {
        /* Petit message d'erreur, à traduire */
        $request->getSession()->getFlashBag()->add('alert', 'Vous ne pouvez pas créer de scénario pédagogique : il n\'y a aucune démarche de scénarisation à laquelle l\'associer.');
        return $this->redirectToRoute('pedagogicalscenario_index', array('userRole' => "pedagogical-engineer"));
      }
      else
      {
        // We generate the new breadcrumb
        $this->createBreadcrumb("new");
        $this->updateMenu();

        $pedagogicalScenario = new Pedagogicalscenario();

        $form = $this->createForm('ScenarioBundle\Form\PedagogicalScenarioTypeNew', $pedagogicalScenario);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $em->persist($pedagogicalScenario);
            $em->flush();

            /* Message de reussite, à traduire */
            $request->getSession()->getFlashBag()->add('success', 'Le scénario pédagogique \'' . $pedagogicalScenario->getShortname() . '\' a bien été ajoutée !');
            return $this->redirectToRoute('pedagogicalscenario_show', array('id' => $pedagogicalScenario->getId(), 'userRole' => "pedagogical-engineer"));
        }

        return $this->render('ScenarioBundle:pedagogicalscenario:new.html.twig', array(
            'pedagogicalScenario' => $pedagogicalScenario,
            'form' => $form->createView(),
        ));
      }

    }

    /**
     * Finds and displays a pedagogicalScenario entity.
     *
     */
    public function showAction(Request $request, PedagogicalScenario $pedagogicalScenario)
    {
      // We generate the new breadcrumb
      $this->createBreadcrumb("show",$pedagogicalScenario);
      $this->updateMenu();

        $deleteForm = $this->createDeleteForm($pedagogicalScenario);

        // On récupère la liste des trainingCourses dans lesquelles intervient le scénario
        // courant
        $em = $this->getDoctrine()->getManager();
        $dql_trainingCoursesAssociees = $em->createQuery ('
        select f
        FROM ScenarioBundle:TrainingCourse f
        JOIN f.pedagogicalScenarioTrainingCourse psf
        WHERE psf.pedagogicalScenario = :pedagogicalScenario');
        $dql_trainingCoursesAssociees->setParameter('pedagogicalScenario', $pedagogicalScenario);
        $trainingCoursesAssociees = $dql_trainingCoursesAssociees->getResult();

        return $this->render('ScenarioBundle:pedagogicalscenario:show.html.twig', array(
            'pedagogicalScenario' => $pedagogicalScenario,
            'delete_form' => $deleteForm->createView(),
            'trainingCourses' => $trainingCoursesAssociees
        ));
    }

    /**
     * Displays a form to edit an existing pedagogicalScenario entity.
     * @Security("has_role('ROLE_PEDAGOGICAL_ENGINEER')")
     */
    public function editAction(Request $request, PedagogicalScenario $pedagogicalScenario)
    {
        // We generate the new breadcrumb
        $this->createBreadcrumb("edit", $pedagogicalScenario);
        $this->updateMenu();

        $deleteForm = $this->createDeleteForm($pedagogicalScenario);
        $editForm   = $this->createForm('ScenarioBundle\Form\PedagogicalScenarioTypeEdit', $pedagogicalScenario);

        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid())
        {

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('pedagogicalscenario_edit', array('id' => $pedagogicalScenario->getId(), 'userRole' => 'pedagogical-engineer'));
        }

        return $this->render('ScenarioBundle:pedagogicalscenario:edit.html.twig', array(
            'pedagogicalScenario' => $pedagogicalScenario,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a pedagogicalScenario entity.
     * @Security("has_role('ROLE_PEDAGOGICAL_ENGINEER')")
     */
    public function deleteAction(Request $request, PedagogicalScenario $pedagogicalScenario)
    {
        $form = $this->createDeleteForm($pedagogicalScenario);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($pedagogicalScenario);
            $em->flush();
        }

        /* Petit message de reussite, à traduire */
        $request->getSession()->getFlashBag()->add('success', "Le scénario pédagogique '" . $pedagogicalScenario->getShortname() . "' a bien été supprimé.");
        return $this->redirectToRoute('pedagogicalscenario_index', array('userRole' => "pedagogical-engineer"));
    }

    /**
     * Creates a form to delete a pedagogicalScenario entity.
     *
     * @param PedagogicalScenario $pedagogicalScenario The pedagogicalScenario entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(PedagogicalScenario $pedagogicalScenario)
    {
      if ($this->get('security.context')->isGranted('ROLE_PEDAGOGICAL_ENGINEER'))
      {
        $currentRole = 'pedagogical-engineer';
      }
      else if ($this->get('security.context')->isGranted('ROLE_TEACHER'))
      {
        $currentRole = 'teacher';
      }
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('pedagogicalscenario_delete', array('id' => $pedagogicalScenario->getId(), 'userRole' => $currentRole)))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     * Permit to manage the TrainingCourses of a Pedagogical Scenario
     *
     * @param PedagogicalScenario $pedagogicalScenario The pedagogicalScenarioEntity entity
     *
     */
    public function trainingCourseManagementAction(PedagogicalScenario $pedagogicalScenario)
    {
      // We generate the new breadcrumb
      $this->createBreadcrumb("training_courses_management", $pedagogicalScenario);
      $this->updateMenu();
        $em = $this->getDoctrine()->getManager();

        /*$queryBuilder = $em->getRepository('ScenarioBundle:PedagogicalScenario')->createQueryBuilder('pedagogicalScenarios');
        $query = $queryBuilder->getQuery();

        $paginator  = $this->get('knp_paginator');

        $pedagogicalScenarios = $paginator->paginate(
          $query,
          $request->query->getInt('page', 1), // page number
          5 // limit per page
        );*/

        // On récupère la liste des trainingCourses dans lesquelles le scenarioPedagogique est déjà associé
        $trainingCoursesAlreadyLinked = $em->createQuery ('
        select f, psf, ps
        FROM ScenarioBundle:TrainingCourse f
        JOIN f.pedagogicalScenarioTrainingCourse psf
        JOIN psf.pedagogicalScenario ps
        WHERE psf.pedagogicalScenario = :pedagogicalScenario');
        $trainingCoursesAlreadyLinked->setParameter('pedagogicalScenario', $pedagogicalScenario);

        // On récupère la liste trainingCourses dans laquelle le scénario pédagogique n'est pas lié
        $trainingCoursesAvailable = $em->createQuery ('
        select ff
        FROM ScenarioBundle:TrainingCourse ff
        WHERE ff NOT IN (select f
        FROM ScenarioBundle:TrainingCourse f
        JOIN f.pedagogicalScenarioTrainingCourse psf
        JOIN psf.pedagogicalScenario ps
        WHERE psf.pedagogicalScenario = :pedagogicalScenario)');
        $trainingCoursesAvailable->setParameter('pedagogicalScenario', $pedagogicalScenario);

        $trainingCoursesAlreadyLinked = $trainingCoursesAlreadyLinked->getResult();
        $trainingCoursesAvailable     = $trainingCoursesAvailable->getResult();

          return $this->render('ScenarioBundle:pedagogicalscenario:trainingcoursemanagement.html.twig', array(
              'trainingCoursesAlreadyLinked' => $trainingCoursesAlreadyLinked,
              'trainingCoursesAvailable' => $trainingCoursesAvailable,
              'pedagogicalScenario' => $pedagogicalScenario
          ));
    }

    // On change le projet. Appelé depuis le menu des projets, lors du changement de projet d'un scénar
    public function changeProjectAction(PedagogicalScenario $pedagogicalScenario, Project $project)
    {
      $em = $this->getDoctrine()->getManager();

      // On défait l'ancien lien entre le projet et le scénario
      if ($pedagogicalScenario->getProject() != null)
        $pedagogicalScenario->getProject()->removePedagogicalScenario($pedagogicalScenario);


      $pedagogicalScenario->setProject($project);

      $em->persist($pedagogicalScenario);
      $em->flush();

      return $this->redirectToRoute('project_pedagogical_scenario_management', array('id' => $project->getId(), 'userRole' => "pedagogical-engineer"));

    }

    public function defineTeachersAction(PedagogicalScenario $pedagogicalScenario)
    {
      // We generate the new breadcrumb
      $this->createBreadcrumb("define_teachers",$pedagogicalScenario);
      $this->updateMenu();

      $em   = $this->getDoctrine()->getManager();
      $repo = $em->getRepository('UserBundle:User');

      $teachersDesigning     = $repo->getTeachersDesigningPedagogicalScenario($pedagogicalScenario);
      $teachersNotDesigning  = $repo->getTeachersNotDesigningPedagogicalScenario($pedagogicalScenario);

      return $this->render('ScenarioBundle:pedagogicalscenario:defineteachers.html.twig', array(
        'teachersDesigning' => $teachersDesigning,
        'teachersNotDesigning' => $teachersNotDesigning,
        'pedagogicalScenario' => $pedagogicalScenario
      ) );
    }

    public function addTeacherAction(PedagogicalScenario $pedagogicalScenario,User $teacher)
    {
      $em   = $this->getDoctrine()->getManager();
      $teacher->addPedagogicalScenarioDesigned($pedagogicalScenario);
      $em->persist($teacher);
      $em->flush();

      return $this->redirectToRoute('pedagogicalscenario_define_teachers', array('id' => $pedagogicalScenario->getId(), 'userRole' => "pedagogical-engineer"));
    }

    public function removeTeacherAction(PedagogicalScenario $pedagogicalScenario,User $teacher)
    {
      $em   = $this->getDoctrine()->getManager();
      $teacher->removePedagogicalScenarioDesigned($pedagogicalScenario);
      $em->persist($teacher);
      $em->flush();

      return $this->redirectToRoute('pedagogicalscenario_define_teachers', array('id' => $pedagogicalScenario->getId(), 'userRole' => "pedagogical-engineer"));
    }

    public function designAction(PedagogicalScenario $pedagogicalScenario)
    {
      // Get the entity manager
      $em = $this->getDoctrine()->getManager();

      // We generate the new breadcrumb
      $this->createBreadcrumb("design", $pedagogicalScenario);
      $this->updateMenu();

      // On récupère la liste des étapes dans lesquelles le scenarioPedagogique est déjà associé
      $dql_etapes = $em->createQuery ('
      select etapesOrdre, etapes, qo, q, a
      FROM ScenarisationProcessBundle:ScenarisationStageOrder etapesOrdre
      JOIN etapesOrdre.scenarisationStage etapes
      JOIN etapesOrdre.scenarisationProcess p
      JOIN etapes.questionOrder qo
      JOIN qo.question q
      LEFT JOIN q.answers a
      WHERE p = :scenarisationProcess
      ORDER BY etapesOrdre.position');

      $dql_etapes->setParameter('scenarisationProcess', $pedagogicalScenario->getScenarisationProcess());

      $etapes = $dql_etapes->getResult();
      $tab_avancement_etapes = array();

      $compteurEtape    = 0;
      $compteurQuestion = 0;

      // On parcourt toutes les étapes de la démarche
      foreach ($etapes as $etapeCourante)
      {
        $termine = false;

        // On ne vérifie par qu'il y a au moins une question dans l'étape, à priori c'est obligé
        foreach ($etapeCourante->getScenarisationStage()->getQuestionOrder() as $key2 => $currentQuestionOrder)
        {
          /* Au début, on dit qu'à la question X de l'étape Y, il n'y a aucune réponse de valide */
          $tab[$compteurEtape][$compteurQuestion] = 0;

          /* Si la question courante possède au moins une reponse */
          if ($currentQuestionOrder->getQuestion()->getAnswers() != null)
          {
            $answer = $currentQuestionOrder->getQuestion()->getAnswers();
            $indiceCourant = 0;
            while ($answer[$indiceCourant] != null && $termine == false)
            {
              if ($answer[$indiceCourant]->getPedagogicalScenario() == $pedagogicalScenario)
              {
                $termine = true;

                if ($answer[$indiceCourant]->getIsValid() == true)
                {
                  $tab[$compteurEtape][$compteurQuestion]++;
                }
              }

              $indiceCourant++;
            }
          }
          // On passe à la question suivante
          $compteurQuestion++;
        }
        /* On passe à l'étape suivante */
        $compteurEtape++;
      }

      //var_dump($tab);

      return $this->render('ScenarioBundle:pedagogicalscenario:design.html.twig',array(
        'etapes' => $etapes,
        'pedagogicalScenario' => $pedagogicalScenario
      ));
    }

    public function stageQuestionsAnswersViewAction(PedagogicalScenario $pedagogicalScenario, ScenarisationStage $scenarisationStage)
    {
      // We generate the new breadcrumb
      $this->createBreadcrumb("answers_view", $pedagogicalScenario, $scenarisationStage);
      $this->updateMenu();

      $em = $this->getDoctrine()->getManager();

      // Récupérer les questions (et les réponses associées) de l'étape et du scénario pédagogique
      $dql_questions = $em->createQuery ('
      SELECT questionOrders, question, answers
      FROM ScenarisationProcessBundle:QuestionOrder questionOrders
      LEFT JOIN questionOrders.question question
      LEFT JOIN question.answers answers
      WHERE (questionOrders.scenarisationStage = :currentStage)
        AND (question.answers IS EMPTY OR answers.pedagogicalScenario = :pedagogicalScenario)
      ORDER BY questionOrders.position');

      $dql_questions->setParameter('currentStage', $scenarisationStage);
      $dql_questions->setParameter('pedagogicalScenario', $pedagogicalScenario);


      return $this->render('ScenarioBundle:pedagogicalscenario:stagequestionsanswersview.html.twig', array(
        'questions' => $dql_questions->getResult(),
        'pedagogicalScenario' => $pedagogicalScenario,
        'scenarisationStage' => $scenarisationStage
      ));
    }

    public function stageQuestionsAnswersEditAction(Request $request, PedagogicalScenario $pedagogicalScenario, ScenarisationStage $scenarisationStage, Request $userRequest)
    {
      // We generate the new breadcrumb
      $this->createBreadcrumb("answers_edit", $pedagogicalScenario, $scenarisationStage);
      $this->updateMenu();

      $em = $this->getDoctrine()->getManager();

      // Récupère les questions (et les réponses) de l'étape courante
      $dql_questions = $em->createQuery ('
      SELECT questionOrders, question, answers
      FROM ScenarisationProcessBundle:QuestionOrder questionOrders
      LEFT JOIN questionOrders.question question
      LEFT JOIN question.answers answers
      WHERE (questionOrders.scenarisationStage = :currentStage)
      ORDER BY questionOrders.position');

      $dql_questions->setParameter('currentStage', $scenarisationStage);

      $Questions         = $dql_questions->getResult();

      $numberOfQuestions = sizeof($Questions);

      // Si l'étape contient des questions
      if ($numberOfQuestions > 0)
      {
          /* On construit un tableau dans lequel les données du formulaire
          seront recueillies */
          $tabReponses = array();

          /* On créé un constructeur de formulaires qui pourra fabriquer des
          formulaires capables de remplir $tabQuestionsReponses */
          $createurFormulaires = $this->createFormBuilder($tabReponses);

          /* Booléen : si l'utilisateur courant est un "ingénieur pédagogique", il peut modifier le champ "remarque ingénieur"
                       dans tous les autres cas (il n'y en a qu'un, c'est "enseignant") l'utilisateur ne peut pas modifier le champ */
          $readonly = ($this->get('security.authorization_checker')->isGranted("ROLE_PEDAGOGICAL_ENGINEER")? false:true);

          /* On utilise le constructeur de formulaires pour définir les champs qui
          constitueront le formulaire de contact */
          for ($i = 0 ; $i < $numberOfQuestions;  $i++)
          {
            /* A chaque tour de boucle on réinitialise les variables */
            $placeholderEngineerObservation = "";
            $engineerObservation            = "";
            $validationState                = false;
            $reponseAlaQuestionCourante     = $this->extractAnswerFromCollection($Questions[$i]->getQuestion()->getAnswers(), $pedagogicalScenario);

            /* On récupère la remarque de l'ingénieur pédagogique */
            if ($reponseAlaQuestionCourante != null && !empty($reponseAlaQuestionCourante->getPedagogicalEngineerObservation()))
              $engineerObservation = $reponseAlaQuestionCourante->getPedagogicalEngineerObservation();
            else
              $placeholderEngineerObservation = "noObservation";

            /* On récupère l'état (satisfaisante ou pas) de la question */
            if ($reponseAlaQuestionCourante != null && $reponseAlaQuestionCourante->getIsValid() == true)
              $validationState = true;

            // S'il s'agit d'une question dite "générale" (<=> textuelle)
            if ($Questions[$i]->getQuestion()->getType() == 'generalQuestion')
            {
              $answerPlaceholder = "";
              $actualAnswer      = "";

              /* S'il l'objet réponse est initialisé et n'est pas vide */
              if ($reponseAlaQuestionCourante != null && !empty($reponseAlaQuestionCourante->getTextAnswer()) )
                // On récupère le contenu
                $actualAnswer      = $reponseAlaQuestionCourante->getTextAnswer();
              else
                // On met un placeholder
                $answerPlaceholder = "noAnswer";

              $createurFormulaires->add("answer_$i",  'textarea', array(
                                                          'data' => $actualAnswer,
                                                          'required' => false,
                                                          'attr' => array(
                                                                      'placeholder' => $answerPlaceholder)));

            }
            // Sinon si c'est une question séquence
            else if ($Questions[$i]->getQuestion()->getType() == 'sequenceQuestion')
            {
              /* Cette ligne permet de faire en sorte que
                 si la réponseSéquece est vide, un tableau de deux cases vides s'affiche
                 (avant, de faire cette ligne, un tableau avec seulement la ligne d'en-tête s'affichait) */
              $sequencesPrecedementSpecifiees = array(null,null);
              // On récupère la réponse séquence si on y a déjà répondu auparavant
              if ($reponseAlaQuestionCourante != null && !empty($reponseAlaQuestionCourante->getSequencesArray()))
                $sequencesPrecedementSpecifiees = $reponseAlaQuestionCourante->getSequencesArray();

              $createurFormulaires->add("answer_$i", 'collection', array(
                                                          'data'         => $sequencesPrecedementSpecifiees,
                                                          'required'     => false,
                                                          'allow_add'    => true,
                                                          'by_reference' => false,));
            }
            else if ($Questions[$i]->getQuestion()->getType() == 'sessionQuestion')
            {
              /* Cette ligne permet de faire en sorte que
                 si la réponseSéance est vide, un tableau de trois cases vides s'affiche
                 (avant, de faire cette ligne, un tableau avec seulement la ligne d'en-tête s'affichait) */
              $seancesPrecedemmentSpecifiees = array(null,null,null);
              // On récupère la réponse séance si on y a déjà répondu auparavant
              if ($reponseAlaQuestionCourante != null && !empty($reponseAlaQuestionCourante->getSessionsArray()))
                $seancesPrecedemmentSpecifiees = $reponseAlaQuestionCourante->getSessionsArray();

              $createurFormulaires->add("answer_$i", 'collection', array(
                                                          'data'         => $seancesPrecedemmentSpecifiees,
                                                          'required'     => false,
                                                          'allow_add'    => true,
                                                          'by_reference' => false,));
            }

            // Quelque soit le cas, il y a une remarque de la part d'un ingénieur ainsi que la validation
            $createurFormulaires->add("observation_$i", 'textarea', array(
                                                        'data'      => $engineerObservation,
                                                        'read_only' => $readonly,
                                                        'required'  => false,
                                                        'attr'      => array(
                                                                        'placeholder' => $placeholderEngineerObservation)));

            $createurFormulaires->add("isValid_$i", "checkbox", array(
                                                        'data'     => $validationState,
                                                        'label'    => 'satisfactory',
                                                        'required' => false,
                                                        'attr'     => array('class' => 'caseAcocher')
                                                        ));

          }

          // On génère le formulaire reponses avec les champs précédemment définis
          $formulaireReponses =  $createurFormulaires->getForm();

          // Enregistrement des données dans le tableau
          $formulaireReponses->handleRequest($userRequest);

          $submit = $formulaireReponses->isSubmitted();

          if ($submit)
          {
              /* On récupère les données du formulaire dans un tableau */
              $tabbReponses = $formulaireReponses->getData();
              var_dump($tabbReponses);
              for ($i = 0 ; $i < $numberOfQuestions;  $i++)
              {
                /* On récupère Réponse qui répond à la question courante */
                $reponseAlaQuestionCourante = $this->extractAnswerFromCollection($Questions[$i]->getQuestion()->getAnswers(), $pedagogicalScenario);

                // Réponse de type générale
                if ($Questions[$i]->getQuestion()->getType() == "generalQuestion")
                {
                  if ($reponseAlaQuestionCourante != null)
                  {
                    $answer = $reponseAlaQuestionCourante;
                    //$em->persist($reponseAlaQuestionCourante);
                  }
                  else
                  {
                    $answer = new GeneralAnswer();
                    /* Le lien GeneralQuestion 1 <-> * GeneralQuestion (la raciproque est faire de l'autre coté) */
                    $answer->setGeneralQuestion($Questions[$i]->getQuestion());
                  }
                  $answer->setTextAnswer($tabbReponses["answer_$i"]);
                }
                // Réponse de type séquence
                else if ($Questions[$i]->getQuestion()->getType() == "sequenceQuestion")
                {
                    // Si l'utilisateur a répondu à la question courante
                    if ($reponseAlaQuestionCourante != null)
                    {
                      $answer = $reponseAlaQuestionCourante;
                    }
                    else
                    {
                      $answer = new SequenceAnswer();
                      /* Le lien SequenceQuestion 1 <-> * SequenceAnswer (la raciproque est faire de l'autre coté) */
                      $answer->setSequenceQuestion($Questions[$i]->getQuestion());
                    }

                    $tableauApresRetraitDesNull = $this->removeNullElementsSequenceTab($tabbReponses["answer_$i"]);

                    // Fonction "Array Filter" au cas où l'utilisateur aurait laissé des cases vides (null)
                    $answer->setSequencesArray($tableauApresRetraitDesNull);

                }
                // Réponse de type séance
                else if ($Questions[$i]->getQuestion()->getType() == "sessionQuestion")
                {
                  // Si l'utilisateur a répondu à la question courante
                  if ($reponseAlaQuestionCourante != null)
                  {
                    $answer = $reponseAlaQuestionCourante;
                  }
                  else
                  {
                    $answer = new SessionAnswer();
                    /* Le lien SequenceQuestion 1 <-> * SequenceAnswer (la raciproque est faire de l'autre coté) */
                    $answer->setSessionQuestion($Questions[$i]->getQuestion());
                  }
                  $tableauApresRetraitDesNull = $this->removeNullElementsSessionTab($tabbReponses["answer_$i"]);

                  // Fonction "Array Filter" au cas où l'utilisateur aurait laissé des cases vides (null)
                  $answer->setSessionsArray($tableauApresRetraitDesNull);
                }
                // La remarque ingé et la validité sont propores à tous les types de questions
                $answer->setPedagogicalEngineerObservation($tabbReponses["observation_$i"]);

                $answer->setIsValid($tabbReponses["isValid_$i"]);

                /* Le lien Question 1 <-> * Reponse (la réciproque est faite de l'autre cote) */
                $Questions[$i]->getQuestion()->addAnswer($answer);
                /* Le lien PedagogicalScenario 1 <-> * Reponse (la réciproque est faite de l'autre cote) */
                $pedagogicalScenario->addAnswer($answer);

                /* On enregistre en base de données*/
                $em->persist($Questions[$i]);
                $em->flush();

                /* Petit message de reussite, à traduire */
                $request->getSession()->getFlashBag()->add('success', "Les modifications ont bien été enregistrées.");


              }

              // On doit recharger la page sinon les nouvelles données ne seront pas affichées correctement (on aura celles avant enregistrement)
              // Alors que les modifications auront bien été prises en compte
              $request = $this->getRequest();
              return $this->redirect($request->getUri());
          }
      }
      else {
        /* Petit message de reussite, à traduire */
        $request->getSession()->getFlashBag()->add('alert', "L'étape ne contient aucune question.");
        return $this->redirectToRoute('pedagogicalscenario_design', array('id' => $pedagogicalScenario->getId(), 'userRole' => $this->get('security.context')->getToken()->getUser()->getRoleForRouter()));
      }

      return $this->render('ScenarioBundle:pedagogicalscenario:stagequestionsanswersedit.html.twig', array(
        'questions'           => $Questions,
        'pedagogicalScenario' => $pedagogicalScenario,
        'scenarisationStage'  => $scenarisationStage,
        'formulaireReponses'  => $formulaireReponses->createView(),
        'submit'              => $submit
      ));
    }

    /* A partir d'une collection d'object Answer et d'un scénario pédagogique :
              Retourne la réponse correspondant au scénario pédagogique
              Sinon, retourne 'null'                                            */
    // J'ai fait cette méthode car au niveau de la requete ligne 340
    // Je n'ai pas réussi à récupérer la réponse d'un scénario pédagogique en particulier.
    // Dans le cas où des reponses existaient déjà mais dans d'autres scénario péda,
    // La requête ne renvoyait pas la question
    private function extractAnswerFromCollection($answers, $pedagogicalScenario)
    {
      $indiceCourant  = 0;
      $tailleTab      = sizeof($answers);
      $reponseTrouvee = null;
      $idScenarioPeda = $pedagogicalScenario->getId();

      while ($indiceCourant < $tailleTab && $reponseTrouvee == null)
      {
        if ($answers[$indiceCourant]->getPedagogicalScenario()->getId() == $idScenarioPeda)
        {
          $reponseTrouvee = $answers[$indiceCourant];
        }
        $indiceCourant++;
      }

      return $reponseTrouvee;

    }

    /* Cette fonction a pour objectif de retirer les "null" du tableau de réponse et de correctement le reconstituer */
    private function removeNullElementsEndOfTab($tab,$typeTableau)
    {
      if ($typeTableau == "sequence")
        $nbColEffective = 2;
      else
        $nbColEffective = 3;

      if (sizeof($tab)-$nbColEffective >= 0)
      {
          $newTab = array();

          for ($i=0 ; $i < sizeof($tab) ; $i+=$nbColEffective)
          {
            if ($tab[$i] != null || $tab[$i+1] != null)
            {
              array_push($newTab,$tab[$i], $tab[$i+1]);
            }
          }
      }
      return $newTab;
    }

    /* Cette fonction a pour objectif de retirer les "null" du tableau de réponse sequence */
    private function removeNullElementsSequenceTab($tab)
    {
      if (sizeof($tab)-2 >= 0)
      {
          $newTab = array();

          for ($i=0 ; $i < sizeof($tab) ; $i+=2)
          {
            if ($tab[$i] != null || $tab[$i+1] != null)
            {
              array_push($newTab,$tab[$i], $tab[$i+1]);
            }
          }
      }
      return $newTab;
    }

    /* Cette fonction a pour objectif de retirer les "null" du tableau de réponse séance */
    private function removeNullElementsSessionTab($tab)
    {
      if (sizeof($tab)-3 >= 0)
      {
          $newTab = array();

          for ($i=0 ; $i < sizeof($tab) ; $i+=3)
          {
            if ($tab[$i] != null || $tab[$i+1] != null || $tab[$i+2] != null)
            {
              array_push($newTab,$tab[$i], $tab[$i+1], $tab[$i+2]);
            }
          }
      }
      return $newTab;
    }

    private function createBreadcrumb($currentAction, $pedagogicalScenario = null, $scenarisationStage = null)
    {
      $breadcrumbs = $this->get("white_october_breadcrumbs");
      $breadcrumbs->addItem("admin");
      switch($currentAction)
      {
        case "index":
          $breadcrumbs->addItem("pedagogicalScenarios");
        break;

        case "show":
          $breadcrumbs->addRouteItem("pedagogicalScenarios", "pedagogicalscenario_index", [
              'userRole' => ($this->get('security.authorization_checker')->isGranted('ROLE_PEDAGOGICAL_ENGINEER')?"pedagogical-engineer":"teacher")
          ]);
          $breadcrumbs->addItem("show");
          $breadcrumbs->addItem($pedagogicalScenario->getShortname());
        break;

        case "new":
          $breadcrumbs->addRouteItem("pedagogicalScenarios", "pedagogicalscenario_index", [
              'userRole' => ($this->get('security.authorization_checker')->isGranted('ROLE_PEDAGOGICAL_ENGINEER')?"pedagogical-engineer":"teacher")
          ]);
          $breadcrumbs->addItem("pedagogicalScenario.new");
        break;

        case "edit":
          $breadcrumbs->addRouteItem("pedagogicalScenarios", "pedagogicalscenario_index", [
              'userRole' => ($this->get('security.authorization_checker')->isGranted('ROLE_PEDAGOGICAL_ENGINEER')?"pedagogical-engineer":"teacher")
          ]);
          $breadcrumbs->addItem("edit");
          $breadcrumbs->addItem($pedagogicalScenario->getShortname());
        break;

        case "training_courses_management":
          $breadcrumbs->addRouteItem("pedagogicalScenarios", "pedagogicalscenario_index", [
              'userRole' => ($this->get('security.authorization_checker')->isGranted('ROLE_PEDAGOGICAL_ENGINEER')?"pedagogical-engineer":"teacher")
          ]);
          $breadcrumbs->addRouteItem($pedagogicalScenario->getShortname(), "pedagogicalscenario_show", [
              'userRole' => ($this->get('security.authorization_checker')->isGranted('ROLE_PEDAGOGICAL_ENGINEER')?"pedagogical-engineer":"teacher"),
              'id'       => $pedagogicalScenario->getId()
          ]);
          $breadcrumbs->addItem("pedagogicalScenario.trainingCourse.management");
        break;

        case "define_teachers":
          $breadcrumbs->addRouteItem("pedagogicalScenarios", "pedagogicalscenario_index", [
              'userRole' => ($this->get('security.authorization_checker')->isGranted('ROLE_PEDAGOGICAL_ENGINEER')?"pedagogical-engineer":"teacher")
          ]);
          $breadcrumbs->addRouteItem($pedagogicalScenario->getShortname(), "pedagogicalscenario_show", [
              'userRole' => ($this->get('security.authorization_checker')->isGranted('ROLE_PEDAGOGICAL_ENGINEER')?"pedagogical-engineer":"teacher"),
              'id' => $pedagogicalScenario->getId()
          ]);
          $breadcrumbs->addItem("pedagogicalScenario.teachers.management");
        break;

        case "design":
          $breadcrumbs->addRouteItem("pedagogicalScenarios", "pedagogicalscenario_index", [
              'userRole' => ($this->get('security.authorization_checker')->isGranted('ROLE_PEDAGOGICAL_ENGINEER')?"pedagogical-engineer":"teacher")
          ]);
          $breadcrumbs->addRouteItem($pedagogicalScenario->getShortname(), "pedagogicalscenario_show", [
              'userRole' => ($this->get('security.authorization_checker')->isGranted('ROLE_PEDAGOGICAL_ENGINEER')?"pedagogical-engineer":"teacher"),
              'id'       => $pedagogicalScenario->getId()
          ]);
          $breadcrumbs->addItem("design");
        break;
        case 'answers_view':
          $breadcrumbs->addRouteItem("pedagogicalScenarios", "pedagogicalscenario_index", [
              'userRole' => ($this->get('security.authorization_checker')->isGranted('ROLE_PEDAGOGICAL_ENGINEER')?"pedagogical-engineer":"teacher")
          ]);
          $breadcrumbs->addRouteItem($pedagogicalScenario->getShortname(), "pedagogicalscenario_show", [
              'userRole' => ($this->get('security.authorization_checker')->isGranted('ROLE_PEDAGOGICAL_ENGINEER')?"pedagogical-engineer":"teacher"),
              'id'       => $pedagogicalScenario->getId()
          ]);
          $breadcrumbs->addRouteItem("design", "pedagogicalscenario_design", [
              'userRole' => ($this->get('security.authorization_checker')->isGranted('ROLE_PEDAGOGICAL_ENGINEER')?"pedagogical-engineer":"teacher"),
              'id'       => $pedagogicalScenario->getId()
          ]);
          $breadcrumbs->addRouteItem($scenarisationStage->getTitle(), "scenarisation_stage_show", [
              'id'       => $scenarisationStage->getId()
          ]);
          $breadcrumbs->addItem("answersView");
        break;
        case 'answers_edit':
          $breadcrumbs->addRouteItem("pedagogicalScenarios", "pedagogicalscenario_index", [
              'userRole' => ($this->get('security.authorization_checker')->isGranted('ROLE_PEDAGOGICAL_ENGINEER')?"pedagogical-engineer":"teacher")
          ]);
          $breadcrumbs->addRouteItem($pedagogicalScenario->getShortname(), "pedagogicalscenario_show", [
              'userRole' => ($this->get('security.authorization_checker')->isGranted('ROLE_PEDAGOGICAL_ENGINEER')?"pedagogical-engineer":"teacher"),
              'id'       => $pedagogicalScenario->getId()
          ]);
          $breadcrumbs->addRouteItem("design", "pedagogicalscenario_design", [
              'userRole' => ($this->get('security.authorization_checker')->isGranted('ROLE_PEDAGOGICAL_ENGINEER')?"pedagogical-engineer":"teacher"),
              'id'       => $pedagogicalScenario->getId()
          ]);
          $breadcrumbs->addRouteItem($scenarisationStage->getTitle(), "scenarisation_stage_show", [
              'id'       => $scenarisationStage->getId()
          ]);
          $breadcrumbs->addItem("answersEdit");

        break;
      }
    }

    private function updateMenu()
    {
      // Mise en évidence du menu, changement du menu sélectionné
      $menu = array('pedagogical_scenario' => "list-group-item-info",
                    'projects'             => "",
                    'trainingCourse'       => "",
                    'user'                 => "",
                    'parameters'           => "",
                    'scenarisation_process'=> "",
                    'scenarisation_stage'  => "",
                    'question'             => "");

      // Mise en session du menu
      $this->get('session')->set('menu',$menu);
    }
}
