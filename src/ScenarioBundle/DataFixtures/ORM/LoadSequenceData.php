<?php

// src/ScenarioBundle/DataFixtures/ORM/LoadSequenceData.php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use ScenarioBundle\Entity\Sequence;

class LoadSequenceData implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        /*$em = $this->container->get('doctrine')->getManager();

        $this->ajouterS1($manager,$em);
          */
    }

    public function ajouterS1(ObjectManager $manager,$em)
    {
      $s1 = new Sequence();

      $s1->setTitle("Les conditions");
      $s1->setDuration("1 semaine");
      $s1->setOrder(1);

      $repository = $em->getRepository('ScenarioBundle:PedagogicalScenario');
      $m1102 = $repository->findOneBy(array('shortname' => 'M1102'));
      $m1102->addSequence($s1);

      $manager->persist($s1);
      $manager->flush();

    }

    public function getOrder()
    {
        return 4;
    }
}
