<?php

namespace ScenarioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PedagogicalScenario
 *
 * @ORM\Table(name="pedagogical_scenario")
 * @ORM\Entity(repositoryClass="ScenarioBundle\Repository\PedagogicalScenarioRepository")
 */
class PedagogicalScenario
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="shortname", type="string", length=255)
     */
    private $shortname;

    /**
     * @var string
     *
     * @ORM\Column(name="fullname", type="text")
     */
    private $fullname;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User", inversedBy="pedagogicalScenarioPiloted")
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    private $referentTeacher;

    /**
     * @ORM\ManyToOne(targetEntity="ScenarioBundle\Entity\Project", inversedBy="pedagogicalScenarios")
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    private $project;

    /**
     * @ORM\OneToMany(targetEntity="ScenarioBundle\Entity\Sequence", mappedBy="pedagogicalScenario", cascade={"persist","remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $sequences;

    /**
     * @ORM\ManyToOne(targetEntity="ScenarisationProcessBundle\Entity\ScenarisationProcess", inversedBy="pedagogicalScenarios", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $scenarisationProcess;

    /**
     * @ORM\OneToMany(targetEntity="ScenarioBundle\Entity\Answer", mappedBy="pedagogicalScenario", cascade={"persist","remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $answers;

    /**
     * @ORM\OneToMany(targetEntity="ScenarioBundle\Entity\PedagogicalScenarioTrainingCourse", mappedBy="pedagogicalScenario")
     * @ORM\JoinColumn(nullable=true)
     */
    private $pedagogicalScenarioTrainingCourse;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set shortname
     *
     * @param string $shortname
     *
     * @return PedagogicalScenario
     */
    public function setShortname($shortname)
    {
        $this->shortname = $shortname;

        return $this;
    }

    /**
     * Get shortname
     *
     * @return string
     */
    public function getShortname()
    {
        return $this->shortname;
    }

    /**
     * Set fullname
     *
     * @param string $fullname
     *
     * @return PedagogicalScenario
     */
    public function setFullname($fullname)
    {
        $this->fullname = $fullname;

        return $this;
    }

    /**
     * Get fullname
     *
     * @return string
     */
    public function getFullname()
    {
        return $this->fullname;
    }

    /**
     * Set referentTeacher
     *
     * @param \UserBundle\Entity\User $referentTeacher
     *
     * @return PedagogicalScenario
     */
    public function setReferentTeacher(\UserBundle\Entity\User $referentTeacher = null)
    {
      $this->referentTeacher = $referentTeacher;

      if ($referentTeacher != null)
        $referentTeacher->addPedagogicalScenarioPiloted($this);

      return $this;
    }

    /**
     * Update referentTeacher
     *
     * @param \UserBundle\Entity\User $referentTeacher
     *
     * @return PedagogicalScenario
     *
     * Met à jour l'enseignant référent d'un scénario pédagogique.
     * Si le nouvel enseignant référent ne 'concoit' pas déjà le pédagogique,
     * la fonction l'ajoute.
     */
    public function updateReferentTeacher(\UserBundle\Entity\User $newReferentTeacher)
    {
      /* Si l'utilisateur que l'on veut nommer comme référent n'est pas null ou ne l'est pas déjà */
      if ($newReferentTeacher != null && $this->getReferentTeacher() != $newReferentTeacher)
      {

        /* Déjà, on récupère la liste des PedagogicalScenario Concus par le nouvel utilisateur */
        $tab_pedagogicalScenarioDesigned = $newReferentTeacher->getPedagogicalScenarioDesigned()->toArray();

        /* Si le PedagogicalScenario courant n'est pas dans cette liste, on l'ajoute */
        if (!in_array($this, $tab_pedagogicalScenarioDesigned))
          $newReferentTeacher->addPedagogicalScenarioDesigned($this);

        /* On spécifie chez l'ancien utilisateur qu'il n'est plus référent du scénario pédagogique courant */
        if ($this->getReferentTeacher() != null)
          $this->getReferentTeacher()->removePedagogicalScenarioPiloted($this);

        /* Ce setter fait automatiquement utilisateur->addPedagogicalScenarioDesigned*/
        $this->setReferentTeacher($newReferentTeacher);

      }

      return $this;
    }

    /**
     * Get referentTeacher
     *
     * @return \UserBundle\Entity\User
     */
    public function getReferentTeacher()
    {
        return $this->referentTeacher;
    }

    /**
     * Set project
     *
     * @param \ScenarioBundle\Entity\Project $project
     *
     * @return PedagogicalScenario
     */
    public function setProject(\ScenarioBundle\Entity\Project $project = null)
    {
        if ($this->project != null)
          $this->project->removePedagogicalScenario($this);
        // faudra faire un remove coco
        $this->project = $project;

        // Lorsque la liste déroulante du formulaire laisse vide
        if ($project != null)
        {
          $project->addPedagogicalScenario($this);
        }

        return $this;
    }

    /**
     * Get project
     *
     * @return \ScenarioBundle\Entity\Project
     */
    public function getProject()
    {
        return $this->project;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->sequences = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add sequence
     *
     * @param \ScenarioBundle\Entity\Sequence $sequence
     *
     * @return PedagogicalScenario
     */
    public function addSequence(\ScenarioBundle\Entity\Sequence $sequence)
    {
        $this->sequences[] = $sequence;
        $sequence->setPedagogicalScenario($this);

        return $this;
    }

    /**
     * Remove sequence
     *
     * @param \ScenarioBundle\Entity\Sequence $sequence
     */
    public function removeSequence(\ScenarioBundle\Entity\Sequence $sequence)
    {
        $this->sequences->removeElement($sequence);
        /* Je ne sais pas encore si je dois mettre ça ; comme j'ai mis nullable d'un côte de la relation mais pas de l'autre */
        $sequence->setPedagogicalScenario(null);
    }

    /**
     * Get sequences
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSequences()
    {
        return $this->sequences;
    }

    /**
     * Set scenarisationProcess
     *
     * @param \ScenarisationProcessBundle\Entity\ScenarisationProcess $scenarisationProcess
     *
     * @return PedagogicalScenario
     */
    public function setScenarisationProcess(\ScenarisationProcessBundle\Entity\ScenarisationProcess $scenarisationProcess = null)
    {
        $this->scenarisationProcess = $scenarisationProcess;
        $scenarisationProcess->addPedagogicalScenario($this);

        return $this;
    }

    /**
     * Get scenarisationProcess
     *
     * @return \ScenarisationProcessBundle\Entity\ScenarisationProcess
     */
    public function getScenarisationProcess()
    {
        return $this->scenarisationProcess;
    }

    /**
     * Add answer
     *
     * @param \ScenarioBundle\Entity\Answer answer
     *
     * @return PedagogicalScenario
     */
    public function addAnswer(\ScenarioBundle\Entity\Answer $answer)
    {
        $this->answers[] = $answer;
        $answer->setPedagogicalScenario($this);

        return $this;
    }

    /**
     * Remove answer
     *
     * @param \ScenarioBundle\Entity\Answer $answer
     */
    public function removeAnswer(\ScenarioBundle\Entity\Answer $answer)
    {
        $this->answers->removeElement($answer);
    }

    /**
     * Get answers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * Add pedagogicalScenarioTrainingCourse
     *
     * @param \ScenarioBundle\Entity\PedagogicalScenarioTrainingCourse $pedagogicalScenarioTrainingCourse
     *
     * @return PedagogicalScenario
     */
    public function addPedagogicalScenarioTrainingCourse(\ScenarioBundle\Entity\PedagogicalScenarioTrainingCourse $pedagogicalScenarioTrainingCourse)
    {
        $this->pedagogicalScenarioTrainingCourse[] = $pedagogicalScenarioTrainingCourse;
        $pedagogicalScenarioTrainingCourse->setPedagogicalScenario($this);

        return $this;
    }

    /**
     * Delete pedagogicalScenarioTrainingCourse
     *
     * @param \ScenarioBundle\Entity\PedagogicalScenarioTrainingCourse PedagogicalScenarioTrainingCourse
     */
    public function removePedagogicalScenarioTrainingCourse(\ScenarioBundle\Entity\PedagogicalScenarioTrainingCourse $pedagogicalScenarioTrainingCourse)
    {
        $this->pedagogicalScenarioTrainingCourse->removeElement($pedagogicalScenarioTrainingCourse);
    }

    /**
     * Get pedagogicalScenarioTrainingCourse
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPedagogicalScenarioTrainingCourse()
    {
        return $this->pedagogicalScenarioTrainingCourse;
    }
}
