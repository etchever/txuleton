<?php

namespace ScenarioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GeneralAnswer
 *
 * @ORM\Table(name="general_answer")
 * @ORM\Entity(repositoryClass="ScenarioBundle\Repository\GeneralAnswerRepository")
 */
class GeneralAnswer extends Answer
{

  /**
   * @var int
   *
   * @ORM\Column(name="id", type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  private $id;

  /**
   * @var string
   *
   * @ORM\Column(name="textAnswer", type="text", nullable=true)
   */
  private $textAnswer;

  /**
   * @ORM\ManyToOne(targetEntity="ScenarisationProcessBundle\Entity\GeneralQuestion", inversedBy="generalAnswers", cascade={"persist"})
   * @ORM\JoinColumn(nullable=false)
   */
  private $generalQuestion;

  /**
   * @ORM\ManyToOne(targetEntity="ScenarioBundle\Entity\PedagogicalScenario", inversedBy="generalAnswers", cascade={"persist"})
   * @ORM\JoinColumn(nullable=false)
   */
  private $pedagogicalScenario;

  /**
   * Get id
   *
   * @return int
   */
  public function getId()
  {
      return $this->id;
  }

  /**
   * Set textAnswer
   *
   * @param string $textAnswer
   *
   * @return GeneralAnswer
   */
  public function setTextAnswer($textAnswer)
  {
      $this->textAnswer = $textAnswer;

      return $this;
  }

  /**
   * Get textAnswer
   *
   * @return string
   */
  public function getTextAnswer()
  {
      return $this->textAnswer;
  }

  /**
   * Set generalQuestion
   *
   * @param \ScenarisationProcessBundle\Entity\GeneralQuestion $generalQuestion
   *
   * @return GeneralAnswer
   */
  public function setGeneralQuestion(\ScenarisationProcessBundle\Entity\GeneralQuestion $generalQuestion)
  {
    $this->generalQuestion = $generalQuestion;

    $generalQuestion->addGeneralAnswer($this);

    return $this;
  }

  /**
   * Get generalQuestion
   *
   * @return \ScenarisationProcessBundle\Entity\GeneralQuestion
   */
  public function getGeneralQuestion()
  {
      return $this->generalQuestion;
  }

}
