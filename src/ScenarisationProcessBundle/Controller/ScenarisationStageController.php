<?php

namespace ScenarisationProcessBundle\Controller;

use ScenarisationProcessBundle\Entity\ScenarisationStage;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use ScenarisationProcessBundle\Entity\Question;
use ScenarisationProcessBundle\Entity\QuestionOrder;

/**
 * ScenarisationStage controller.
 *
 */
class ScenarisationStageController extends Controller
{
    /**
     * Lists all scenarisationStage entities.
     *
     */
    public function indexAction(Request $request)
    {
      // We generate the new breadcrumb
      $this->createBreadcrumb("index");
      // Mise en évidence du menu, changement du menu sélectionné
      $this->updateMenu();

        $em = $this->getDoctrine()->getManager();

        //$scenarisationStages = $em->getRepository('ScenarisationProcessBundle:ScenarisationStage')->findAll();
        $queryBuilder = $em->getRepository('ScenarisationProcessBundle:ScenarisationStage')->createQueryBuilder('scenarisationStages');
        $query = $queryBuilder->getQuery();

        $paginator  = $this->get('knp_paginator');

        $scenarisationStages = $paginator->paginate(
          $query,
          $request->query->getInt('page', 1)/*page number*/,
          5/*limit per page*/
        );
        return $this->render('ScenarisationProcessBundle:scenarisationstage:index.html.twig', array(
            'scenarisationStages' => $scenarisationStages
        ));
    }

    /**
     * Creates a new scenarisationStage entity.
     *
     */
    public function newAction(Request $request)
    {
      // We generate the new breadcrumb
      $this->createBreadcrumb("new");
      // Mise en évidence du menu, changement du menu sélectionné
      $this->updateMenu();

        $scenarisationStage = new ScenarisationStage();
        $form = $this->createForm('ScenarisationProcessBundle\Form\ScenarisationStageType', $scenarisationStage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($scenarisationStage);
            $em->flush();

            return $this->redirectToRoute('scenarisation_stage_show', array('id' => $scenarisationStage->getId()));
        }

        return $this->render('ScenarisationProcessBundle:scenarisationstage:new.html.twig', array(
            'scenarisationStage' => $scenarisationStage,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a scenarisationStage entity.
     *
     */
    public function showAction(ScenarisationStage $scenarisationStage)
    {

      // We generate the new breadcrumb
      $this->createBreadcrumb("show",$scenarisationStage);
      // Mise en évidence du menu, changement du menu sélectionné
      $this->updateMenu();

      $deleteForm = $this->createDeleteForm($scenarisationStage);

      //$questions = $scenarisationStage->getQuestionOrder()->getQuestion();
      $entityManager = $this->get('doctrine')->getManager();

       $requetePerso = $entityManager->createQuery ('
       select o,q
       FROM ScenarisationProcessBundle:QuestionOrder o
       JOIN o.question q
       WHERE o.scenarisationStage = :scenarisationStage');

       /*$requetePerso = $gestionnaireEntite->createQuery ('
       select s,o,q
       FROM ScenarisationProcessBundle:scenarisationStage s
       JOIN s.questionOrder o JOIN o.question q
       WHERE s = :scenarisationStage');*/

       $requetePerso->setParameter('scenarisationStage',$scenarisationStage);

        $questions_and_orders = $requetePerso->getResult();

        return $this->render('ScenarisationProcessBundle:scenarisationstage:show.html.twig', array(
            'scenarisationStage' => $scenarisationStage,
            'questions_and_orders' => $questions_and_orders,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing scenarisationStage entity.
     *
     */
    public function editAction(Request $request, ScenarisationStage $scenarisationStage)
    {
      // We generate the new breadcrumb
      $this->createBreadcrumb("edit",$scenarisationStage);
      // Mise en évidence du menu, changement du menu sélectionné
      $this->updateMenu();

        $deleteForm = $this->createDeleteForm($scenarisationStage);
        $editForm = $this->createForm('ScenarisationProcessBundle\Form\ScenarisationStageType', $scenarisationStage);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid())
        {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('scenarisation_stage_edit', array('id' => $scenarisationStage->getId()));
        }

        return $this->render('ScenarisationProcessBundle:scenarisationstage:edit.html.twig', array(
            'scenarisationStage' => $scenarisationStage,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a scenarisationStage entity.
     *
     */
    public function deleteAction(Request $request, ScenarisationStage $scenarisationStage)
    {
        $form = $this->createDeleteForm($scenarisationStage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($scenarisationStage);
            $em->flush();
        }

        /* Petit message de reussite, à traduire */
        $request->getSession()->getFlashBag()->add('success', "La démarche de scénarisation '" . $scenarisationStage->getTitle() . "' a bien été supprimée.");
        return $this->redirectToRoute('scenarisation_stage_index');
    }

    /**
     * Creates a form to delete a scenarisationStage entity.
     *
     * @param ScenarisationStage $scenarisationStage The scenarisationStage entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ScenarisationStage $scenarisationStage)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('scenarisation_stage_delete', array('id' => $scenarisationStage->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
      * Call in ajax.
      * Resorts QuestionOrders using their doctrine sortable property
      * @param integer $scen_stage_id the id of the scenarisation stage which we are modifing
      * @param integer $questionOrder_id id of the scenarisation stage order which we want to modify the position
      * @param integer $position the new position for the scenarisation stage
      */
    public function sortAction(Request $request, $scen_stage_id, $questionOrder_id , $position)
    {
      $em = $this->getDoctrine()->getManager();

      $scenarisationStage = $em->getRepository('ScenarisationProcessBundle:ScenarisationStage')->find($scen_stage_id);
      $questionOrder = $em->getRepository('ScenarisationProcessBundle:QuestionOrder')->find($questionOrder_id);

      // On a des vérifications supplémentaires à faire lorsqu'il y a une question séance ET une question séance dans l'étape
      if ($this->haveASequenceQuestion($scenarisationStage) && $this->haveASessionQuestion($scenarisationStage))
      {
        // Tableau dans lequel je vais enregristrer toutes les questions de l'étape
        $tab_questions = array();

        // J'enregistre
        foreach ($scenarisationStage->getQuestionOrder() as $currentQuestionOrder)
        {
          $tab_questions[$currentQuestionOrder->getPosition()] = $currentQuestionOrder;
        }

        // Je trie le tableau en fonction de la position
        ksort($tab_questions);

        // Simuler le déplacement
        $tab_questions_apres_deplacement = $this->deplacer($tab_questions,$questionOrder->getPosition(),$position);

        $deplacementAutorise = $this->testerCoherenceDeplacement($tab_questions_apres_deplacement);
      }
      else
      {
        $deplacementAutorise = true;
      }

      if ($deplacementAutorise == true)
      {
        $questionOrder->setPosition($position);

        $em->persist($questionOrder);
        $em->flush();
      }
      else
      {
        /* Petit message de reussite, à traduire */
        $request->getSession()->getFlashBag()->add('alert', "Impossible de déplacer la question comme demandé : vous ne pouvez pas positionner une question séance avant une question séance.");
      }

      return $this->questionsEditAction($request, $scenarisationStage);

    }


    /**
    * Displays a drag&drop table to edit the questions of a scenarisation stage.
    * @param ScenarisationStage $scenarisationStage
    */
    public function questionsEditAction(Request $request, ScenarisationStage $scenarisationStage)
    {
    /* Avant d'afficher la vue permettant d'ajouter/supprimer/ordonner les question
       d'une étape, on doit s'assurer de deux choses :
          - L'étape est-elle déjà utilisée dans une démarche concevant un scénario pédagogique ? ;
          - Si non, on peu afficher la vue, si oui il faut vérifier qu'elle ne possède aucune question
            ayant une réponse concernant un de ces scénarios pédagogique. */

      $entityManager = $this->get('doctrine')->getManager();

      /* [ARBITRAIRE] On part du principe qu'on a pas le droit d'accéder à la vue
         et on va effectuer au départ un certain nombre de vérifications */
      $modificationInterdite = TRUE;

      /* On va évaluer la première condition en cherchant à savoir si
         l'étape courante est utilisée dans une démarche. Pour cela,
         on récupère en base de données l'ensemble des démarches utilisant
         l'étape courante */
      $dql_demarches_utilisant_letape = $entityManager->createQuery ('
      select p, so, ps, a
      FROM ScenarisationProcessBundle:scenarisationProcess p
      JOIN p.scenarisationStageOrder so
      LEFT JOIN p.pedagogicalScenarios ps
      LEFT JOIN ps.answers a
      WHERE so.scenarisationStage = :scenarisationStage');
      $dql_demarches_utilisant_letape->setParameter('scenarisationStage',$scenarisationStage);
      $demarches_utilisant_letape  = $dql_demarches_utilisant_letape->getResult();

      /* Si au moins une démarche utilise l'étape
         on procède à d'autres vérifications */
      if (!empty($demarches_utilisant_letape))
      {
        /* Maintenant, on va regarder si parmi ces démarches,
           une d'entre elles est utilisée dans la conception
           d'un scénario pédagogique. On parlera de démarche 'scénarisante' */

        // On initialise un booléen de parcours à faux, en supposant qu'il n'y en a pas
        $demarcheScenarisanteTrouvee = FALSE;

        /* On construit un tableau à deux dimensions qui contiendra
        dans la premiere dimension les demarches de scenarisation utilisant l'étape
        dans la deuxieme dimension les scénarios pédagogiques étant scénarisés par la démarche
        (Dans la première dimension, on met uniquement des démarches scénarisant au moins
        un scénario pédagogique sinon c'est inutile.) */
        $tableau_des_tableaux_de_scenarios = array();

        /* On parcourt toutes les démarches de scénarisation utilisant l'étape couarnte */
        while (current($demarches_utilisant_letape) != FALSE)
        {
          /* On extrait les scénarios pédagogiques scénarisés par la démarche courante */
          $tab_scenarios_designes = current($demarches_utilisant_letape)->getPedagogicalScenarios()->getValues();

          /* Si la démarche de scénarisation courante scénarise un scénario pédagogique */
          if (sizeof($tab_scenarios_designes) > 0 )
          {
            /* On enregistre ce scénario pédagogique */
            array_push($tableau_des_tableaux_de_scenarios, $tab_scenarios_designes);
          }

          /* On passe à la démarche suivante */
          next($demarches_utilisant_letape);
        }

        /* Si au moins une démarche est utilisée dans la conception d'un scénario pédagogique */
        if (!empty($tableau_des_tableaux_de_scenarios))
        {
          /* Là, on passe à la deuxième vérification  :
             Vérifier que l'étape possède une réponse concernant un de ces scénario pédagogique */

          // Dans un premier temps, on récupère toutes les réponses aux questions de l'étape
          $dql_toutes_reponses_aux_questions = $entityManager->createQuery ('
          select a
          FROM ScenarioBundle:Answer a
          LEFT JOIN a.question q
          LEFT JOIN q.questionOrder qo
          LEFT JOIN qo.scenarisationStage st
          WHERE st = :scenarisationStage');
          $dql_toutes_reponses_aux_questions->setParameter('scenarisationStage',$scenarisationStage);
          /* Cette variable (tableau) contient toutes les réponses aux questions de l'étape courante */
          $toutesReponses  = $dql_toutes_reponses_aux_questions->getResult();

          /* S'il y a au moins une réponse à une des questions de l'étapen,
             il nous faut vérifier si les réponses concernent un des scénarios
             pédagogiques enregistrés dans le précédent tableau */
          if (!empty($toutesReponses))
          {
            // Par défaut, on suppose qu'il n'y a aucune réponse qui concerne un scénario pédagogique
            $trouve = false;

            /* Tant qu'on est pas arrivé au bout de notre parcours des réponses
               et
               qu'on a pas trouvé une réponse qui concerne un scénario pédagogique */
            while(current($toutesReponses) != FALSE && $trouve == false)
            {
              /* Il faut déterminer si le scénario pédagogique auquel répond la réponse est un des
                 scénarios pédagogique du tableau précédent */
              foreach ($tableau_des_tableaux_de_scenarios as $tableau_de_scenarios)
              {
                /* On cherche dans le tableau de scénarios courant, le scénario auquel répond la question
                   array_search retour la position si elle trouve un correpondance*/
                $trouve = array_search(current($toutesReponses)->getPedagogicalScenario(), $tableau_de_scenarios);
                if ($trouve != false)
                {
                  /* On sort du foreach */
                  break 1;
                }
              }
              next($toutesReponses);
            }

            /* Si on est arrivé en fin de boucle dans trouver aucune réponse
               associée à une des démarche, on autorise l'accès */
            if ($trouve === false)
            {
              $modificationInterdite = false;
            }
          }
          else
          {
            $modificationInterdite = false;
          }
        }
        else
        {
          $modificationInterdite = false;
        }
      }
      else
      {
        $modificationInterdite = false;
      }

      if ($modificationInterdite == false)
      {
        // We generate the new breadcrumb
        $this->createBreadcrumb("questions_management",$scenarisationStage);
        // Mise en évidence du menu, changement du menu sélectionné
        $this->updateMenu();

        // On récupère la liste des questions (et leur position) pour l'étape courante
        $question_etape_courante = $entityManager->createQuery ('
        select o,q
        FROM ScenarisationProcessBundle:questionOrder o
        JOIN o.question q
        WHERE o.scenarisationStage = :scenarisationStage
        ORDER BY o.position');

        // On récupère toutes les autres questions, çad celles qui ne sont pas dans la démarche courante
        $autres_questions = $entityManager->createQuery ('
        select q
        FROM ScenarisationProcessBundle:Question q
        WHERE q NOT IN (select qo
        FROM ScenarisationProcessBundle:QuestionOrder o
        JOIN o.question qo
        WHERE o.scenarisationStage = :scenarisationStage)');

       $question_etape_courante->setParameter('scenarisationStage',$scenarisationStage);
       $autres_questions->setParameter('scenarisationStage',$scenarisationStage);

       $questionOrders  = $question_etape_courante->getResult();
       $questionsNotLinked = $autres_questions->getResult();

       return $this->render('ScenarisationProcessBundle:scenarisationstage:questionsEdit.html.twig', array(
       'scenarisationStage' => $scenarisationStage,
       'questionOrders'     => $questionOrders,
       'questionsNotLinked' => $questionsNotLinked
        ));
    }
    else
    {
      /* Message d'erreur, à traduire */
      $request->getSession()->getFlashBag()->add('alert', 'Impossible de modifier les questions de l\'étape : au moins une question possède déjà une réponse.');

      return $this->redirectToRoute('scenarisation_stage_index');
    }

    }

    /**
     * Add a Question to a ScenarisationStage
     */
    public function questionsAddAction(Request $request, ScenarisationStage $scenarisationStage,Question $question)
    {
      $entityManager = $this->container->get('doctrine')->getEntityManager('default');

      /* ---------------------------------------------------------------
      Il y a un certain nombre de choses à vérifier avant de valider l'ajouter de la question

        1 : Si une (des) démarche(s) utilise(nt) l'étape :

          1.1 : Il faut vérifier qu'une aucune autre étape de ces démarche
                n'utilise déjà la question que l'on souhaite ajouter ;

          1.2.1 : Si c'est une question générale que l'on souhaite ajouter :
                  alors on peut l'ajouter

          1.2.2 : Si c'est une question séquence que l'on souhaite ajouter :
                  Pour chaque démarche utilisant l'étape, on s'assure qu'il n'y
                  a pas déjà une question séquence dans l'étape courante ni dans
                  les étapes précédentes.

        2 : Si aucune (des) démarche(s) utilise(nt) l'étape :

          2.1 : Si c'est une question générale, on l'ajoute ;

          2.2 : Si c'est une question séquence, on s'assure qu'il n'y en ait
                pas déjà une ;

          2.3 : Si c'est une question séance :

            2.3.1 : On s'assure qu'il y a une question séquence ;

            2.3.2 : On s'assure qu'il y n'y a pas déjà une question séance.

      ---------------------------------------------------------------  */

      // Par défaut, on interdit d'ajouter la question
      $autorise_a_ajouter_la_question = false;

      // Booléen pour savoir si la question apparait déjà, dans
      // une étape précédente
      $questionDejaDansUneAutreDemarche = false;

      // Première chose à vérifier : On liste les démarches utilisant
      // l'étape courante
      $dql_demarches_utilisant_letape = $entityManager->createQuery ('
      select p
      FROM ScenarisationProcessBundle:scenarisationProcess p
      JOIN p.scenarisationStageOrder so
      JOIN so.scenarisationStage s
      WHERE s = :scenarisationStage');
      $dql_demarches_utilisant_letape->setParameter('scenarisationStage',$scenarisationStage);
      $demarches_utilisant_letape  = $dql_demarches_utilisant_letape->getResult();

      /* 1 : Si une (des) démarche(s) utilise(nt) l'étape */
      if (!empty($demarches_utilisant_letape))
      {
        /* 1.1 : Il faut vérifier qu'une aucune autre étape de ces démarche
              n'utilise déjà la question que l'on souhaite ajouter */

        /* Je ne suis pas parvenu à récupérer en BDD les démarches (et tous les liens fils) pour uniquement
           Celle ayant l'étape courante en étape donc je procède différemment :
           Je récupère toutes les démarches et j'analyse si je dois les traiter ou pas en fonction
           de si la démarche utilise l'étape courante */

         $dql_toutes_demarches = $entityManager->createQuery ('
         select p, so, st
         FROM ScenarisationProcessBundle:scenarisationProcess p
         JOIN p.scenarisationStageOrder so
         JOIN so.scenarisationStage st');
         $toutesDemarches  = $dql_toutes_demarches->getResult();

        // Ici j'enregistre dans un tableau la liste des démarches qui utilisent l'étape.
        // Contrairement à l'autre variable '$demarches_utilisant_letape', celle-ci possèdera tous les liens fils
        $demarches_utilisant_letape2 = array();

        /* On parcourt toutes les démarches de Txuleton */
        foreach ($toutesDemarches as $demarcheCourante)
        {
          // Si la démarche courante est une démarche utilisant l'étape courante
          $demarcheCouranteUtiliseEtape = false;

          /* On parcourt le tableau des démarches qui utilisent l'étape et on
             on regarde si c'est la démarche fait partie de l'une d'elle*/
          foreach ($demarches_utilisant_letape as $parcoursDemarchesUtilisantEtape)
          {
            if ($parcoursDemarchesUtilisantEtape->getId() == $demarcheCourante->getId())
            {
              $demarcheCouranteUtiliseEtape = true;
              break 1;
            }
          }

          /* Si la démarche courante utilise l'étape */
          if ($demarcheCouranteUtiliseEtape)
          {
            array_push($demarches_utilisant_letape2, $demarcheCourante);

            /* Si on a déjà trouvé la question dans une autre démarche, c'est pas la peine de continuer à boucler */
            if ($questionDejaDansUneAutreDemarche == false)
            {
              foreach ($demarcheCourante->getScenarisationStageOrder() as $scenarisationStageOrderCourante)
              {
                foreach ($scenarisationStageOrderCourante->getScenarisationStage()->getQuestionOrder() as $questionOrderCourante)
                {
                  $questionCourante = $questionOrderCourante->getQuestion();

                  if ($questionCourante->getId() == $question->getId())
                  {
                    $questionDejaDansUneAutreDemarche = true;
                    break 3;
                  }
                }
              }
            }
          }
        }

        if ($questionDejaDansUneAutreDemarche == false)
        {
          /* 1.2.1 : Si la question est une question générale, on peut l'ajouter */
          if ($question->getType() == 'generalQuestion')
          {
            $autorise_a_ajouter_la_question = true;
          }
          else
          {
            /* 1.2.2 : Si c'est une question séquence que l'on souhaite ajouter :
                       Pour chaque démarche utilisant l'étape, on s'assure qu'il n'y
                       a pas déjà une question séquence dans l'étape courante ni dans
                       les étapes précédentes. */
            if ($question->getType() == 'sequenceQuestion')
            {
              $dejaUneQuestionSequenceDansUneDemarche = false;

              foreach ($demarches_utilisant_letape2 as $demarcheCourante)
              {
                foreach ($demarcheCourante->getScenarisationStageOrder() as $scenStageOrder)
                {
                  if ($this->haveASequenceQuestion($scenStageOrder->getScenarisationStage()))
                  {
                    $dejaUneQuestionSequenceDansUneDemarche = true;
                    break 2;
                  }
                }
              }

              if ($dejaUneQuestionSequenceDansUneDemarche)
              {
                /* Message d'erreur, à traduire */
                $request->getSession()->getFlashBag()->add('alert', "Il est impossible d'ajouter la question séquence à l'étape. L'étape est utilisée dans une démarche dans laquelle une question séquence est déjà présente. Or, une démarche ne peut pas avoir deux questions séquence.");
              }
              else
              {
                // Il faut qu'on s'assuer que dans l'étape courant il n'y a pas une question séance
                if ($this->haveASessionQuestion($scenarisationStage))
                {
                  /* Message d'erreur, à traduire */
                  $request->getSession()->getFlashBag()->add('alert', "Il est impossible d'ajouter la question séquence à l'étape. Veuillez d'abord retirer la question séance.");

                }
                else
                {
                  $autorise_a_ajouter_la_question = true;
                }

              }
            }
            // si c'est une question séance
            else
            {
              /* ---------------------------------------------------------------
              1.2.3 : Si c'est une question séance que l'on souhaite ajouter, pour chaque
                      démarche utilisant l'étape :

                [ANNULEE]- 1.2.3.1 : Il faut qu'il y ait une question séquence dans une étape précédente ou l'actuelle ;
                                    Cette condition a été annulée.

                - 1.2.3.2 : Il faut qu'aucune des démarches ne possède déjà de quesiton séance.
              --------------------------------------------------------------- */

              /* ---------------------------------------------------------------
              1.2.3.1 : Il faut qu'il y ait une question séquence dans une étape précédente ou l'actuelle
              --------------------------------------------------------------- */

              $peut_passer_a_letape_2 = false;

              $a_une_question_sequence_avant = false;
              // Est-ce que l'étape courante a une question séquence
              if ($this->haveASequenceQuestion($scenarisationStage))
              {
                // Il y a une question séquene dans l'étape actuelle, on peut passer à l'étape 2
                $peut_passer_a_letape_2 = true;
              }
              else
              {
                // Pour chaque démarche
                foreach ($demarches_utilisant_letape2 as $demarcheCourante)
                {
                  $a_une_question_sequence_avant = false;

                  // Position de l'étape courante dans la démarche courante
                  foreach ($demarcheCourante->getScenarisationStageOrder() as $currentScenOrder)
                  {
                    if ($currentScenOrder->getScenarisationStage()->getId() == $scenarisationStage->getId())
                    {
                      $positionEtapeCouranteDansDemarcheCourante = $currentScenOrder->getPosition();
                      break;
                    }
                  }
                  // On peut tester les étapes précédentes
                  foreach ($demarcheCourante->getScenarisationStageOrder() as $currentScenOrder)
                  {
                    /* On ne teste uniquement les étapes précédentes à l'actuelle puique l'actuelle a déjà
                       été testée */
                    if ($currentScenOrder->getPosition() < $positionEtapeCouranteDansDemarcheCourante)
                    {
                      if ($this->haveASequenceQuestion($currentScenOrder->getScenarisationStage()))
                      {
                        // On a trouvé une question séquence avant l'étape courante, on peut
                        // passer à l'étape 2
                        $a_une_question_sequence_avant = true;
                        break;
                      }
                    }
                  }

                  /* Si la démarche que l'on vient de tester n'a pas de question
                  sequence avant l'étape courante, on sort */
                  if ($a_une_question_sequence_avant == false)
                  {
                    $peut_passer_a_letape_2 = false;
                    break;
                  }
                  else
                  {
                    $peut_passer_a_letape_2 = true;
                  }
                }

              }

              /* ---------------------------------------------------------------
              1.2.3.2 : Il faut qu'aucune des démarches ne possède déjà de quesiton séance.
              --------------------------------------------------------------- */
              if ($peut_passer_a_letape_2)
              {
                foreach ($demarches_utilisant_letape2 as $demarcheCourante)
                {
                  foreach ($demarcheCourante->getScenarisationStageOrder() as $scenStageOrder)
                  {
                    // Si une seule des démarches possède déjà une question séance, c'est perdu !
                    if ($this->haveASessionQuestion($scenStageOrder->getScenarisationStage()))
                    {
                      $request->getSession()->getFlashBag()->add('alert', "Impossible d'ajouter cette question séance car une des démarches utilisant l'étape possède déjà une question séance.");
                      $autorise_a_ajouter_la_question = false;
                      break 2;
                    }
                    else
                    {
                      $autorise_a_ajouter_la_question = true;
                    }
                  }
                }
              }
              else
              {
                /* Message d'erreur, à traduire */
                $request->getSession()->getFlashBag()->add('alert', "Il est impossible d'ajouter la question séance à l'étape. L'étape est utilisée dans une démarche dans laquelle il n'y a pas de question séquence positionnée avant. Or, une démarche doit avoir une question séquence avant une question séance.");

              }
            }
          }
        }
        else
        {
          $autorise_a_ajouter_la_question = false;
          /* Message d'erreur, à traduire */
          $request->getSession()->getFlashBag()->add('alert', "Il est impossible d'ajouter la question à l'étape. L'étape est utilisée dans une démarche dans laquelle la question que vous souhaitez ajouter est déjà présente. Or, une démarche ne peut pas avoir deux fois la même question.");
        }
      }
      /* 2 : Si aucune démarche n'utilise l'étape */
      else
      {
        /* 2.1 : Si c'est une question générale, on l'ajoute ; */
        if ($question->getType() == 'generalQuestion')
        {
          $autorise_a_ajouter_la_question = true;
        }
        /* 2.2 : Si c'est une question séquence, on s'assure qu'il n'y en ait
              pas déjà une  ET qu'il n'y a pas de question séance ; */
        else if ($question->getType() == 'sequenceQuestion')
        {
          // Je m'assure que l'étape n'a pas déjà une question sequecne
          if($this->haveASequenceQuestion($scenarisationStage) == false)
          {
            // Il faut qu'on s'assuer que dans l'étape courant il n'y a pas une question séance
            if ($this->haveASessionQuestion($scenarisationStage))
            {
              /* Message d'erreur, à traduire */
              $request->getSession()->getFlashBag()->add('alert', "Il est impossible d'ajouter la question séquence à l'étape. Veuillez d'abord retirer la question séance.");

            }
            else
            {
              $autorise_a_ajouter_la_question = true;
            }
          }
          else
          {
            /* Message d'erreur, à traduire */
            $request->getSession()->getFlashBag()->add('alert', "Impossible d'ajouter la question séquence à l'étape car l'étape possède déjà une question séquence.");
          }
        }
        /* 2.3 : Si c'est une question séance : */
        else
        {
          /* Finalement, je tolère de créer une étape avec uniquement une question séance
          2.3.1 : On s'assure qu'il y a une question séquence ; */
          /*if ($this->haveASequenceQuestion($scenarisationStage) == false)
          {
            //Message d'erreur, à traduire
            $request->getSession()->getFlashBag()->add('alert', "Impossible d'ajouter la question séance à l'étape car l'étape ne possède pas de question séquence.");
          }
          else
          {*/
            /* 2.3.2 : On s'assure qu'il y n'y a pas déjà une question séance. */
            if($this->haveASessionQuestion($scenarisationStage) == true)
            {
              /* Message d'erreur, à traduire */
              $request->getSession()->getFlashBag()->add('alert', "Impossible d'ajouter la question séance à l'étape car l'étape possède déjà une question séance.");
            }
            else
            {
              $autorise_a_ajouter_la_question = true;
            }
          //}//
        }
      }

      if ($autorise_a_ajouter_la_question)
      {
        $questionOrder = new QuestionOrder();

        // Ici on vérifie si on est en train d'ajouter notre première question à notre étape
        // Car L'extension Sortable de Doctrine commence la position à 0
        // Et je n'ai trouvé aucune manière moins gourmande et plus élégante de faire ça

        $dql_question_order_count = $entityManager->createQuery('SELECT count(q)
                                                                 FROM ScenarisationProcessBundle:QuestionOrder q
                                                                 WHERE q.scenarisationStage = :scenarisationStage');
        $dql_question_order_count->setParameter('scenarisationStage',$scenarisationStage);
        $nbQuestionOrder = $dql_question_order_count->getResult();

        // Si l'étape de scénarisation ne contient aucune question
        if ($nbQuestionOrder[0][1] == 0)
          $questionOrder->setPosition(1);
        // ----------------------------

        $question->addQuestionOrder($questionOrder);
        $scenarisationStage->addQuestionOrder($questionOrder);

        $entityManager->persist($questionOrder);
        $entityManager->flush();
      }

      return $this->redirectToRoute('scenarisation_stage_question_management', array('id' => $scenarisationStage->getId(), 'already' => false));
  }

    /**
     * Remove a Question from a ScenarisationStage
     * Pas de contrôle à faire puisqu'on peut enlever n'importe quelle question sans soucis
     */
    public function questionsRemoveAction(ScenarisationStage $scenarisationStage,QuestionOrder $questionOrder)
    {
      $manager = $this->container->get('doctrine')->getEntityManager('default');

      $manager->remove($questionOrder);
      $manager->flush();

      return $this->redirectToRoute('scenarisation_stage_question_management', array('id' => $scenarisationStage->getId()));

    }

    /**
    * Displays some drag&drop tables to edit the processes in which the main scenarisationStage appears
    * @param ScenarisationStage $scenarisationStage
     */
    public function associateToProcessAction(Request $request, ScenarisationStage $scenarisationStage)
    {
      // We generate the new breadcrumb
      $this->createBreadcrumb("scenarisation_processes_management",$scenarisationStage);
      // Mise en évidence du menu, changement du menu sélectionné
      $this->updateMenu();

      /* Pour Paginator
      Si un numéro de page a été spécifié, il faudra renvoyer vers cette dernière */
      if ( !empty($request->query->get('page')) )
        $currentPage = $request->query->get('page');
      else
        $currentPage = 1; /* Par défaut, on montre la première page */


      $manager = $this->container->get('doctrine')->getEntityManager('default');

      // Récupérer toutes les démarches
      // On récupère la liste des démarches (et leur étapes)
      /*$dql_get_all_processes = $manager->createQuery ('
      select process,stageOrder,stage
      FROM ScenarisationProcessBundle:ScenarisationProcess process
      JOIN process.scenarisationStageOrder stageOrder
      JOIN stageOrder.scenarisationStage stage
      ORDER BY stageOrder.position');

      $allProcesses  = $dql_get_all_processes->getResult();*/

      $dql_allProcesses = $manager->createQuery ('
      select process,stageOrder,stage
      FROM ScenarisationProcessBundle:ScenarisationProcess process
      LEFT JOIN process.scenarisationStageOrder stageOrder
      LEFT JOIN stageOrder.scenarisationStage stage
      ORDER BY process.title, stageOrder.position');


      $paginator  = $this->get('knp_paginator');
      $allProcesses = $paginator->paginate(
        $dql_allProcesses,
        $request->query->getInt('page', $currentPage),/*page number*/
        2,/*limit per page*/
        array('wrap-queries'=>true) // Obligatoire pour faire fonctionner le order by
        );

      return $this->render('ScenarisationProcessBundle:scenarisationstage:processManagement.html.twig', array(
      'processes' => $allProcesses,
      'scenarisationStage' => $scenarisationStage
       ));
    }

    // Il faut que je m'occupe de cette fonction..
    public function cloneAction(Request $request, ScenarisationStage $scenarisationStage)
    {
      $em = $this->container->get('doctrine')->getEntityManager('default');

      // On crée la nouvelle étape de scénarisation
      $newScenarisationStage = new ScenarisationStage();

      // On copie les attributs
      $newScenarisationStage->setTitle("Copie - " . $scenarisationStage->getTitle());
      $newScenarisationStage->setGoals($scenarisationStage->getGoals());
      $newScenarisationStage->setDescription($scenarisationStage->getDescription());

      // On clone chacune des questions avec le lien
      foreach ($scenarisationStage->getQuestionOrder() as $currentQuestionOrder)
      {
        /* On clone la question */
        $questionCopie  = clone $currentQuestionOrder->getQuestion();

        $questionCopie->setFrWording("Copie - " . $questionCopie->getFrWording());
        $questionCopie->setEsWording("Copia - " . $questionCopie->getEsWording());
        $questionCopie->setEnWording("Copy - " . $questionCopie->getEnWording());

        // On clone la classe d'association
        $questionOrderCopie = clone $currentQuestionOrder;

        /* On fait les liens entre étape -> association && question -> association */
        $questionCopie->addQuestionOrder($questionOrderCopie);
        $newScenarisationStage->addQuestionOrder($questionOrderCopie);
      }

      $em->persist($newScenarisationStage);
      $em->flush();

      /* Message de reussite, à traduire */
      $request->getSession()->getFlashBag()->add('success', 'L\'étape \'' . $scenarisationStage->getTitle() . '\' a bien été clonée !');
      return $this->redirectToRoute('scenarisation_stage_index');

    }

    private function updateMenu()
    {
      // Mise en évidence du menu, changement du menu sélectionné
      $menu = array('pedagogical_scenario' => "",
                    'projects'             => "",
                    'trainingCourse'       => "",
                    'user'                 => "",
                    'parameters'           => "",
                    'scenarisation_process'=> "",
                    'scenarisation_stage'  => "list-group-item-info",
                    'question'             => "");

      // Mise en session du menu
      $this->get('session')->set('menu',$menu);
    }

    private function createBreadcrumb($currentAction, $scenarisationStage = null)
    {
      $breadcrumbs = $this->get("white_october_breadcrumbs");
      switch($currentAction)
      {
        case "index":
          $breadcrumbs->addItem("scenarisationStage");
        break;

        case "show":
          $breadcrumbs->addRouteItem("scenarisationStage", "scenarisation_stage_index");
          $breadcrumbs->addItem($scenarisationStage->getTitle());
          $breadcrumbs->addItem("show");

        break;

        case "new":
          $breadcrumbs->addRouteItem("scenarisationStage", "scenarisation_stage_index");
          $breadcrumbs->addItem("scenarisationStage.new");
        break;

        case "edit":
          $breadcrumbs->addRouteItem("scenarisationStage", "scenarisation_stage_index");
          $breadcrumbs->addRouteItem($scenarisationStage->getTitle(), "scenarisation_stage_show", [
              'id' => $scenarisationStage->getId()
          ]);
          $breadcrumbs->addItem("edit");

        break;

        case "scenarisation_processes_management":
          $breadcrumbs->addRouteItem("scenarisationStage", "scenarisation_stage_index");
          $breadcrumbs->addRouteItem($scenarisationStage->getTitle(), "scenarisation_stage_show", [
              'id' => $scenarisationStage->getId()
          ]);
          $breadcrumbs->addItem("scenarisationStage.associateToAprocess");
        break;

        case "questions_management":
          $breadcrumbs->addRouteItem("scenarisationStage", "scenarisation_stage_index");
          $breadcrumbs->addRouteItem($scenarisationStage->getTitle(), "scenarisation_stage_show", [
              'id' => $scenarisationStage->getId()
          ]);
          $breadcrumbs->addItem("scenarisationStage.questionsManagement");
        break;
      }
    }

    private function haveASequenceQuestion(scenarisationStage $scenarisationStage)
    {
      $haveASequenceQuestion = false;

      foreach ($scenarisationStage->getQuestionOrder() as $currentQuestionOrder)
      {
        if ($currentQuestionOrder->getQuestion()->getType() == 'sequenceQuestion')
        {
          $haveASequenceQuestion = true;
          break 1;
        }
      }

      return $haveASequenceQuestion;
    }

    private function haveASessionQuestion(scenarisationStage $scenarisationStage)
    {
      $haveASessionQuestion = false;

      foreach ($scenarisationStage->getQuestionOrder() as $currentQuestionOrder)
      {
        if ($currentQuestionOrder->getQuestion()->getType() == 'sessionQuestion')
        {
          $haveASessionQuestion = true;
          break 1;
        }
      }

      return $haveASessionQuestion;
    }

    private function getStagePositionInProcess($demarcheCourante,$scenarisationStage)
    {
      foreach ($demarcheCourante->getScenarisationStageOrder() as $currentScenarisationStageOrder)
      {
        if ($currentScenarisationStageOrder->getScenarisationStage()->getId() == $scenarisationStage->getId())
        {
          return $currentScenarisationStageOrder->getPosition();
        }
      }
    }

  /* Cette fonction permet de déplacer dans un tableau
     Un élément en position initiale vers la nouvelle position.
     Pour un tableau trié : [1] [2] [3] [4] [5] [6]
     Si on demande a déplacer 5 en 2, le tableau retourné est
     le suivant : [1] [5] [2] [3] [4] [6]

     Si on demande a déplacer 2 en 5, le tableau retourné est
     le suivant : [1] [3] [4] [5] [2] [6]
     */
    private function deplacer($tab, int $positionInitiale, int $nouvellePosition)
    {
    	if ($nouvellePosition != $positionInitiale)
    	{
    		$pivot                   = $tab[$nouvellePosition];
    		$tab[$nouvellePosition]  = $tab[$positionInitiale];

    		if ($nouvellePosition < $positionInitiale)
    		{
    			for ($i=$nouvellePosition+1 ; $i <= $positionInitiale ; $i++)
    			{
    				$pivott  = $tab[$i];
    				$tab[$i] = $pivot;
    				$pivot   = $pivott;
    			}
    		}
    		else
    		{
    			for ($i=$nouvellePosition-1 ; $i >= $positionInitiale ; $i--)
    			{
    				$pivott  = $tab[$i];
    				$tab[$i] = $pivot;
    				$pivot   = $pivott;
    			}
    		}
    	}

    	return $tab;
    }

    /* Cette méthode permet de tester la cohérence d'un tableau de questions lors de la simulation
       du changement de la position des questions */
    private function testerCoherenceDeplacement($tab_questions_order)
    {
      $coherent = false;

      foreach ($tab_questions_order as $currentQuestionOrder)
      {
        if ($currentQuestionOrder->getQuestion()->getType() == "sessionQuestion")
        {
          break;
        }
        else if ($currentQuestionOrder->getQuestion()->getType() == "sequenceQuestion")
        {
          // On vient de trouver une question sequence avant une question séance
          $coherent = true;
          break;
        }
      }
      return $coherent;
    }

}
