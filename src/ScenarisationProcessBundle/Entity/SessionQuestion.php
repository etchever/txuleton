<?php

namespace ScenarisationProcessBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SessionQuestion
 *
 * @ORM\Table(name="session_question")
 * @ORM\Entity(repositoryClass="ScenarisationProcessBundle\Repository\SessionQuestionRepository")
 */
class SessionQuestion extends Question
{
  /**
   * @ORM\OneToMany(targetEntity="ScenarioBundle\Entity\SessionAnswer", mappedBy="sessionQuestion", cascade={"persist"})
   * @ORM\JoinColumn(nullable=false)
   */
  private $sessionAnswers;

  /**
   * Add sessionAnswer
   *
   * @param \ScenarioBundle\Entity\SessionAnswer sessionAnswer
   *
   * @return SessionQuestion
   */
  public function addSessionAnswer(\ScenarioBundle\Entity\SessionAnswer $sessionAnswer)
  {
      $this->sessionAnswers[] = $sessionAnswer;

      return $this;
  }

  /**
   * Remove sessionAnswer
   *
   * @param \ScenarioBundle\Entity\ReponseGenerale $sessionAnswer
   */
  public function removeSessionAnswer(\ScenarioBundle\Entity\SessionAnswer $sessionAnswer)
  {
      $this->sessionAnswers->removeElement($sessionAnswer);
  }

  /**
   * Get sessionAnswer
   *
   * @return \Doctrine\Common\Collections\Collection
   */
  public function getSessionAnswers()
  {
      return $this->sessionAnswers;
  }

  public function getType()
  {
    return 'sessionQuestion';
  }
}
