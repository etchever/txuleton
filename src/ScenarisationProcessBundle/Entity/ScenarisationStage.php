<?php

namespace ScenarisationProcessBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ScenarisationStage
 *
 * @ORM\Table(name="scenarisation_stage")
 * @ORM\Entity(repositoryClass="ScenarisationProcessBundle\Repository\ScenarisationStageRepository")
 */
class ScenarisationStage
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="goals", type="text", nullable=true)
     */
    private $goals;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity="ScenarisationProcessBundle\Entity\QuestionOrder", mappedBy="scenarisationStage", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $questionOrder;

    /**
     * @ORM\OneToMany(targetEntity="ScenarisationProcessBundle\Entity\ScenarisationStageOrder", mappedBy="scenarisationStage", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $scenarisationStageOrder;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return ScenarisationStage
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set goals
     *
     * @param string $goals
     *
     * @return ScenarisationStage
     */
    public function setGoals($goals)
    {
        $this->goals = $goals;

        return $this;
    }

    /**
     * Get goals
     *
     * @return string
     */
    public function getGoals()
    {
        return $this->goals;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return ScenarisationStage
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->modelesDocumentScenarisation = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add questionOrder
     *
     * @param \ScenarisationProcessBundle\Entity\QuestionOrder $questionOrder
     *
     * @return ScenarisationStage
     */
    public function addQuestionOrder(\ScenarisationProcessBundle\Entity\QuestionOrder $questionOrder)
    {
        $this->questionOrder[] = $questionOrder;
        $questionOrder->setScenarisationStage($this);

        return $this;
    }

    /**
     * Remove questionOrder
     *
     * @param \ScenarisationProcessBundle\Entity\QuestionOrder $questionOrder
     */
    public function removeQuestionOrder(\ScenarisationProcessBundle\Entity\QuestionOrder $questionOrder)
    {
        $this->questionOrder->removeElement($questionOrder);
    }

    /**
     * Get questionOrder
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuestionOrder()
    {
        return $this->questionOrder;
    }

    /**
     * Add scenarisationStageOrder
     *
     * @param \ScenarisationProcessBundle\Entity\ScenarisationStageOrder $scenarisationStageOrder
     *
     * @return ScenarisationStage
     */
    public function addScenarisationStageOrder(\ScenarisationProcessBundle\Entity\ScenarisationStageOrder $scenarisationStageOrder)
    {
        $this->scenarisationStageOrder[] = $scenarisationStageOrder;
        $scenarisationStageOrder->setScenarisationStage($this);

        return $this;
    }

    /**
     * Remove scenarisationStageOrder
     *
     * @param \ScenarisationProcessBundle\Entity\ScenarisationStageOrder $scenarisationStageOrder
     */
    public function removeScenarisationStageOrder(\ScenarisationProcessBundle\Entity\ScenarisationStageOrder $scenarisationStageOrder)
    {
        $this->scenarisationStageOrder->removeElement($scenarisationStageOrder);
    }

    /**
     * Get scenarisationStageOrder
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getScenarisationStageOrder()
    {
        return $this->scenarisationStageOrder;
    }
}
