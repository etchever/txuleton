<?php

namespace ScenarisationProcessBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ScenarisationProcess
 *
 * @ORM\Table(name="scenarisation_process")
 * @ORM\Entity(repositoryClass="ScenarisationProcessBundle\Repository\ScenarisationProcessRepository")
 */
class ScenarisationProcess
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity="ScenarisationProcessBundle\Entity\ScenarisationStageOrder", mappedBy="scenarisationProcess", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $scenarisationStageOrder;

    /**
     * @ORM\OneToMany(targetEntity="ScenarioBundle\Entity\PedagogicalScenario", mappedBy="scenarisationProcess", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $pedagogicalScenarios;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User", inversedBy="scenarisationProcessesCreated", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    private $author;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return ScenarisationProcess
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return ScenarisationProcess
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->etapesScenarisation = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
      return $this->getTitle();
    }

    /**
     * Add scenarisationStageOrder
     *
     * @param \ScenarisationProcessBundle\Entity\ScenarisationStageOrder $scenarisationStageOrder
     *
     * @return ScenarisationProcess
     */
    public function addScenarisationStageOrder(\ScenarisationProcessBundle\Entity\ScenarisationStageOrder $scenarisationStageOrder)
    {
        $this->scenarisationStageOrder[] = $scenarisationStageOrder;
        $scenarisationStageOrder->setScenarisationProcess($this);

        return $this;
    }

    /**
     * Remove scenarisationStageOrder
     *
     * @param \ScenarisationProcessBundle\Entity\ScenarisationStageOrder $scenarisationStageOrder
     */
    public function removeScenarisationStageOrder(\ScenarisationProcessBundle\Entity\ScenarisationStageOrder $scenarisationStageOrder)
    {
        $this->scenarisationStageOrder->removeElement($scenarisationStageOrder);
    }

    /**
     * Get scenarisationStageOrder
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getScenarisationStageOrder()
    {
        return $this->scenarisationStageOrder;
    }


    /**
     * Add pedagogicalScenario
     *
     * @param \ScenarioBundle\Entity\PedagogicalScenario $pedagogicalScenario
     *
     * @return ScenarisationProcess
     */
    public function addPedagogicalScenario(\ScenarioBundle\Entity\PedagogicalScenario $pedagogicalScenario)
    {
        $this->pedagogicalScenarios[] = $pedagogicalScenario;

        return $this;
    }

    /**
     * Remove pedagogicalScenario
     *
     * @param \ScenarioBundle\Entity\PedagogicalScenario $pedagogicalScenario
     */
    public function removePedagogicalScenario(\ScenarioBundle\Entity\PedagogicalScenario $pedagogicalScenario)
    {
        $this->pedagogicalScenarios->removeElement($pedagogicalScenario);
    }

    /**
     * Get pedagogicalScenarios
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPedagogicalScenarios()
    {
        return $this->pedagogicalScenarios;
    }

    /**
     * Get the Author of the main Scenarisation Process
     *
     * @return \UserBundle\Entity\User
     */
    public function getAuthor()
    {
      return $this->author;
    }

    /**
     * Set the Author of the main Scenarisation Process
     *
     * @param \UserBundle\Entity\User $author
     *
     * @return ScenarisationProcess
     */
    public function setAuthor($author)
    {
      $this->author = $author;
      $author->addScenarisationProcessCreated($this);

      return $this;
    }

  /**
    * This method check if the current ScenarisationProcess is already
    * liked to a ScenarisationStage given.
    *
    * This method is only used by the view ScenarioBundle:scenarisationstage:processManagement.html.twig
    */
    public function isAlreadyLinkedToStage(ScenarisationStage $scenarisationStage)
    {
      // Faire une requête à partir d'une entité est une très mauvaise pratique, j'abandonne donc l'idée
      /*
      $dql= $this->createQueryBuilder('
      select count(stageOrder)
      FROM ScenarisationProcessBundle:ScenarisationStageOrder stageOrder
      JOIN stageOrder.scenarisationStage stage
      JOIN stageOrder.scenarisationProcess process
      WHERE process.id = idP AND stage.id = idS');

      $dql->setParameter('idP', $this->getId());
      $dql->setParameter('idS', $scenarisationStage->getId());
      $resultat = $dql->getSingleScalarResult();
      */

      // Booléen qui informe sur l'existance d'un lien entre la démarche et l'étape
      $etapeTrouvee = false;
      $scenarisationStageOrderTab = $this->getScenarisationStageOrder();

      // On initialise avant le parcours de toutes les étapes de scénarisation liées à la démarche courante
      $indiceCourant = 0;
      $tailleTableau = sizeof($scenarisationStageOrderTab);

      while( ($indiceCourant < $tailleTableau) && ($etapeTrouvee == false) )
      {
        if ($scenarisationStageOrderTab[$indiceCourant]->getScenarisationStage() == $scenarisationStage)
          $etapeTrouvee = true;

        $indiceCourant++;
      }

      return $etapeTrouvee;
    }

}
