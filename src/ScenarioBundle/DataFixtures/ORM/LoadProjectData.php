<?php

// src/ScenarioBundle/DataFixtures/ORM/LoadProjectData.php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use ScenarioBundle\Entity\Project;

class LoadProjetData implements FixtureInterface, OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        //$this->ajouterDutInfo($manager);
    }

    public function ajouterDutInfo(ObjectManager $manager)
    {
      $dutInfo = new Project();

      $dutInfo->setShortname("DUT INFO");
      $dutInfo->setFullname("Diplome Universitaire de Technologie Informatique");

      $manager->persist($dutInfo);
      $manager->flush();
    }

    public function getOrder()
    {
        return 2;
    }
}
