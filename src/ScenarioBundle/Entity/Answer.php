<?php

namespace ScenarioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Answer
 *
 * @ORM\Entity(repositoryClass="ScenarioBundle\Repository\AnswerRepository")
 *
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({"answer" = "Answer", "generalAnswer" = "GeneralAnswer", "sequenceAnswer" = "SequenceAnswer", "sessionAnswer" = "SessionAnswer"})
 */
class Answer
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="pedagogicalEngineerObservation", type="text", nullable=true)
     */
    private $pedagogicalEngineerObservation;

    /**
     * @var bool
     *
     * @ORM\Column(name="isValid", type="boolean")
     */
    private $isValid;

    /**
     * @ORM\ManyToOne(targetEntity="ScenarisationProcessBundle\Entity\Question", inversedBy="answers", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    private $question;

    /**
     * @ORM\ManyToOne(targetEntity="ScenarioBundle\Entity\PedagogicalScenario", inversedBy="answers", cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $pedagogicalScenario;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pedagogicalEngineerObservation
     *
     * @param string $pedagogicalEngineerObservation
     *
     * @return Answer
     */
    public function setPedagogicalEngineerObservation($pedagogicalEngineerObservation)
    {
        $this->pedagogicalEngineerObservation = $pedagogicalEngineerObservation;

        return $this;
    }

    /**
     * Get pedagogicalEngineerObservation
     *
     * @return string
     */
    public function getPedagogicalEngineerObservation()
    {
        return $this->pedagogicalEngineerObservation;
    }

    /**
     * Set isValid
     *
     * @param boolean $isValid
     *
     * @return Answer
     */
    public function setIsValid($isValid)
    {
        $this->isValid = $isValid;

        return $this;
    }

    /**
     * Get isValid
     *
     * @return bool
     */
    public function getIsValid()
    {
        return $this->isValid;
    }

    /**
     * Set question
     *
     * @param \ScenarisationProcessBundle\Entity\Question $question
     *
     * @return Answer
     */
    public function setQuestion(\ScenarisationProcessBundle\Entity\Question $question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return \ScenarisationProcessBundle\Entity\Question
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Get pedagogicalScenario
     *
     * @return \ScenarioBundle\Entity\PedagogicalScenario
     */
    public function getPedagogicalScenario()
    {
        return $this->pedagogicalScenario;
    }

    /**
     * Set pedagogicalScenario
     *
     * @param \ScenarioBundle\Entity\PedagogicalScenario $pedagogicalScenario
     *
     * @return Answer
     */
    public function setPedagogicalScenario(\ScenarioBundle\Entity\PedagogicalScenario $pedagogicalScenario)
    {
        $this->pedagogicalScenario = $pedagogicalScenario;

        return $this;
    }
}
