<?php

namespace ScenarisationProcessBundle\Controller;

use ScenarisationProcessBundle\Entity\ScenarisationProcess;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use ScenarisationProcessBundle\Entity\ScenarisationStage;
use ScenarisationProcessBundle\Entity\ScenarisationStageOrder;

/**
 * ScenarisationProcess controller.
  */
class ScenarisationProcessController extends Controller
{
    /**
     * Lists all scenarisationProcess entities.
     */
    public function indexAction(Request $request)
    {
      // We generate the new breadcrumb
      $this->createBreadcrumb("index");
      // Mise en évidence du menu, changement du menu sélectionné
      $this->updateMenu();

        $em = $this->getDoctrine()->getManager();

        //$scenarisationProcesses = $em->getRepository('ScenarisationProcessBundle:ScenarisationProcess')->findAll();
        $queryBuilder = $em->getRepository('ScenarisationProcessBundle:ScenarisationProcess')->createQueryBuilder('scenarisationProcesses');

        $query = $queryBuilder->getQuery();

        $paginator  = $this->get('knp_paginator');

        $scenarisationProcesses = $paginator->paginate(
          $query,
          $request->query->getInt('page', 1)/*page number*/,
          5/*limit per page*/
        );
        return $this->render('ScenarisationProcessBundle:scenarisationprocess:index.html.twig', array(
            'scenarisationProcesses' => $scenarisationProcesses
        ));
    }

    /**
     * Creates a new scenarisationProcess entity.
     *
     */
    public function newAction(Request $request)
    {
      // We generate the new breadcrumb
      $this->createBreadcrumb("new");
      // Mise en évidence du menu, changement du menu sélectionné
      $this->updateMenu();

        $scenarisationProcess = new ScenarisationProcess();
        $user  = $this->get('security.token_storage')->getToken()->getUser();
        $scenarisationProcess->setAuthor($user);

        $form = $this->createForm('ScenarisationProcessBundle\Form\ScenarisationProcessType', $scenarisationProcess);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($scenarisationProcess);
            $em->flush();

            /* Pour qu'après la redirection il y ait un joli petite boîte de validation*/
            $request->getSession()->getFlashBag()->add('success', 'La démarche \'' . $scenarisationProcess->getTitle() . '\' a bien été ajoutée.');

            return $this->redirectToRoute('scenarisation_process_index');
        }

        return $this->render('ScenarisationProcessBundle:scenarisationprocess:new.html.twig', array(
            'scenarisationProcess' => $scenarisationProcess,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a scenarisationProcess entity.
     *
     */
    public function showAction(ScenarisationProcess $scenarisationProcess)
    {
        // We generate the new breadcrumb
        $this->createBreadcrumb("show",$scenarisationProcess);
        // Mise en évidence du menu, changement du menu sélectionné
        $this->updateMenu();

        $deleteForm = $this->createDeleteForm($scenarisationProcess);

        return $this->render('ScenarisationProcessBundle:scenarisationprocess:show.html.twig', array(
            'scenarisationProcess' => $scenarisationProcess,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing scenarisationProcess entity.
     *
     */
    public function editAction(Request $request, ScenarisationProcess $scenarisationProcess)
    {
        // We generate the new breadcrumb
        $this->createBreadcrumb("edit",$scenarisationProcess);
        // Mise en évidence du menu, changement du menu sélectionné
        $this->updateMenu();

        $deleteForm = $this->createDeleteForm($scenarisationProcess);
        $editForm = $this->createForm('ScenarisationProcessBundle\Form\ScenarisationProcessType', $scenarisationProcess);

        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            /* Pour qu'après la redirection il y ait un joli petite boîte de validation*/
            $request->getSession()->getFlashBag()->add('Succès', 'LE SCENARIO A BIEN ETE EDITEE.');

            return $this->redirectToRoute('scenarisation_process_index');
        }

        return $this->render('ScenarisationProcessBundle:scenarisationprocess:edit.html.twig', array(
            'scenarisationProcess' => $scenarisationProcess,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a scenarisationProcess entity.
     *
     */
    public function deleteAction(Request $request, ScenarisationProcess $scenarisationProcess)
    {
          /* On vérifie si la démarche 'scénarise' déjà un scénario pédagogique. */
          if ($scenarisationProcess->getPedagogicalScenarios()[0] == null)
          {
            $form = $this->createDeleteForm($scenarisationProcess);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->remove($scenarisationProcess);
                $em->flush();

                /* Mettre un message de réussite */
                $request->getSession()->getFlashBag()->add('success', 'La démarche \'' . $scenarisationProcess->getTitle() . '\' a bien été supprimée.');
            }
          }
          else
          {
            /* Mettre un message d'erreur */
            $request->getSession()->getFlashBag()->add('alert', 'Vous ne pouvez par supprimer la démarche \'' . $scenarisationProcess->getTitle() . '\' car elle est liée à des scénarios pédagogiques.');
          }


          return $this->redirectToRoute('scenarisation_process_index');
    }

    /**
     * Creates a form to delete a scenarisationProcess entity.
     *
     * @param ScenarisationProcess $scenarisationProcess The scenarisationProcess entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ScenarisationProcess $scenarisationProcess)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('scenarisation_process_delete', array('id' => $scenarisationProcess->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }


    /**
    * Call in ajax.
    * Resorts ScenarisationStageOrders using their doctrine sortable property
    * @param integer $scen_process_id the id of the scenarisation stage which we are modifing
    * @param integer $scen_stageOrder_id id of the scenarisation stage order which we want to modify the position
    * @param integer $position the new position for the scenarisation stage
    */
    public function sortAction(Request $request, $scen_process_id, $scen_stageOrder_id, $position, $current_stage_id = NULL)
    {
      $routeOrigin = $this->container->get('request')->get('_route');
      $em = $this->getDoctrine()->getManager();

      $scenarisationStageOrder = $em->getRepository('ScenarisationProcessBundle:ScenarisationStageOrder')->find($scen_stageOrder_id);
      $scenarisationProcess = $em->getRepository('ScenarisationProcessBundle:ScenarisationProcess')->find($scen_process_id);

      $haveStageWithSequenceQuestion = $em->getRepository('ScenarisationProcessBundle:ScenarisationProcess')->haveAlreadyStageWithSequenceQuestion($scenarisationProcess);
      $haveStageWithSessionQuestion  = $em->getRepository('ScenarisationProcessBundle:ScenarisationProcess')->haveAlreadyStageWithSessionQuestion($scenarisationProcess);

      // On a des vérifications à faire.
      // Lorsqu'une démarche possède une étape avec un question séquence et une étape avec une
      // question séance on doit vérifier qu'on est pas en train de positionner l'étape
      // avec le question séance avant l'étape avec la question sequence.
      if ($haveStageWithSequenceQuestion && $haveStageWithSessionQuestion)
      {
        // Tableau dans lequel je vais enregristrer toutes les étapes de la démarche
        $tab_etapes = array();

        // J'enregistre
        foreach ($scenarisationProcess->getScenarisationStageOrder() as $currentScenarisationStageOrder)
        {
          $tab_etapes[$currentScenarisationStageOrder->getPosition()] = $currentScenarisationStageOrder;
        }

        // Je trie le tableau en fonction de la position
        ksort($tab_etapes);

        // Simuler le déplacement
        $tab_etapes_apres_deplacement = $this->deplacer($tab_etapes,$scenarisationStageOrder->getPosition(),$position);

        $deplacementAutorise = $this->testerCoherenceDeplacement($tab_etapes_apres_deplacement);
      }
      else
      {
        $deplacementAutorise = true;
      }


      if ($deplacementAutorise == true)
      {
        $scenarisationStageOrder->setPosition($position);
        $em->persist($scenarisationStageOrder);
        $em->flush();
      }
      else
      {
        /* Petit message de reussite, à traduire */
        $request->getSession()->getFlashBag()->add('alert', "Impossible de déplacer l'étape comme demandé : vous ne pouvez pas positionner une étape avec une question séance avant une étape avec une question séquence.");

      }


      // Si la route d'origine est la gestion des étapes dans un process on renvoie vers cette vue
      # Si on vient de l'étape on renvoie à l'étape et si on vient de la démarche on renvoie vers la démarche
      if ($routeOrigin != 'move_scenarisation_stage_in_a_process')
        return $this->stagesEditAction($request,$scenarisationProcess);
      else
        return $this->redirectToRoute('scenarisation_stage_process_management', array('id' => $current_stage_id));
    }


    /**
    * Displays a drag&drop table to edit the scenarisation stages of a scenarisation process.
    * @param ScenarisationProcess $scenarisationProcess
    */
    public function stagesEditAction(Request $request, ScenarisationProcess $scenarisationProcess)
    {
      // Avant de permettre l'édition il va falloir s'assurer que la démarche n'est pas
      // Déjà utilisée par des scénarios pédagogiques qui ont des réponses initialisées
      if ($this->haveAlreadyAnAnswer($scenarisationProcess) == false)
      {
        $entityManager = $this->get('doctrine')->getManager();
        // We generate the new breadcrumb
        $this->createBreadcrumb("stages_management", $scenarisationProcess);
        // Mise en évidence du menu, changement du menu sélectionné
        $this->updateMenu();

        // On récupère la liste des étapes (et leur position) pour la démarche courante
        $etape_demarche_courante = $entityManager->createQuery ('
        select o,s
        FROM ScenarisationProcessBundle:ScenarisationStageOrder o
        JOIN o.scenarisationStage s
        WHERE o.scenarisationProcess = :scenarisationProcess
        ORDER BY o.position');

        // On récupère toutes les autres étapes, çad celles qui ne sont pas dans la démarche courante
        $autres_etapes = $entityManager->createQuery ('
        select s
        FROM ScenarisationProcessBundle:ScenarisationStage s
        WHERE s NOT IN (select ss
        FROM ScenarisationProcessBundle:ScenarisationStageOrder o
        JOIN o.scenarisationStage ss
        WHERE o.scenarisationProcess = :scenarisationProcess)');

       $etape_demarche_courante->setParameter('scenarisationProcess',$scenarisationProcess);
       $autres_etapes->setParameter('scenarisationProcess',$scenarisationProcess);

       $scenarisationStageOrders  = $etape_demarche_courante->getResult();
       $scenarisationStagesNotLinked = $autres_etapes->getResult();

       return $this->render('ScenarisationProcessBundle:scenarisationprocess:stagesEdit.html.twig', array(
       'scenarisationProcess' => $scenarisationProcess,
       'scenarisationStageOrders' => $scenarisationStageOrders,
       'scenarisationStagesNotLinked' => $scenarisationStagesNotLinked
        ));
      }
      else
      {
        /* Message d'erreur, à traduire */
        $request->getSession()->getFlashBag()->add('alert', 'Impossible de modifier les étapes de la démarche : une question possède déjà une réponse.');

        return $this->redirectToRoute('scenarisation_process_index');
      }
    }

    /**
     * Add a ScenarisationStage to a ScenarisationProcess
     */
    public function stagesAddAction(Request $request, ScenarisationProcess $scenarisationProcess,ScenarisationStage $scenarisationStage, $page = 1)
    {
      /* ---------------------------------------------------------------
      Il y a un certain nombre de choses à vérifier avant de valider l'ajout de l'étape

        1 : Si l'étape à ajouter à la démarche possède une question
            séquence (et) ou une question séance ;

          1.1 : Si l'étape à ajouter possède une question séquence et pas de question séance

            1.1.1 : On vérifie si la démarche possède déjà une question séquence

          1.2 : Si l'étape à ajouter possède une question séance et pas de question séquence ;

            1.2.1 : On vérifie si la démarche possède une question séquence dans une des étapes avant l'étape courante ;

          1.3 : Si l'étape à ajouter possède une question qéquence et une question séance

            1.3.1 : On vérifie que la démarche ne possède ni question séquence ni question séance

            1.3.2 : On vérifie que la question séance est bien après la question séquence

      ---------------------------------------------------------------  */

      # On récupère la route car la vue appelée n'est pas la même.
      # En effet, on utilise cette même méthode que l'on doit dans une démarche en train d'ajouter une étape
      # Ou bien dans une etape en train de la lier à une démarche
      $routeOrigin = $this->container->get('request')->get('_route');

      $manager = $this->container->get('doctrine')->getEntityManager('default');
      $autorise_a_ajouter_letape = false;

      /* 1 : Si l'étape à ajouter à la démarche possède une question
          séquence (et) ou une question séance ; */
           $haveASequenceQuestion = $this->haveASequenceQuestion($scenarisationStage);
           $haveASessionQuestion  = $this->haveASessionQuestion($scenarisationStage);

      if ( $haveASessionQuestion || $haveASequenceQuestion )
      {
        $currentProcessHaveAlreadyStageWithSeqQuest = $manager->getRepository('ScenarisationProcessBundle:ScenarisationProcess')->haveAlreadyStageWithSequenceQuestion($scenarisationProcess);
        $currentProcessHaveAlreadyStageWithSessQuest = $manager->getRepository('ScenarisationProcessBundle:ScenarisationProcess')->haveAlreadyStageWithSessionQuestion($scenarisationProcess);

        /* 1.1 : Si l'étape à ajouter possède une question séquence et pas de question séance */
        if ( $haveASequenceQuestion && !$haveASessionQuestion)
        {
          /* 1.1.1 : On vérifie si la démarche possède déjà une question séquence */
          if ($currentProcessHaveAlreadyStageWithSeqQuest)
          {
            /* Message d'erreur, à traduire */
            $request->getSession()->getFlashBag()->add('alert', "Impossible d'ajouter l'étape à la démarche. L'étape possède une question séquence et la démarche possède déjà une question séquence (dans une autre de ses étapes).");
          }
          else
          {
            $autorise_a_ajouter_letape = true;
          }
        }
        /* 1.2 : Si l'étape à ajouter possède une question séance et pas de question séquence */
        else if (!$haveASequenceQuestion && $haveASessionQuestion)
        {
          if ($currentProcessHaveAlreadyStageWithSessQuest)
          {
            /* Message d'erreur, à traduire */
            $request->getSession()->getFlashBag()->add('alert', "Impossible d'ajouter l'étape à la démarche. L'étape possède une question séance et la démarche possède déjà une question séance (dans une autre de ses étapes).");
          }
          else
          {

            if ($currentProcessHaveAlreadyStageWithSeqQuest)
            {
              $autorise_a_ajouter_letape = true;
            }
            else
            {
              /* Message d'erreur, à traduire */
              $request->getSession()->getFlashBag()->add('alert', "Impossible d'ajouter l'étape à la démarche. L'étape possède une question séance et la démarche ne possède pas de question séquence. Une question séquence est nécessaire avant une question séance.");
            }

          }

        }
        /* 1.3 : Si l'étape à ajouter possède une question séquence et une question séance */
        else // if ($haveASequenceQuestion && $haveASessionQuestion)
        {
          // On vérifie que la démarche ne possède pas de question séquence et séance
          if (!$currentProcessHaveAlreadyStageWithSeqQuest && !$currentProcessHaveAlreadyStageWithSessQuest)
          {
            // On vérifie que la position de la question séquence est bien avant la question séance
            $positionQuestionSequence = 0;
            $positionQuestionSeance   = 0;

            foreach ($scenarisationStage->getQuestionOrder() as $currentQuestionOrder)
            {
              if ($currentQuestionOrder->getQuestion()->getType() == "sequenceQuestion")
              {
                $positionQuestionSequence = $currentQuestionOrder->getPosition();
              }
              else if ($currentQuestionOrder->getQuestion()->getType() == "sessionQuestion")
              {
                $positionQuestionSeance = $currentQuestionOrder->getPosition();
              }
            }

            if ($positionQuestionSequence < $positionQuestionSeance)
            {
              $autorise_a_ajouter_letape = true;
            }
            else
            {
              /* Message d'erreur, à traduire */
              $request->getSession()->getFlashBag()->add('alert', "Impossible d'ajouter l'étape à la démarche. La question séance est avant la question séquence, or la question séance doit se trouver après le question séance.");
            }
          }
          else
          {
            if ($currentProcessHaveAlreadyStageWithSeqQuest && !$currentProcessHaveAlreadyStageWithSessQuest)
            {
              /* Message d'erreur, à traduire */
              $request->getSession()->getFlashBag()->add('alert', "Impossible d'ajouter l'étape à la démarche. L'étape possède une question séquence et une question séance et la démarche possède déjà une question séquence (dans une autre de ses étapes).");
            }
            else if (!$currentProcessHaveAlreadyStageWithSeqQuest && $currentProcessHaveAlreadyStageWithSessQuest)
            {
              /* Message d'erreur, à traduire */
              $request->getSession()->getFlashBag()->add('alert', "Impossible d'ajouter l'étape à la démarche. L'étape possède une question séquence et une question séance et la démarche possède déjà une question séance (dans une autre de ses étapes).");
            }
            else
            {
              /* Message d'erreur, à traduire */
              $request->getSession()->getFlashBag()->add('alert', "Impossible d'ajouter l'étape à la démarche. L'étape possède une question séquence et une question séance et la démarche possède déjà une question séquence et une question séance (dans d'autres de ses étapes).");
            }
          }
        }
      }
      else
      {
        $autorise_a_ajouter_letape = true;
      }

      if($autorise_a_ajouter_letape)
      {
        $scenarisationStageOrder = new ScenarisationStageOrder();

        // Ici on vérifie si on est en train d'ajouter notre première étape à notre démarche
        // Car L'extension Sortable de Doctrine commence la position à 0
        // Et je n'ai trouvé aucune manière moins gourmande et plus élégante de faire ça

        $dql_stage_order_count = $manager->createQuery('SELECT count(s)
                                                        FROM ScenarisationProcessBundle:ScenarisationStageOrder s
                                                        WHERE s.scenarisationProcess = :scenarisationProcess');
        $dql_stage_order_count->setParameter('scenarisationProcess',$scenarisationProcess);
        $nbStageOrder = $dql_stage_order_count->getResult();

        // Si la démarche de scénarisation ne contient aucune étape de scénarisation
        if ($nbStageOrder[0][1] == 0)
          $scenarisationStageOrder->setPosition(1);
        // ----------------------------

        $scenarisationStage->addScenarisationStageOrder($scenarisationStageOrder);
        $scenarisationProcess->addScenarisationStageOrder($scenarisationStageOrder);

        $manager->persist($scenarisationStageOrder);
        $manager->flush();
      }

      # Si on vient de l'étape on renvoie à l'étape et si on vient de la démarche on renvoie vers la démarche
      if ($routeOrigin != 'add_scenarisation_stage_in_a_process')
        return $this->redirectToRoute('scenarisation_process_stage_management', array('id' => $scenarisationProcess->getId()));
      else
        return $this->redirectToRoute('scenarisation_stage_associate_to_process', array('id' => $scenarisationStage->getId(), 'page' => $page ));
    }

    /**
     * Remove a ScenarisationStage from a ScenarisationProcess
     */
    public function stagesRemoveAction(Request $request, ScenarisationProcess $scenarisationProcess, ScenarisationStageOrder $scenarisationStageOrder)
    {
        $scenarisationStage = $scenarisationStageOrder->getScenarisationStage();
        $manager = $this->container->get('doctrine')->getEntityManager('default');
        $autorise_a_retirer_letape = false;

      /* ---------------------------------------------------------------
      Il y a un point à vérifier avant de valider le retrait d'une étape

        1 : Si l'étape courante a une question séquence et n'a pas de question
            séance et que la démarche à une question séance
            alors on interdit la suppression.
            Sinon, on se retrouverait avec une démarche sans question séquence.
      ---------------------------------------------------------------  */

      $haveASequenceQuestion = $this->haveASequenceQuestion($scenarisationStage);
      $haveASessionQuestion  = $this->haveASessionQuestion($scenarisationStage);
      $currentProcessHaveSessQuest = $manager->getRepository('ScenarisationProcessBundle:ScenarisationProcess')->haveAlreadyStageWithSessionQuestion($scenarisationProcess);

      /* 1 : Si je retire l'étape, est-ce que je me retrouve avec une démarche ayant une question séance
          sans question séquence */

      if ($haveASequenceQuestion && !$haveASessionQuestion && $currentProcessHaveSessQuest)
      {
        /* Message d'erreur, à traduire */
        $request->getSession()->getFlashBag()->add('alert', "Impossible de retirer l'étape. L'étape possède une question séquence et la démarche possède (dans une étape différente) une question séance. Une démarche doit nécessairement avoir une question séquence avant une question séance.");
      }
      else
      {
          $autorise_a_retirer_letape = true;
      }

      if ($autorise_a_retirer_letape)
      {
        $manager->remove($scenarisationStageOrder);
        $manager->flush();
      }

      return $this->redirectToRoute('scenarisation_process_stage_management', array('id' => $scenarisationProcess->getId()));
    }

    public function cloneAction(ScenarisationProcess $scenarisationProcess)
    {
      $em = $this->container->get('doctrine')->getEntityManager('default');
      // Repository de question : on en aura besoin pour récupérer à partir d'un ID
      $questionRepo = $em->getRepository('ScenarisationProcessBundle:Question');

      // On clone la démarche
      $scenarisationProcessCopie = clone $scenarisationProcess;

      // On change le titre
      $scenarisationProcessCopie->setTitle($scenarisationProcessCopie->getTitle() . " - (copie)");

      /*  Dans le cas où une question est présente dans deux étapes différentes de la démarche à cloner,
          il ne faut pas cloner la question deux fois mais 'se souvenir' qu'on l'a déjà clonée */

      /* Ce tableau enregistre les ID des questions qui ont été clonées */
      $questionsAlreadyCloned = array();
      /* Ce tableau enregistre les ID des clones */
      $questionsCloned = array();

      // On clone chacune des étapes avec le lien bien sûr...
      foreach ($scenarisationProcess->getScenarisationStageOrder() as $currentScenarisationStageOrder)
      {
        /* On clone l'étape et la classe d'association */
        $scenarisationStageCopie      = clone $currentScenarisationStageOrder->getScenarisationStage();
        $scenarisationStageOrderCopie = clone $currentScenarisationStageOrder;

        // On change le titre de l'étape
        $scenarisationStageCopie->setTitle($scenarisationStageCopie->getTitle() . " - (copie)");

        /* On fait les liens entre démarche -> association && étape -> association */
        $scenarisationStageCopie->addScenarisationStageOrder($scenarisationStageOrderCopie);
        $scenarisationProcessCopie->addScenarisationStageOrder($scenarisationStageOrderCopie);

        // On clone chacune des questions avec le lien bien sûr...
        foreach ($currentScenarisationStageOrder->getScenarisationStage()->getQuestionOrder() as $currentQuestionOrder)
        {
          /* On regarde la question actuelle a déjà été clonée précédemment */
          $positionTrouvee = array_search($currentQuestionOrder->getQuestion()->getId(), $questionsAlreadyCloned);

          if ($positionTrouvee === false)
          {
            /* On clone la question et la classe d'association */
            $questionCopie      = clone $currentQuestionOrder->getQuestion();

            $questionCopie->setFrWording("Copie - " . $questionCopie->getFrWording());
            $questionCopie->setEsWording("Copia - " . $questionCopie->getEsWording());
            $questionCopie->setEnWording("Copy - " . $questionCopie->getEnWording());

            /* Il faut flush maintenant car la base de données va affecter au clone
               un 'id' correct. En effet, l'id avant flush est identique au clone */
            $em->persist($questionCopie);
            $em->flush();

            array_push($questionsAlreadyCloned, $currentQuestionOrder->getQuestion()->getId());
            array_push($questionsCloned, $questionCopie->getId());
          }
          else
          {
            $questionCopie = $questionRepo->find($questionsCloned[$positionTrouvee]);
          }

          // Dans tous les cas on clone la classe d'association
          $questionOrderCopie = clone $currentQuestionOrder;

          /* On fait les liens entre étape -> association && question -> association */
          $questionCopie->addQuestionOrder($questionOrderCopie);
          $scenarisationStageCopie->addQuestionOrder($questionOrderCopie);

          // Et puis on enregistre
          $em->persist($questionOrderCopie);

        }
      }
      $em->persist($scenarisationProcessCopie);
      $em->flush();

      return $this->redirectToRoute('scenarisation_process_index', array('id' => $scenarisationProcess->getId()));
    }

    private function updateMenu()
    {
      // Mise en évidence du menu, changement du menu sélectionné
      $menu = array('pedagogical_scenario' => "",
                    'projects'             => "",
                    'trainingCourse'       => "",
                    'user'                 => "",
                    'parameters'           => "",
                    'scenarisation_process'=> "list-group-item-info",
                    'scenarisation_stage'  => "",
                    'question'             => "");

      // Mise en session du menu
      $this->get('session')->set('menu',$menu);
    }

    private function createBreadcrumb($currentAction, $scenarisationProcess = null)
    {
      $breadcrumbs = $this->get("white_october_breadcrumbs");
      switch($currentAction)
      {
        case "index":
          $breadcrumbs->addItem("scenarisationProcess");
        break;

        case "show":
          $breadcrumbs->addRouteItem("scenarisationProcess", "scenarisation_process_index");
          $breadcrumbs->addItem($scenarisationProcess->getTitle());
          $breadcrumbs->addItem("show");

        break;

        case "new":
          $breadcrumbs->addRouteItem("scenarisationProcess", "scenarisation_process_index");
          $breadcrumbs->addItem("scenarisationProcess.new");
        break;

        case "edit":
          $breadcrumbs->addRouteItem("scenarisationProcess", "scenarisation_process_index");
          $breadcrumbs->addRouteItem($scenarisationProcess->getTitle(), "scenarisation_process_show", [
              'id' => $scenarisationProcess->getId()
          ]);
          $breadcrumbs->addItem("edit");

        break;

        case "stages_management":
          $breadcrumbs->addRouteItem("scenarisationProcess", "scenarisation_process_index");
          $breadcrumbs->addRouteItem($scenarisationProcess->getTitle(), "scenarisation_process_show", [
              'id' => $scenarisationProcess->getId()
          ]);
          $breadcrumbs->addItem("scenarisationStage");
        break;
      }
    }

    private function haveAlreadyAnAnswer(ScenarisationProcess $scenarisationProcess)
    {
      /* On part du principe qu'il n'y a pas de réponse initialisée à la démarche */
      $answerInitializedFound = false;

      /* Si la démarche de scénarisation courante scénarise une démarche, alors on peut
         passer à des tests plus poussés */
      if (!empty($scenarisationProcess->getPedagogicalScenarios()->toArray()))
      {
        // On parcourt toutes les étapes
        foreach ($scenarisationProcess->getScenarisationStageOrder() as $currentScenarisationStageOrder)
        {
          // Toutes les questions
          foreach ($currentScenarisationStageOrder->getScenarisationStage()->getQuestionOrder() as $currentQuestionOrder)
          {
            // Toutes les réponses
            foreach ($currentQuestionOrder->getQuestion()->getAnswers() as $currentAnswer)
            {
              // Si on tombe sur une vraie réponse correctement initialisée
              if($currentAnswer != null)
              {
                /* On regarde si le scénario pédagogique associé à la réponse trouvée
                   est une des scénario pédagogique lié à la démarche de scénarisation courante */
                foreach ($scenarisationProcess->getPedagogicalScenarios() as $currentPedagogicalScenario)
                {
                  if ($currentAnswer->getPedagogicalScenario()->getId() == $currentPedagogicalScenario->getId())
                  {
                    // On change l'état du booléen
                    $answerInitializedFound = true;
                    // On sort des 4 boucles imbriquées (d'où le '4')
                    break 4;
                  }
                }
              }
            }
          }
        }
      }
      // On retourne le booléen : Est ce qu'on a trouvé une réponse ou non
      return $answerInitializedFound;
    }

    private function haveASequenceQuestion(scenarisationStage $scenarisationStage)
    {
      $haveASequenceQuestion = false;

      foreach ($scenarisationStage->getQuestionOrder() as $currentQuestionOrder)
      {
        if ($currentQuestionOrder->getQuestion()->getType() == 'sequenceQuestion')
        {
          $haveASequenceQuestion = true;
          break 1;
        }
      }

      return $haveASequenceQuestion;
    }

    private function haveASessionQuestion(scenarisationStage $scenarisationStage)
    {
      $haveASessionQuestion = false;

      foreach ($scenarisationStage->getQuestionOrder() as $currentQuestionOrder)
      {
        if ($currentQuestionOrder->getQuestion()->getType() == 'sessionQuestion')
        {
          $haveASessionQuestion = true;
          break 1;
        }
      }

      return $haveASessionQuestion;
    }

    /* Cette fonction permet de déplacer dans un tableau
       Un élément en position initiale vers la nouvelle position.
       Pour un tableau trié : [1] [2] [3] [4] [5] [6]
       Si on demande a déplacer 5 en 2, le tableau retourné est
       le suivant : [1] [5] [2] [3] [4] [6]

       Si on demande a déplacer 2 en 5, le tableau retourné est
       le suivant : [1] [3] [4] [5] [2] [6]
       */
      private function deplacer($tab, int $positionInitiale, int $nouvellePosition)
      {
        if ($nouvellePosition != $positionInitiale)
        {
          $pivot                   = $tab[$nouvellePosition];
          $tab[$nouvellePosition]  = $tab[$positionInitiale];

          if ($nouvellePosition < $positionInitiale)
          {
            for ($i=$nouvellePosition+1 ; $i <= $positionInitiale ; $i++)
            {
              $pivott  = $tab[$i];
              $tab[$i] = $pivot;
              $pivot   = $pivott;
            }
          }
          else
          {
            for ($i=$nouvellePosition-1 ; $i >= $positionInitiale ; $i--)
            {
              $pivott  = $tab[$i];
              $tab[$i] = $pivot;
              $pivot   = $pivott;
            }
          }
        }

        return $tab;
      }

      /* Cette méthode permet de tester la cohérence d'un tableau d'étapes lors de la simulation
         du changement de la position des étapes */
      private function testerCoherenceDeplacement($tab_etapes_order)
      {
        $coherent = false;

        foreach ($tab_etapes_order as $currentStageOrder)
        {
          $currentStageHaveSequenceQuestion = $this->haveASequenceQuestion($currentStageOrder->getScenarisationStage());
          $currentStageHaveSessionQuestion  = $this->haveASessionQuestion($currentStageOrder->getScenarisationStage());

          // Si l'étape courant a une question séance et n'a pas de question séquence, on sort
          if ($currentStageHaveSessionQuestion && !$currentStageHaveSequenceQuestion)
          {
            break;
          }
          else if ($currentStageHaveSequenceQuestion)
          {
            // On vient de trouver une question sequence avant une question séance
            $coherent = true;
            break;
          }
        }
        return $coherent;
      }

}
