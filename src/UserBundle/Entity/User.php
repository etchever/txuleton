<?php

namespace UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

 /**
  * User
  *
  * @ORM\Table(name="user")
  * @ORM\Entity(repositoryClass="UserBundle\Repository\UserRepository")
  */
class User implements UserInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=255)
     */
    private $lastname;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=255)
     */
    private $firstname;

    /**
     * @var string
     *
     * @ORM\Column(name="plateformLanguage", type="string", length=255, nullable=false)
     */
    private $plateformLanguage;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, unique=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="salt", type="string", length=255)
     */
    private $salt;

    /**
     * @var string
     *
     * @ORM\Column(name="token", type="string", length=255, nullable=true)
     */
    private $token;

    /**
     * @var string
     *
     * @ORM\Column(name="roles", type="array", nullable=false)
     *
     * This class (USER) 'implements' USER INTERFACE that obligate to define the abstract attributes
     * like 'roles', we can't use 'role'
     */
    private $roles = array();

    /**
      * @ORM\ManyToMany(targetEntity="ScenarioBundle\Entity\PedagogicalScenario", cascade={"persist"})
      * @ORM\JoinColumn(nullable=true)
      */
    private $pedagogicalScenarioDesigned;

    /**
     * @ORM\OneToMany(targetEntity="ScenarioBundle\Entity\PedagogicalScenario", mappedBy="referentTeacher", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     *
     * Those are the Pedagogical Scenarios that a Pedagogical Engineer pilots.
     */
    private $pedagogicalScenarioPiloted;

    /**
     * @ORM\OneToMany(targetEntity="ScenarisationProcessBundle\Entity\ScenarisationProcess", mappedBy="author", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $scenarisationProcessesCreated;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     *
     * @return User
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set plateformLanguage
     *
     * @param string $plateformLanguage
     *
     * @return User
     */
    public function setPlateformLanguage($plateformLanguage)
    {
        $this->plateformLanguage = $plateformLanguage;

        return $this;
    }

    /**
     * Get plateformLanguage
     *
     * @return string
     */
    public function getPlateformLanguage()
    {
        return $this->plateformLanguage;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        $this->email    = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set salt
     *
     * @param string $salt
     *
     * @return User
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Get salt
     *
     * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Set role
     *
     * @param string $role
     *
     * @return User
     * Must be an array so I use array() around the string role.
     */
    public function setRoles($role)
    {
        $this->roles = array($role);

        return $this;
    }

    /**
     * Get role
     *
     * @return string
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * Get role
     *
     * @return string
     */
    public function getRole()
    {
        return $this->roles[0];
    }

    /**
     * Get role for router
     *
     * @return string
     */
    public function getRoleForRouter()
    {
        if($this->getRole() == "ROLE_PEDAGOGICAL_ENGINEER")
          return "pedagogical-engineer";
        else if ($this->getRole() == "ROLE_TEACHER")
          return "teacher";
    }

    /* Sert uniquement à la barre de debug de Symfony en bas de page */
    public function getUsername()
    {
      if ($this->getRoles()[0] == "ROLE_PEDAGOGICAL_ENGINEER")
        $role = "engineer";
      else
        $role = "teacher";

      return $role . " - " . $this->email;
    }


    public function eraseCredentials() {}
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->cours = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add pedagogicalScenarioDedigned
     *
     * @param \ScenarioBundle\Entity\PedagogicalScenario $pedagogicalScenarioDesigned
     *
     * @return User
     */
    public function addPedagogicalScenarioDesigned(\ScenarioBundle\Entity\PedagogicalScenario $pedagogicalScenarioDedigned)
    {
        $this->pedagogicalScenarioDesigned[] = $pedagogicalScenarioDedigned;

        return $this;
    }

    /**
     * Remove pedagogicalScenarioDedigned
     *
     * @param \ScenarioBundle\Entity\PedagogicalScenario $pedagogicalScenarioDesigned
     */
    public function removePedagogicalScenarioDesigned(\ScenarioBundle\Entity\PedagogicalScenario $pedagogicalScenarioDedigned)
    {
        $this->pedagogicalScenarioDesigned->removeElement($pedagogicalScenarioDedigned);
    }

    /**
     * Get pedagogicalScenarioDesigned
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPedagogicalScenarioDesigned()
    {
        return $this->pedagogicalScenarioDesigned;
    }

    /**
     * Add pedagogicalScenarioPiloted
     *
     * @param \ScenarioBundle\Entity\PedagogicalScenario $pedagogicalScenarioPiloted
     * @return User
     */
    public function addPedagogicalScenarioPiloted(\ScenarioBundle\Entity\PedagogicalScenario $pedagogicalScenarioPiloted)
    {
        $this->pedagogicalScenarioPiloted[] = $pedagogicalScenarioPiloted;

        return $this;
    }

    /**
     * Remove pedagogicalScenarioPiloted
     *
     * @param \ScenarioBundle\Entity\PedagogicalScenario $pedagogicalScenarioPiloted
     */
    public function removePedagogicalScenarioPiloted(\ScenarioBundle\Entity\PedagogicalScenario $pedagogicalScenarioPiloted)
    {
        $this->pedagogicalScenarioPiloted->removeElement($pedagogicalScenarioPiloted);
    }

    /**
     * Get pedagogicalScenarioPiloted
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPedagogicalScenarioPiloted()
    {
        return $this->pedagogicalScenarioPiloted;
    }

    /**
     * Generate Salt
     *
     * @return \UserBundle\Entity\User
     */
    public function generateSalt()
    {
        if ($this->getSalt() == null)
          $this->setSalt(md5(time()));

        return $this;
    }

    /**
     * Get Lastname and Firstname (for the choice label of the form)
     *
     * @return string
     */
    public function getLastnameFirstname()
    {
        return $this->getLastname() . " " . $this->getFirstname();
    }

    /**
     * Add a Scenarisation Process to the Scenarisation Processes Created tab.
     *
     * @param \ScenarisationProcessBundle\Entity\ScenarisationProcess $scenarisationProcess
     *
     * @return User
     */
    public function addScenarisationProcessCreated(\ScenarisationProcessBundle\Entity\ScenarisationProcess $scenarisationProcess)
    {
      $this->scenarisationProcessesCreated[] = $scenarisationProcess;

      return $this;
    }

    /**
     * Remove a Scenarisation Process from the Scenarisation Processes Created tab.
     *
     * @param \ScenarisationProcessBundle\Entity\ScenarisationProcess $scenarisationProcess
     *
     * @return User
     */
    public function removeScenarisationProcessCreated(\ScenarisationProcessBundle\Entity\ScenarisationProcess $scenarisationProcess)
    {
      $this->scenarisationProcessesCreated->removeElement($scenarisationProcess);

      return $this;
    }

    /**
     * Get Scenarisation Processes created.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getScenarisationProcessesCreated()
    {
      return $this->scenarisationProcessesCreated;
    }

    /**
     * Set token
     *
     * @param string $useless
     *
     * @return User
     */
    public function setToken($useless = null)
    {
      $this->token = bin2hex(random_bytes(20));
      return $this;
    }

    /**
     * Remove token
     *
     * @return User
     */
    public function removeToken()
    {
      $this->token = null;
      return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }
}
