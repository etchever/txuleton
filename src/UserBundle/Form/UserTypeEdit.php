<?php

namespace UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;


class UserTypeEdit extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
      // I have to specify the label to make it work with translation
      // Else, it put the label with the first caracter in uppercase and
      // I don't want it.

        $builder->add('lastname','text',array('label'=> 'lastname'));
        $builder->add('firstname','text',array('label'=> 'firstname'));
        $builder->add('email',EmailType::class, array('label'=> 'email'));

        $builder->add('plateformLanguage', 'choice', array(
                               'label' => "plateformLanguage",
                               'choices' => array(
                                   'fr' => 'fr',
                                   'en' => 'en',
                                   'es' => 'es'
                                 ),
                                'required' => true,
                                //'preferred_choices' => array('es')
                             )
                          );


    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'UserBundle\Entity\User'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'utilisateurbundle_utilisateur';
    }


}
