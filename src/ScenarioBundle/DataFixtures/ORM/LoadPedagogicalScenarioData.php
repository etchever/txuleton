<?php

// src/ScenarioBundle/DataFixtures/ORM/LoadPedagogicalScenarioData

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use ScenarioBundle\Entity\PedagogicalScenario;

class LoadPedagogicalScenarioData implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        /*$em = $this->container->get('doctrine')->getManager();

        // On récupère le projet
        $repoProjet = $em->getRepository('ScenarioBundle:Project');
        $dutInfo    = $repoProjet->findOneBy(array('shortname' => 'DUT INFO'));

        // On récupère les utilisateurs
        $repoUtilisateur = $em->getRepository('UserBundle:User');
        $gaizka          = $repoUtilisateur->findOneBy(array('firstname'=>'Gaizka'));
        $patrick         = $repoUtilisateur->findOneBy(array('firstname'=>'Patrick'));
        $claire          = $repoUtilisateur->findOneBy(array('firstname'=>'Claire'));
        $pantxika        = $repoUtilisateur->findOneBy(array('firstname'=>'Pantxika'));



        $demarche = "Démarche de Marta";
        //On ajoute deux scenario pedagogique
        $m1102 = $this->ajouterM1102($manager,$em,$patrick,$dutInfo,$demarche);
        $m1103 = $this->ajouterM1103($manager,$em,$patrick,$dutInfo,$demarche);
        $m1104 = $this->ajouterM1104($manager,$em,$patrick,$dutInfo,$demarche);

        $gaizka->addPedagogicalScenarioDesigned($m1102);
        $pantxika->addPedagogicalScenarioDesigned($m1102);
        $claire->addPedagogicalScenarioDesigned($m1102);
        $claire->addPedagogicalScenarioDesigned($m1103);

        $manager->flush();
        */
    }

    public function ajouterM1102(ObjectManager $manager,$em,$enseignantReferent,$projet,$demarche)
    {
      $m1102 = new PedagogicalScenario();

      $repoDemarche = $em->getRepository('ScenarisationProcessBundle:ScenarisationProcess');
      $demarcheMere = $repoDemarche->findOneBy(array('title'=>$demarche));
      $m1102->setScenarisationProcess($demarcheMere);

      $m1102->setShortname("M1102");
      $m1102->setFullname("Initiation à l'algorithmique et à la programmation");
      $m1102->updateReferentTeacher($enseignantReferent);

      //$m1102->setProject($projet);

      $manager->persist($m1102);
      $manager->flush();

      return $m1102;
    }

    public function ajouterM1103(ObjectManager $manager,$em,$enseignantReferent,$projet,$demarche)
    {
      $m1103 = new PedagogicalScenario();

      $repoDemarche = $em->getRepository('ScenarisationProcessBundle:ScenarisationProcess');
      $demarcheMere = $repoDemarche->findOneBy(array('title'=>$demarche));
      $m1103->setScenarisationProcess($demarcheMere);

      $m1103->setShortname("M1103");
      $m1103->setFullname("Gestion de projets");
      $m1103->updateReferentTeacher($enseignantReferent);
      $m1103->setProject($projet);


      $manager->persist($m1103);
      $manager->flush();

      return $m1103;
    }

    public function ajouterM1104(ObjectManager $manager,$em,$enseignantReferent,$projet,$demarche)
    {
      $m1104 = new PedagogicalScenario();

      $repoDemarche = $em->getRepository('ScenarisationProcessBundle:ScenarisationProcess');
      $demarcheMere = $repoDemarche->findOneBy(array('title'=>$demarche));
      $m1104->setScenarisationProcess($demarcheMere);

      $m1104->setShortname("M1104");
      $m1104->setFullname("POO");
      $m1104->updateReferentTeacher($enseignantReferent);
      $m1104->setProject($projet);


      $manager->persist($m1104);
      $manager->flush();

      return $m1104;
    }

    public function getOrder()
    {
        return 4;
    }
}
